package de.craften.plugins.plotplus.gui;


import de.craften.plugins.mcguilib.*;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.plot.Plot;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class PlotPlusGui implements Listener {
    private ViewManager viewManager;
    private PlotPlus plugin;

    public PlotPlusGui(PlotPlus plugin) {
        this.plugin = plugin;
        viewManager = new ViewManager(plugin);
    }

    public void showPlotPlusMenu(final Player player) {
        SinglePageView view = new SinglePageView(ChatColor.BOLD + "PlotPlus", 9);

        Button b = new Button(Material.STAINED_GLASS_PANE, (byte) 14, "Your plots");
        b.setOnClick(new ClickListener() {
            @Override
            public void clicked(InventoryClickEvent event) {
                showMyPlotsMenu(player, 0);
            }
        });
        view.addElement(b);

        b = new Button(Material.STAINED_GLASS_PANE, (byte) 13, "Shared plots", "Plots by other players that you may build on.");
        b.setOnClick(new ClickListener() {
            @Override
            public void clicked(InventoryClickEvent event) {
                showHelpingOnPlotsMenu(player);
            }
        });
        view.addElement(b);

        b = new Button(Material.SIGN, "Close");
        b.setOnClick(new ClickListener() {
            @Override
            public void clicked(InventoryClickEvent event) {
                event.getWhoClicked().closeInventory();
            }
        });
        view.insertElement(8, b);

        viewManager.showView(player, view);
    }

    public void showMyPlotsMenu(final Player player, int page) {
        final MultiPageView view = new PlotsView(ChatColor.BOLD + "Your Plots" + ChatColor.RESET, 4 * 9);
        for (final Plot plot : plugin.getWorld(player.getWorld()).getPlotsByOwner(player.getUniqueId())) {
            Button button;
            if (plot.getConfig().hasName()) {
                button = new Button(Material.CARPET, (byte) 5, "\"" + plot.getConfig().getName() + "\"");
                button.setDescription("Plot " + plot.getIdX() + ";" + plot.getIdZ());
            } else {
                button = new Button(Material.CARPET, (byte) 5, "Plot " + plot.getIdX() + ";" + plot.getIdZ());
            }
            button.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent event) {
                    if (event.isShiftClick() && player.hasPermission("plotplus.user.teleport.home")) {
                        plot.teleportPlayer(player);
                    } else {
                        showMyPlotMenu(player, plot, view.getPage());
                    }
                }
            });
            view.addElement(button);
        }

        viewManager.showView(player, view);
    }

    public void showMyPlotMenu(final Player player, final Plot plot, final int previousPlotsPage) {
        View view = new SinglePageView(ChatColor.BOLD + "Plot " + plot.getIdX() + ";" + plot.getIdZ() + ChatColor.RESET, 9);

        Button backButton = new Button(Material.SIGN, "Back");
        backButton.setOnClick(new ClickListener() {
            @Override
            public void clicked(InventoryClickEvent event) {
                showMyPlotsMenu(player, previousPlotsPage);
            }
        });
        view.addElement(backButton);

        if (player.hasPermission("plotplus.user.teleport.home")) {
            Button teleportButton = new Button(Material.COMPASS, "Teleport");
            teleportButton.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent event) {
                    plot.teleportPlayer(player);
                }
            });
            view.addElement(teleportButton);
        }

        if (player.hasPermission("plotplus.user.editconfig.hitprotection")) {
            final Checkbox hitProtectionCheckbox = new Checkbox("Hit protection");
            hitProtectionCheckbox.setAutoDescriptionEnabled(true);
            hitProtectionCheckbox.setChecked(plot.getConfig().getEnableHitProtection());
            hitProtectionCheckbox.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent event) {
                    try {
                        plot.getConfig().setEnableHitProtection(hitProtectionCheckbox.isChecked());
                    } catch (PersistenceException e) {
                        hitProtectionCheckbox.setChecked(!hitProtectionCheckbox.isChecked());
                        PlotPlus.prefix().append("Could not save plot configuration.").red().sendTo(player);
                    }
                }
            });
            view.addElement(hitProtectionCheckbox);
        }

        if (player.hasPermission("plotplus.user.editconfig.projectileprotection")) {
            final Checkbox projectileProtectionCheckbox = new Checkbox("Projectile protection");
            projectileProtectionCheckbox.setAutoDescriptionEnabled(true);
            projectileProtectionCheckbox.setChecked(plot.getConfig().getEnableProjectileProtection());
            projectileProtectionCheckbox.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent event) {
                    try {
                        plot.getConfig().setEnableHitProtection(projectileProtectionCheckbox.isChecked());
                    } catch (PersistenceException e) {
                        projectileProtectionCheckbox.setChecked(!projectileProtectionCheckbox.isChecked());
                        PlotPlus.prefix().append("Could not save plot configuration.").red().sendTo(player);
                    }
                }
            });
            view.addElement(projectileProtectionCheckbox);
        }

        if (player.hasPermission("plotplus.user.editconfig.welcomemessage")) {
            final Checkbox welcomeMessageCheckbox = new Checkbox("Welcome message");
            welcomeMessageCheckbox.setAutoDescriptionEnabled(true);
            welcomeMessageCheckbox.setChecked(plot.getConfig().getShowWelcomeMessage());
            welcomeMessageCheckbox.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent event) {
                    try {
                        plot.getConfig().setShowWelcomeMessage(welcomeMessageCheckbox.isChecked());
                    } catch (PersistenceException e) {
                        welcomeMessageCheckbox.setChecked(!welcomeMessageCheckbox.isChecked());
                        PlotPlus.prefix().append("Could not save plot configuration.").red().sendTo(player);
                    }
                }
            });
            view.addElement(welcomeMessageCheckbox);
        }

        viewManager.showView(player, view);
    }

    public void showHelpingOnPlotsMenu(final Player player) {
        View view = new PlotsView(ChatColor.BOLD + "Shared plots" + ChatColor.RESET, 4 * 9);
        for (final Plot plot : plugin.getWorld(player.getWorld()).getPlotsByHelper(player.getUniqueId())) {
            Button button;
            if (plot.getConfig().hasName()) {
                button = new Button(Material.CARPET, (byte) 5, "\"" + plot.getConfig().getName() + "\"");
                button.setDescription("Plot " + plot.getIdX() + ";" + plot.getIdZ() + " by " + plot.getOwner().getDisplayName());
            } else {
                button = new Button(Material.CARPET, (byte) 5, "Plot " + plot.getIdX() + ";" + plot.getIdZ());
                button.setDescription("by " + plot.getOwner().getDisplayName());
            }
            if (player.hasPermission("plotplus.user.teleport.others")) {
                button.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent event) {
                        plot.teleportPlayer(player);
                    }
                });
            }
            view.addElement(button);
        }

        viewManager.showView(player, view);
    }

    /**
     * Closes all views.
     */
    public void closeAll() {
        viewManager.closeAll();
    }

    private class PlotsView extends MultiPageView {
        public PlotsView(String title, int size) {
            super(title, size);
        }

        public PlotsView(String title, int size, int page) {
            super(title, size, page);
        }

        @Override
        protected Inventory createInventory() {
            if (getPage() == 0) {
                Button back = new Button(Material.SIGN, "Back");
                back.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent event) {
                        showPlotPlusMenu(getViewer());
                    }
                });
                insertElement(getSize(), back);
            }
            return super.createInventory();
        }
    }
}
