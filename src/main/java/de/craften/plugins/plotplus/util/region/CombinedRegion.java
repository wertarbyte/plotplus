package de.craften.plugins.plotplus.util.region;

import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CombinedRegion extends Region {
    private List<Region> regions = new ArrayList<>();

    public void add(Region region) {
        regions.add(region);
    }

    @Override
    public boolean contains(Location location) {
        for (Region r : regions)
            if (r.contains(location))
                return true;
        return false;
    }

    @Override
    public boolean contains(int x, int z) {
        for (Region r : regions)
            if (r.contains(x, z))
                return true;
        return false;
    }

    @Override
    public Region grow(int north, int east, int south, int west) {
        CombinedRegion newRegion = new CombinedRegion();
        for (Region region : regions)
            newRegion.add(region.grow(north, east, south, west));
        return newRegion;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        for (Region r : regions)
            b.append(r.toString());
        return b.toString();
    }

    @Override
    public Iterator<PointXZ> iterator() {
        return new CombinedRegionIterator();
    }

    private class CombinedRegionIterator implements Iterator<PointXZ> {
        private Iterator<PointXZ> currentRegionIterator = regions.size() > 0 ? regions.get(0).iterator() : null;
        private int regionIndex = 0;

        @Override
        public boolean hasNext() {
            return currentRegionIterator != null && (currentRegionIterator.hasNext() || regionIndex < regions.size() - 1);
        }

        @Override
        public PointXZ next() {
            if (currentRegionIterator.hasNext())
                return currentRegionIterator.next();
            currentRegionIterator = regions.get(++regionIndex).iterator();
            return currentRegionIterator.next();
        }

        @Override
        public void remove() {
        }
    }
}
