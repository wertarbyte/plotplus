package de.craften.plugins.plotplus.util.region;

import de.craften.plugins.plotplus.util.Direction;
import de.craften.plugins.plotplus.util.PointXZ;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * An edge finder for regions.
 */
public class EdgeFinder {
    private EdgeFinder() {
    }

    /**
     * Finds the edges of the given region and returns them in an order so that you can get the shape of the region
     * by connecting the edges. The first edge is equal to the last edge.
     *
     * @param region region to find the edges of
     * @return the edges of the region, sorted so that they can be connected to get the shape of the region
     */
    public static List<PointXZ> findEdges(Region region) {
        List<PointXZ> edges = new ArrayList<>();
        Set<PointXZ> border = new HashSet<>(region.getBorder());
        PointXZ start = border.iterator().next();
        int currentX = start.getX();
        int currentZ = start.getZ();
        Direction currentDirection = Direction.NORTH;

        //The following algorithm only works if we start at an edge, so let's find the first one!
        while (border.contains(new PointXZ(currentX, currentZ - 1))) {
            currentZ--;
        }
        while (border.contains(new PointXZ(currentX - 1, currentZ))) {
            currentX--;
        }
        start = new PointXZ(currentX, currentZ);

        while (!border.isEmpty()) {
            int oldX = currentX;
            int oldZ = currentZ;

            if (currentDirection == Direction.NORTH) {
                while (border.contains(new PointXZ(currentX, currentZ - 1))) {
                    if (currentX != start.getX() || currentZ != start.getZ())
                        border.remove(new PointXZ(currentX, currentZ));
                    currentZ--;
                }
            } else if (currentDirection == Direction.EAST) {
                while (border.contains(new PointXZ(currentX + 1, currentZ))) {
                    if (currentX != start.getX() || currentZ != start.getZ())
                        border.remove(new PointXZ(currentX, currentZ));
                    currentX++;
                }
            } else if (currentDirection == Direction.SOUTH) {
                while (border.contains(new PointXZ(currentX, currentZ + 1))) {
                    if (currentX != start.getX() || currentZ != start.getZ())
                        border.remove(new PointXZ(currentX, currentZ));
                    currentZ++;
                }
            } else { //WEST
                while (border.contains(new PointXZ(currentX - 1, currentZ))) {
                    if (currentX != start.getX() || currentZ != start.getZ())
                        border.remove(new PointXZ(currentX, currentZ));
                    currentX--;
                }
            }

            if (currentX != oldX || currentZ != oldZ) {
                PointXZ p = new PointXZ(currentX, currentZ);
                edges.add(p);
                border.remove(p);
            }
            currentDirection = currentDirection.nextClockwise();
        }

        edges.add(edges.get(0)); //first edge == last edge
        return edges;
    }
}
