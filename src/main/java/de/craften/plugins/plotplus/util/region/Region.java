package de.craften.plugins.plotplus.util.region;


import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Location;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Region implements Iterable<PointXZ> {
    public abstract boolean contains(Location location);

    public abstract boolean contains(int x, int z);

    /**
     * Gets the border of this region.
     *
     * @return The border of this region
     */
    public Set<PointXZ> getBorder() {
        //TODO test
        Set<PointXZ> border = new HashSet<>();
        for (PointXZ p : this) {
            boolean someInside = false;
            boolean someOutside = false;

            for (int x = -1; x <= 1; x++) {
                for (int z = -1; z <= 1; z++) {
                    if (x == 0 && z == 0) break;
                    if (contains(p.getX() + x, p.getZ() + z))
                        someInside = true;
                    else
                        someOutside = true;

                    if (someInside && someOutside)
                        break;
                }

                if (someInside && someOutside) {
                    border.add(p);
                    break;
                }
            }
        }
        return border;
    }

    public List<PointXZ> getEdges() {
        return EdgeFinder.findEdges(this);
    }

    public abstract Region grow(int north, int east, int south, int west);

    /**
     * Gets a list of positions of the chunks that contain this region.
     *
     * @return list of positions of the chunks that contain this region
     */
    public Set<PointXZ> getInvolvedChunks() {
        Set<PointXZ> chunks = new HashSet<>();
        for (PointXZ p : this) {
            chunks.add(new PointXZ(p.getX() >> 4, p.getZ() >> 4));
        }
        return chunks;
    }

    @Override
    public int hashCode() {
        return 42; //TODO Not a real hash code but it does what is needs to do and I have no idea how to implement this
    }

    @Override
    public boolean equals(Object obj) {
        //Two regions are equal if they contain the same points
        if (obj instanceof Region) {
            Set<PointXZ> thisRegion = new HashSet<>();
            for (PointXZ p : this)
                thisRegion.add(p);
            Set<PointXZ> otherRegion = new HashSet<>();
            for (PointXZ p : (Region) obj)
                otherRegion.add(p);
            for (PointXZ p : otherRegion) {
                if (!thisRegion.remove(p))
                    return false;
            }
            return thisRegion.isEmpty();
        }
        return false;
    }
}
