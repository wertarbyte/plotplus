package de.craften.plugins.plotplus.util;

public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;

    public de.craften.plugins.plotplus.util.Direction nextClockwise() {
        switch (this) {
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
        }
        return NORTH;
    }
}