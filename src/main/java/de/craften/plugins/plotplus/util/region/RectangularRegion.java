package de.craften.plugins.plotplus.util.region;

import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Location;

import java.util.*;

/**
 * A rectangular region defined by two corners.
 */
public class RectangularRegion extends Region {
    private int x1;
    private int z1;
    private int x2;
    private int z2;

    //corners are (x1,z1) and (x2,z2) where x1<=x2 and z1<=z2

    public RectangularRegion(PointXZ a, PointXZ b) {
        x1 = Math.min(a.getX(), b.getX());
        x2 = Math.max(a.getX(), b.getX());

        z1 = Math.min(a.getZ(), b.getZ());
        z2 = Math.max(a.getZ(), b.getZ());
    }

    public RectangularRegion(int x1, int z1, int x2, int z2) {
        this.x1 = Math.min(x1, x2);
        this.z1 = Math.min(z1, z2);
        this.x2 = Math.max(x1, x2);
        this.z2 = Math.max(z1, z2);
    }

    public static RectangularRegion byStartAndSize(int x, int z, int width, int length) {
        return new RectangularRegion(x, z, x + width - 1, z + length - 1);
    }

    @Override
    public boolean contains(Location location) {
        return x1 <= location.getBlockX() && x2 >= location.getBlockX()
                && z1 <= location.getBlockZ() && z2 >= location.getBlockZ();
    }

    public int getXMin() {
        return x1;
    }

    public int getXMax() {
        return x2;
    }

    public int getZMin() {
        return z1;
    }

    public int getZMax() {
        return z2;
    }

    @Override
    public boolean contains(int x, int z) {
        return x1 <= x && x2 >= x && z1 <= z && z2 >= z;
    }

    @Override
    public Iterator<PointXZ> iterator() {
        return new RectangularRegionIterator();
    }

    @Override
    public Set<PointXZ> getBorder() {
        //TODO test
        Set<PointXZ> border = new HashSet<>(2 * (getXMax() - getXMin() + getZMax() - getZMin())); //not exactly

        for (int x = getXMin(); x <= getXMax(); x++) {
            border.add(new PointXZ(x, getZMin()));
            border.add(new PointXZ(x, getZMax()));
        }

        for (int z = getZMin(); z <= getZMax(); z++) {
            border.add(new PointXZ(getXMin(), z));
            border.add(new PointXZ(getXMax(), z));
        }

        return border;
    }

    @Override
    public List<PointXZ> getEdges() {
        List<PointXZ> edges = new ArrayList<>(5);
        edges.add(new PointXZ(getXMin(), getZMin()));
        edges.add(new PointXZ(getXMax(), getZMin()));
        edges.add(new PointXZ(getXMax(), getZMax()));
        edges.add(new PointXZ(getXMin(), getZMax()));
        edges.add(new PointXZ(getXMin(), getZMin()));
        return edges;
    }

    @Override
    public Region grow(int north, int east, int south, int west) {
        return new RectangularRegion(getXMin() - west, getZMin() - north, getXMax() + east, getZMax() + south);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RectangularRegion) {
            RectangularRegion other = (RectangularRegion) obj;
            return other.getXMin() == getXMin() && other.getXMax() == getXMax() &&
                    other.getZMin() == getZMin() && other.getZMax() == getZMax();
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public String toString() {
        return String.format("RectangularRegion((%d, %d), (%d, %d))", getXMin(), getZMin(), getXMax(), getZMax());
    }

    private class RectangularRegionIterator implements Iterator<PointXZ> {
        private int x, z;

        public RectangularRegionIterator() {
            this.x = getXMin();
            this.z = getZMin();
        }

        @Override
        public boolean hasNext() {
            return z <= getZMax() && x <= getXMax();
        }

        @Override
        public PointXZ next() {
            PointXZ p = new PointXZ(x, z);
            if (x == getXMax()) {
                x = getXMin();
                z++;
            } else {
                x++;
            }
            return p;
        }

        @Override
        public void remove() {
        }
    }
}
