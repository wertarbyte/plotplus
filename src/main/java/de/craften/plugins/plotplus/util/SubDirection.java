package de.craften.plugins.plotplus.util;

public enum SubDirection {
    NORTH_EAST,
    SOUTH_EAST,
    SOUTH_WEST,
    NORTH_WEST;

    public SubDirection mirrorNorthSouth() {
        switch (this) {
            case NORTH_EAST:
                return SOUTH_EAST;
            case SOUTH_EAST:
                return NORTH_EAST;
            case SOUTH_WEST:
                return NORTH_WEST;
            case NORTH_WEST:
                return SOUTH_WEST;
            default:
                return this;
        }
    }

    public SubDirection mirrorWestEast() {
        switch (this) {
            case NORTH_EAST:
                return NORTH_WEST;
            case SOUTH_EAST:
                return SOUTH_WEST;
            case SOUTH_WEST:
                return SOUTH_EAST;
            case NORTH_WEST:
                return NORTH_EAST;
            default:
                return this;
        }
    }
}
