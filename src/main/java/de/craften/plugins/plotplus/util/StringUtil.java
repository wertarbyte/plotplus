package de.craften.plugins.plotplus.util;

public class StringUtil {
    /**
     * Joins the strings of the given array.
     *
     * @param input     The array to join
     * @param delimiter The delimeter that will be put between all strings
     * @return The joined strings with the delimeter, an empty string if the array is empty
     */
    public static String join(String[] input, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String value : input) {
            sb.append(value);
            sb.append(delimiter);
        }
        int length = sb.length();
        if (length > 0) {
            // Remove the extra delimiter
            sb.setLength(length - delimiter.length());
        }
        return sb.toString();
    }
}
