package de.craften.plugins.plotplus.util;

public interface Callback<U> {
    public void call(U parameter);
}