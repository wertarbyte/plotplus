package de.craften.plugins.plotplus.util;


public class PointXZ {
    private int x;
    private int z;

    public PointXZ(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public PointXZ(PointXZ point) {
        this.x = point.x;
        this.z = point.z;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    @Override
    public int hashCode() {
        return getX() * 17 + getZ() * 31;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PointXZ) {
            PointXZ other = (PointXZ) obj;
            return other.getX() == getX() && other.getZ() == getZ();
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", x, z);
    }
}
