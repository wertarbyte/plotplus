package de.craften.plugins.plotplus.util;

import org.bukkit.World;

/**
 * Utility methods for bukkit worlds.
 */
public class WorldUtil {
    private WorldUtil() {
    }

    /**
     * Regenerates and repopulates the given chunk.
     *
     * @param world The world the chunk is in
     * @param x     The x coordinate of the chunk
     * @param z     The z coordinate of the chunk
     */
    public static void resetChunk(World world, int x, int z) {
        world.regenerateChunk(x, z);
        world.getChunkAt(x, z).unload(true, false);
        for (int cx = x - 1; cx <= x + 1; cx++)
            for (int cz = z - 1; cz <= z + 1; cz++)
                world.getChunkAt(cx, cz);
        world.getChunkAt(x, z);
    }

    public static int topmostSafeLocation(World world, double x, double z) {
        for (int y = world.getMaxHeight(); y >= 0; y--) {
            if (world.getBlockAt((int) x, y, (int) z).getType().isSolid())
                return y + 1;
        }
        return world.getMaxHeight();
    }
}
