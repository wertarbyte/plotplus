package de.craften.plugins.plotplus.util;

/**
 * Utility methods for enums
 */
public class EnumUtil {
    private EnumUtil() {
    }

    /**
     * Returns the enum value for the given string or <code>fallback</code> if it's not found.
     *
     * @param enumClass The enum class to look up
     * @param value     The value to get
     * @param fallback  The fallback value
     * @param <E>       The enum class
     * @return The enum value for the given string or <code>fallback</code> if it's not found
     */
    public static <E extends Enum<E>> E getOrDefault(Class<E> enumClass, String value, E fallback) {
        if (value == null)
            return fallback;
        try {
            return Enum.valueOf(enumClass, value);
        } catch (IllegalArgumentException e) {
            return fallback;
        }
    }
}
