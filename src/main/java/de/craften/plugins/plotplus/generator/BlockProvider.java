package de.craften.plugins.plotplus.generator;

import de.craften.plugins.plotplus.generator.blocks.Block;

public interface BlockProvider {
    public Block getBlockAt(int x, int y, int z);
}
