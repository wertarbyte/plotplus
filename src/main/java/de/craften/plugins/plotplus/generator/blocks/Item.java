package de.craften.plugins.plotplus.generator.blocks;

/**
 * Represents an item. This doesn't need to be a real 'item', but anything that has an id and data.
 */
public class Item {
    private short id;
    private byte data;

    public Item(int id, int data) {
        this.id = (short) id;
        this.data = (byte) data;
    }

    public Item(String idColonData) {
        if (idColonData.matches("\\d+(:\\d+)?")) {
            String[] parts = idColonData.split(":");
            id = (short) Integer.parseInt(parts[0]);
            data = parts.length == 2 ? (byte) Integer.parseInt(parts[1]) : 0;
        } else
            throw new IllegalArgumentException("String has illegal format, expected 'id:data'");
    }

    public short getId() {
        return id;
    }

    public byte getData() {
        return data;
    }

    @Override
    public int hashCode() {
        return id * 13 + data;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Item) {
            Item other = (Item) obj;
            return other.id == id && other.data == data;
        }
        return false;
    }
}
