package de.craften.plugins.plotplus.generator;

import de.craften.plugins.plotplus.util.PointXZ;

public abstract class PlotStructure {
    /**
     * Gets the width of this structure.
     * @return The width of this structure
     */
    public abstract int getWidth();

    /**
     * Gets the length of this structure.
     * @return The length of this structure
     */
    public abstract int getLength();

    /**
     * Converts the given absolute coordinate to a coordinate relative to a plot. Assumes that the first plot
     * with this structure is at (0,0), only this structure is used and there are no gaps.
     *
     * @param x X coordinate of the absolute position
     * @param z Z coordinate of the absolute position
     * @return Position relative to a plot
     */
    public PointXZ getRelativePosition(int x, int z) {
        return new PointXZ(
                x >= 0 ? (x % getWidth()) : (x % getWidth() + getWidth()) % getWidth(),
                z >= 0 ? (z % getLength()) : (z % getLength() + getLength()) % getLength()
        );
    }
}
