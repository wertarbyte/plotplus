package de.craften.plugins.plotplus.generator;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PlotWorldGenerator extends ChunkGenerator {
    static PlotWorld getWorld(World world) {
        return ((PlotPlus) Bukkit.getPluginManager().getPlugin("PlotPlus")).getWorld(world);
    }

    static BlockProvider getBlockProvider(PlotWorld world) {
        PlotStructure structure = world.getScheme().getStructure();
        if (structure instanceof BlockProvider) {
            return (BlockProvider) structure;
        } else {
            Bukkit.getLogger().severe(world.getName() + " is not configured as schematic plot world!" +
                    "Please fix your config.");
            Bukkit.getServer().shutdown();
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public short[][] generateExtBlockSections(World world, Random random, int cx, int cz, BiomeGrid biomes) {
        PlotWorld plotWorld = getWorld(world);
        BlockProvider provider = getBlockProvider(plotWorld);
        short[][] result = new short[world.getMaxHeight() / 16][];

        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = 0; y < world.getMaxHeight(); y++) {
                    Block m = provider.getBlockAt(x + cx * 16, y, z + cz * 16);
                    if (m != null) {
                        setBlock(result, x, y, z, m.getId());
                    } else {
                        setBlock(result, x, y, z, (short) 0);
                    }
                }
                biomes.setBiome(x, z, plotWorld.getConfig().getDefaultBiome());
            }
        }

        return result;
    }

    void setBlock(short[][] result, int x, int y, int z, short id) {
        // is this chunk part already initialized?
        if (result[y >> 4] == null) {
            // Initialize the chunk part
            result[y >> 4] = new short[4096];
        }
        // set the blocks (look above, how this is done)
        result[y >> 4][((y & 0xF) << 8) | (z << 4) | x] = id;
    }

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        return Arrays.asList((BlockPopulator) new PlotWorldPopulator());
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random) {
        if (!world.isChunkLoaded(0, 0)) {
            world.loadChunk(0, 0);
        }

        if ((world.getHighestBlockYAt(0, 0) <= 0) && (world.getBlockAt(0, 0, 0).getType() == Material.AIR)) {
            return new Location(world, 0, 64, 0); // Lets allow people to drop a little before hitting the void then shall we?
        }

        return new Location(world, 0, world.getHighestBlockYAt(0, 0), 0);
    }
}
