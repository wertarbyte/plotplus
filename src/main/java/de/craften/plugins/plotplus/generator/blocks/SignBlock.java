package de.craften.plugins.plotplus.generator.blocks;

import de.craften.plugins.plotplus.PlotPlus;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

import java.util.Arrays;

public class SignBlock extends Block {
    private String[] lines;

    public SignBlock(int id, int data, String... lines) {
        super(id, data);
        this.lines = lines;
    }

    /**
     * Creates an empty sign block.
     *
     * @param id
     * @param data
     */
    public SignBlock(int id, int data) {
        super(id, data);
        lines = new String[4];
    }

    @Override
    public void applyOn(BlockState oldBlock) {
        super.applyOn(oldBlock);
        
        BlockState block = oldBlock.getLocation().getBlock().getState();
        if (block instanceof Sign) {
            Sign sign = (Sign) block;
            for (int i = 0; i < 4; i++) {
                if (lines.length > i)
                    sign.setLine(i, lines[i]);
                else
                    sign.setLine(i, "");
            }
            sign.update(true);
        } else {
            PlotPlus.getPlugin(PlotPlus.class).getLogger().warning("Could not set sign content at " + block.getLocation());
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 17 * Arrays.hashCode(lines);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SignBlock && super.equals(obj) && Arrays.equals(((SignBlock) obj).lines, lines);
    }
}
