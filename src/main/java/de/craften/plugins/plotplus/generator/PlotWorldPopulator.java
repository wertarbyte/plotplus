package de.craften.plugins.plotplus.generator;

import de.craften.plugins.plotplus.generator.blocks.Block;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

import java.util.Random;

public class PlotWorldPopulator extends BlockPopulator {
    @Override
    public void populate(World world, Random random, Chunk chunk) {
        BlockProvider provider = PlotWorldGenerator.getBlockProvider(PlotWorldGenerator.getWorld(world));
        for (int x = 0; x < 16; x++)
            for (int y = 0; y < world.getMaxHeight(); y++)
                for (int z = 0; z < 16; z++) {
                    org.bukkit.block.Block block = chunk.getBlock(x, y, z);
                    Block b = provider.getBlockAt(block.getX(), block.getY(), block.getZ());
                    if (b != null)
                        b.applyOn(block);
                }
    }
}
