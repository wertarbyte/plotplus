package de.craften.plugins.plotplus.generator.blocks;

import org.bukkit.Material;
import org.bukkit.block.BlockState;

/**
 * Represents a block configuration that can be applied on a block.
 */
public class Block extends Item {
    public static final Block AIR = new Block(Material.AIR);

    public Block(Material block) {
        this(block.getId());
    }

    public Block(int id, int data) {
        super(id, data);
    }

    public Block(int id) {
        super(id, 0);
    }

    public Block(String idColonData) {
        super(idColonData);
    }

    public void applyOn(BlockState block) {
        block.setTypeId(getId());
        block.setRawData(getData());
        block.update(true);
    }

    public final void applyOn(org.bukkit.block.Block blockAt) {
        applyOn(blockAt.getState());
    }
}
