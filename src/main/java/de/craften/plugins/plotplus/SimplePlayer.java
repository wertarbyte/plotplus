package de.craften.plugins.plotplus;

import java.util.UUID;

/**
 * A simple player.
 */
public class SimplePlayer {
    private UUID uuid;
    protected String displayName;

    /**
     * Creates a new player instance.
     *
     * @param uuid        UUID of the player
     * @param displayName Display name of the player (i.e. the nickname)
     */
    public SimplePlayer(UUID uuid, String displayName) {
        this.uuid = uuid;
        this.displayName = displayName;
    }

    /**
     * Gets the UUID of this player.
     *
     * @return The UUID of this player
     */
    public UUID getUniqueId() {
        return uuid;
    }

    /**
     * Gets the display name of this player.
     *
     * @return The display name of this player
     */
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode() + 13 * displayName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SimplePlayer) {
            SimplePlayer other = (SimplePlayer) obj;
            return other.uuid.equals(uuid) &&
                    ((other.displayName != null && displayName != null && other.displayName.equals(displayName))
                            || (displayName == null && other.displayName == null));
        }
        return false;
    }
}
