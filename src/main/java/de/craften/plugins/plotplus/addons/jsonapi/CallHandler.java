package de.craften.plugins.plotplus.addons.jsonapi;

import com.alecgorge.minecraft.jsonapi.api.APIMethodName;
import com.alecgorge.minecraft.jsonapi.api.JSONAPICallHandler;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Call handler for the JSON API.
 */
public class CallHandler implements JSONAPICallHandler {
    private PlotPlus plugin;

    public CallHandler(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean willHandle(APIMethodName apiMethodName) {
        if (!apiMethodName.getNamespace().equals("plotplus"))
            return false;

        if (apiMethodName.getMethodName().equals("plots"))
            return true;
        if (apiMethodName.getMethodName().equals("player"))
            return true;

        return false;
    }

    @Override
    public Object handle(APIMethodName apiMethodName, Object[] objects) {
        if (apiMethodName.getMethodName().equals("plots")) {
            if (objects.length == 1) {
                PlotWorld world = plugin.getWorld(objects[0].toString());
                if (world != null) {
                    return plotsToJson(world.getPlots());
                }
            }
        } else if (apiMethodName.getMethodName().equals("player")) {
            Map<String, Object> result = new HashMap<>();
            result.put("plots", plotsToJson(plugin.getManager().getPlotsByPlayer(objects[0].toString())));
            result.put("helpingOn", plotsToJson(plugin.getManager().getPlotsByHelper(objects[0].toString())));
            return result;
        }

        return null;
    }

    private Object toJson(Plot plot) {
        Map<String, Object> jsonPlot = new HashMap<>();
        jsonPlot.put("idX", plot.getIdX());
        jsonPlot.put("idZ", plot.getIdZ());
        jsonPlot.put("world", plot.getWorld().getName());
        jsonPlot.put("owner", toJson(plot.getOwner()));
        jsonPlot.put("helpers", playersToJson(plot.getHelpers()));
        jsonPlot.put("blocked", playersToJson(plot.getBlockedPlayers()));
        return jsonPlot;
    }

    private Object plotsToJson(Iterable<Plot> plots) {
        List<Object> jsonPlots = new ArrayList<>();
        for (Plot p : plots)
            jsonPlots.add(toJson(p));
        return jsonPlots;
    }

    private Object toJson(SimplePlayer player) {
        Map<String, Object> jsonPlayer = new HashMap<>();
        jsonPlayer.put("nickname", player.getDisplayName());
        jsonPlayer.put("uuid", player.getUniqueId().toString());
        return jsonPlayer;
    }

    private Object playersToJson(Iterable<SimplePlayer> players) {
        List<Object> jsonPlayers = new ArrayList<>();
        for (SimplePlayer p : players)
            jsonPlayers.add(toJson(p));
        return jsonPlayers;
    }
}
