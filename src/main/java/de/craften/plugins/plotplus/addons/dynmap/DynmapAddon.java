package de.craften.plugins.plotplus.addons.dynmap;


import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.Addon;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import org.bukkit.plugin.Plugin;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynmapAddon extends Addon {
    private Plugin dynmap;
    private DynmapAPI api;
    private MarkerAPI markerApi;

    private MarkerSet markers;
    private Map<String, AreaMarker> plotAreas = new HashMap<>();

    public DynmapAddon() {
        super("dynmap-layer-addon", "dynmap");
    }

    /**
     * Enables the dynmap layer for all worlds.
     */
    @Override
    protected void onEnabled() {
        dynmap = getPlugin().getServer().getPluginManager().getPlugin("dynmap");
        api = (DynmapAPI) dynmap;
        markerApi = api.getMarkerAPI();

        markers = markerApi.getMarkerSet("plotplus.markerset");
        if (markers == null)
            markers = markerApi.createMarkerSet("plotplus.markerset", "PlotPlus", null, false);
        else
            markers.setMarkerSetLabel("PlotPlus");
        markers.setHideByDefault(false);

        getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(getPlugin(), new Updater(), 20 * 30); //30 seconds
    }

    @Override
    protected void onDisabling() {
        dynmap = null;
        api = null;
        markerApi = null;
        markers = null;
        plotAreas = new HashMap<>();
    }

    /**
     * Updates the plot layers for all worlds.
     */
    private class Updater implements Runnable {
        @Override
        public void run() {
            Map<String, AreaMarker> newmap = new HashMap<>(); /* Build new map */

            for (PlotWorld world : getPlugin().getWorlds()) {
                for (Plot plot : world.getPlots()) {
                    handlePlot(plot, newmap);
                }
            }

            //every marker that's not yet removed from plotAreas doesn't exist anymore (i.e. deleted plots)
            for (AreaMarker oldm : plotAreas.values())
                oldm.deleteMarker();

            plotAreas = newmap;

            getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(getPlugin(), this, 20 * 60 * 10); //10 minutes
        }

        /**
         * Creates or updates the marker area for the given plot and inserts it into <code>newmap</code>.
         * This will remove the marker area from <code>plotAreas</code>.
         *
         * @param plot   Plot to create or update the marker area for
         * @param newmap Map to insert the area marker into
         */
        private void handlePlot(Plot plot, Map<String, AreaMarker> newmap) {
            String id = plot.getWorld().getName() + ";" + plot.getIdX() + ";" + plot.getIdZ();

            if (plot.isConnected()) {
                List<PointXZ> edges = plot.getConnectedBuildRegion().grow(0, 1, 1, 0).getEdges();
                double[] x = new double[edges.size()];
                double[] z = new double[edges.size()];
                for (int i = 0; i < edges.size(); i++) {
                    x[i] = edges.get(i).getX();
                    z[i] = edges.get(i).getZ();
                }

                AreaMarker m1 = plotAreas.remove(id);
                if (m1 == null) {
                    m1 = markers.createAreaMarker(id, plot.getIdX() + ";" + plot.getIdZ(), false, plot.getWorld().getName(), x, z, false);
                    if (m1 == null) {
                        return;
                    }
                } else {
                    //area already exists
                    m1.setCornerLocations(x, z);
                }
                //set line and fill properties
                m1.setLineStyle(2, 1, 0xff0000);
                m1.setFillStyle(0, 0xff0000);
                newmap.put(id, m1);

                RectangularRegion region = plot.getCoreBuildRegion();
                x = new double[]{region.getXMin(), region.getXMax() + 1, region.getXMax() + 1, region.getXMin(), region.getXMin()};
                z = new double[]{region.getZMin(), region.getZMin(), region.getZMax() + 1, region.getZMax() + 1, region.getZMin()};

                AreaMarker m = plotAreas.remove(id + "_2");
                if (m == null) {
                    m = markers.createAreaMarker(id + "_2", plot.getIdX() + ";" + plot.getIdZ(), false, plot.getWorld().getName(), x, z, false);
                    if (m == null) {
                        return;
                    }
                } else {
                    //area already exists
                    m.setCornerLocations(x, z);
                }
                //set line and fill properties
                m.setLineStyle(0, 0, 0xff0000);
                m.setFillStyle(0.001, 0xff0000); //'hack' to make it clickable
                m.setDescription(getDescription(plot));
                newmap.put(id + "_2", m);
            } else {
                RectangularRegion region = plot.getCoreBuildRegion();
                double[] x = new double[]{region.getXMin(), region.getXMax() + 1, region.getXMax() + 1, region.getXMin(), region.getXMin()};
                double[] z = new double[]{region.getZMin(), region.getZMin(), region.getZMax() + 1, region.getZMax() + 1, region.getZMin()};

                AreaMarker m = plotAreas.remove(id + "_2");
                if (m == null) {
                    m = markers.createAreaMarker(id + "_2", plot.getIdX() + ";" + plot.getIdZ(), false, plot.getWorld().getName(), x, z, false);
                    if (m == null) {
                        return;
                    }
                } else {
                    //area already exists
                    m.setCornerLocations(x, z);
                }
                //set line and fill properties
                m.setLineStyle(2, 1, 0xff0000);
                m.setFillStyle(0.001, 0xff0000); //'hack' to make it clickable
                m.setDescription(getDescription(plot));
                newmap.put(id + "_2", m);

                newmap.remove(id);
            }
        }
    }

    private String getDescription(Plot plot) {
        StringBuilder description = new StringBuilder();
        description.append("<div>Plot ").append(plot.getIdX()).append('/').append(plot.getIdZ());
        if (plot.getConfig().hasName()) {
            description.append("<br/>&quot;").append(plot.getConfig().getName()).append("&quot;");
        }
        description.append("<br/>Owner: ").append(plot.getOwner().getDisplayName());
        if (!plot.getHelpers().isEmpty()) {
            description.append("<br/>Helpers: ");
            boolean first = true;
            for (SimplePlayer helper : plot.getHelpers()) {
                if (first) {
                    first = false;
                } else {
                    description.append(", ");
                }
                description.append(helper.getDisplayName());
            }
        }
        description.append("</div>");
        return description.toString();
    }
}
