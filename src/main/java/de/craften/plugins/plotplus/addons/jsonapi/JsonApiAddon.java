package de.craften.plugins.plotplus.addons.jsonapi;

import com.alecgorge.minecraft.jsonapi.JSONAPI;
import de.craften.plugins.plotplus.api.Addon;

/**
 * Provides JSONAPI methods to access PlotPlus information.
 */
public class JsonApiAddon extends Addon {
    public JsonApiAddon() {
        super("jsonapi-addon", "JSONAPI");
    }

    @Override
    protected void onEnabled() {
        try {
            JSONAPI jsonapi = (JSONAPI) getPlugin().getServer().getPluginManager().getPlugin("JSONAPI");
            jsonapi.registerAPICallHandler(new CallHandler(getPlugin()));
        } catch (NoClassDefFoundError e) {
            disable();
        }
    }

    @Override
    protected void onDisabling() {
    }
}
