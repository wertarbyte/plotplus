package de.craften.plugins.plotplus.economy;

public enum PaymentResult {
    Success,
    NotEnoughMoney,
    Failed,
    Reversed
}
