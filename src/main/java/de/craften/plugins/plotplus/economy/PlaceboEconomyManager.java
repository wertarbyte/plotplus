package de.craften.plugins.plotplus.economy;

import org.bukkit.OfflinePlayer;

import java.util.concurrent.Callable;

/**
 * An economy manager to use if there is no economy.
 */
public class PlaceboEconomyManager implements EconomyManager {

    @Override
    public PaymentResult withdrawPlayer(double cost, OfflinePlayer player, Callable<Boolean> performer) {
        try {
            performer.call();
        } catch (Exception ignore) {
        }
        return PaymentResult.Success;
    }

    @Override
    public String formatAmount(double amount) {
        return Double.toString(amount);
    }
}
