package de.craften.plugins.plotplus.economy;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;

import java.util.concurrent.Callable;

/**
 * Economy manager for PlotPlus. Conveniently manages payments, if enabled and gives money back on failures.
 */
public class VaultEconomyManager implements EconomyManager {
    private Economy economy;

    public VaultEconomyManager(Economy economy) {
        this.economy = economy;
    }

    @Override
    public PaymentResult withdrawPlayer(double cost, OfflinePlayer player, Callable<Boolean> performer) {
        if (cost == 0) {
            try {
                performer.call();
            } catch (Exception ignore) {
            }
            return PaymentResult.Success;
        } else if (economy.has(player, cost)) {
            EconomyResponse resp = economy.withdrawPlayer(player, cost);
            if (resp.transactionSuccess()) {
                try {
                    if (performer.call()) {
                        return PaymentResult.Success;
                    } else {
                        economy.depositPlayer(player, cost);
                        return PaymentResult.Reversed;
                    }
                } catch (Exception e) {
                    economy.depositPlayer(player, cost);
                    return PaymentResult.Reversed;
                }
            } else {
                return PaymentResult.Failed;
            }
        } else {
            return PaymentResult.NotEnoughMoney;
        }
    }

    @Override
    public String formatAmount(double amount) {
        return economy.format(amount);
    }
}
