package de.craften.plugins.plotplus.economy;


import org.bukkit.OfflinePlayer;

import java.util.concurrent.Callable;

public interface EconomyManager {
    public PaymentResult withdrawPlayer(double cost, OfflinePlayer player, Callable<Boolean> performer);

    public String formatAmount(double amount);
}
