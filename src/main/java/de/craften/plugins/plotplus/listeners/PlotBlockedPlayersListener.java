package de.craften.plugins.plotplus.listeners;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.plot.RestrictedArea;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.LocationUtil;
import de.craften.plugins.plotplus.util.WorldUtil;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Listener that handles events of players moving on plots they're blocked on.
 */
public class PlotBlockedPlayersListener implements Listener {
    private PlotPlus plugin;

    public PlotBlockedPlayersListener(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.bypassblocking")) {
            Location loc = event.getTo();
            RestrictedArea plot = world.getEffectivePlotAt(loc.getBlock().getLocation());
            if (plot != null && !plot.mayEnter(p)) {
                event.setCancelled(true);
                switch (LocationUtil.yawToFace(loc.getYaw(), false)) {
                    case NORTH:
                        loc.setZ(loc.getZ() + world.getScheme().getStructure().getLength());
                        break;
                    case EAST:
                        loc.setX(loc.getX() - world.getScheme().getStructure().getWidth());
                        break;
                    case SOUTH:
                        loc.setZ(loc.getZ() - world.getScheme().getStructure().getLength());
                        break;
                    case WEST:
                        loc.setX(loc.getX() + world.getScheme().getStructure().getWidth());
                        break;
                }
                loc.setY(WorldUtil.topmostSafeLocation(world.getBukkitWorld(), loc.getX(), loc.getZ()));
                p.teleport(loc);
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 1.0f, 1.0f);
                PlotPlus.prefix().append("You may not enter this plot.").red().sendTo(p);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.bypassblocking")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getTo());
            if (plot != null && !plot.mayEnter(p)) {
                event.setCancelled(true);
                PlotPlus.prefix().append("You may not enter the plot you want to teleport to.").red().sendTo(p);
            }
        }
    }
}
