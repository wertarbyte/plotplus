package de.craften.plugins.plotplus.listeners;

import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlotVisitListener implements Listener {
    private PlotPlus plugin;

    public PlotVisitListener(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.isCancelled()) return;

        PlotWorld world = plugin.getWorld(event.getTo().getWorld());
        if (world != null) {
            Plot previousPlot = world.getPlotAt(event.getFrom().getBlock().getLocation());
            Plot visitedPlot = world.getPlotAt(event.getTo().getBlock().getLocation());
            if ((visitedPlot != null && visitedPlot.getConfig().getShowWelcomeMessage()
                    && !event.getPlayer().hasPermission("plotplus.disablewelcomemessage") && !visitedPlot.equals(previousPlot))) {
                String name = visitedPlot.getConfig().getName() != null ? visitedPlot.getConfig().getName() : "";
                TextBuilder msg = PlotPlus.prefix()
                        .append("Plot " + visitedPlot.getIdX() + ";" + visitedPlot.getIdZ()).darkGreen();
                if (visitedPlot.getConfig().hasName())
                    msg.append(" \"").gray().append(name).darkGreen().append("\"").gray();
                msg.append(" by ").gray()
                        .append(visitedPlot.getOwner().getDisplayName()).darkGreen()
                        .sendTo(event.getPlayer());
            }
        }
    }
}
