package de.craften.plugins.plotplus.listeners;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.plot.RestrictedArea;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.List;

/**
 * Handles the normal plot protection.
 */
public class PlotProtectionListener implements Listener {
    private PlotPlus plugin;

    public PlotProtectionListener(PlotPlus plugin) {
        this.plugin = plugin;
    }

    /*
     * Blocks
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPlace(BlockPlaceEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());
        Block block = event.getBlockPlaced();

        if (world != null) {
            if (world.getConfig().isBlocked(block) && !p.hasPermission("plotplus.admin.buildanything")) {
                event.setCancelled(true);
                return;
            }

            if (!p.hasPermission("plotplus.admin.buildanywhere")) {
                RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
                if (plot == null || !plot.mayBuild(p))
                    event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null || !plot.mayBuild(p))
                event.setCancelled(true);
        }
    }

    /*
     * Buckets
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            Block block = event.getBlockClicked().getRelative(event.getBlockFace());
            RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
            if (plot == null || !plot.mayBuild(p))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlockClicked().getLocation());
            if (plot == null || !plot.mayBuild(p))
                event.setCancelled(true);
        }
    }

    /*
     * Interaction with blocks
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());
        if (event.getClickedBlock() != null && event.getBlockFace() != null) {
            Block block = event.getClickedBlock();

            if (world != null) {
                //Protect cake eating (resolves PP-79)
                if (event.getClickedBlock().getType() == Material.CAKE_BLOCK) {
                    if (!p.hasPermission("plotplus.admin.buildanywhere")) {
                        RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
                        if (plot == null || !plot.mayBuild(p)) {
                            event.setUseInteractedBlock(Event.Result.DENY);
                            return;
                        }
                    }
                }

                // Allow players to destroy ender portal blocks (resolves PP-86)
                if (event.getAction() == Action.LEFT_CLICK_BLOCK &&
                        event.getClickedBlock().getType() == Material.ENDER_PORTAL_FRAME &&
                        p.getGameMode() == GameMode.SURVIVAL) {
                    RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
                    if ((plot != null && plot.mayBuild(p)) || p.hasPermission("plotplus.admin.buildanywhere")) {
                        if ((block.getData() & 0x4) != 0) { //if filled
                            block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.EYE_OF_ENDER, 1));
                        }
                        block.breakNaturally();
                        block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.ENDER_PORTAL_FRAME, 1));
                        return;
                    }
                }


                if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (event.isBlockInHand()) {
                        block = block.getRelative(event.getBlockFace());
                        if (world.getConfig().isBlocked(block) && !p.hasPermission("plotplus.admin.buildanything")) {
                            event.setCancelled(true);
                            return;
                        }

                        RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
                        if (!p.hasPermission("plotplus.admin.buildanywhere")) {
                            if (plot == null || !plot.mayBuild(p)) {
                                event.setCancelled(true);
                            }
                        }
                    } else {
                        ItemStack item = event.getItem();
                        if (item != null) {
                            if (world.getConfig().isBlocked(item) && !p.hasPermission("plotplus.admin.buildanything")) {
                                event.setUseInteractedBlock(Event.Result.DENY);
                                return;
                            }

                            RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
                            if (!p.hasPermission("plotplus.admin.buildanywhere")) {
                                if (plot == null || !plot.mayBuild(p)) {
                                    event.setUseInteractedBlock(Event.Result.DENY);
                                }
                            }
                        }
                    }
                } else if (event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.SOIL) {
                    RestrictedArea plot = world.getEffectivePlotAt(block.getLocation());
                    if (!p.hasPermission("plotplus.admin.buildanywhere")) {
                        if (plot == null || !plot.mayBuild(p)) {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    /*
     * Prevent spreading, damaging, moving by pistions, growing etc. outside plots
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockSpread(BlockSpreadEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null)
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockForm(BlockFormEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null)
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockDamage(BlockDamageEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null && !event.getPlayer().hasPermission("plotplus.admin.buildanywhere"))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockFade(BlockFadeEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null)
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockFromTo(BlockFromToEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null)
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockGrow(BlockGrowEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null)
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());

        if (world != null) {
            final RestrictedArea first = world.getEffectivePlotAt(event.getBlock().getLocation());
            for (Block b : event.getBlocks()) {
                RestrictedArea plot = world.getEffectivePlotAt(b.getRelative(event.getDirection()).getLocation());
                if (plot == null || !plot.getOwner().equals(first.getOwner())) {
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());

        if (world != null) {
            final RestrictedArea first = world.getEffectivePlotAt(event.getBlock().getLocation());
            for (Block b : event.getBlocks()) {
                RestrictedArea plot = world.getEffectivePlotAt(b.getLocation());
                if (plot == null || !plot.getOwner().equals(first.getOwner())) {
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onStructureGrow(StructureGrowEvent event) {
        List<BlockState> blocks = event.getBlocks();
        boolean found = false;

        for (int i = 0; i < blocks.size(); i++) {
            PlotWorld world = plugin.getWorld(blocks.get(i).getWorld());
            if (found || world != null) {
                found = true;
                RestrictedArea plot = world.getEffectivePlotAt(blocks.get(i).getLocation());
                if (plot == null) {
                    blocks.remove(i);
                    i--;
                }
            }
        }
    }

    /*
     * Explosions and fire
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityExplode(EntityExplodeEvent event) {
        PlotWorld world = plugin.getWorld(event.getLocation().getWorld());
        if (world != null && world.getConfig().isEntityExplosionDisabled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockIgnite(BlockIgniteEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getLocation().getWorld());
        if (world != null) {
            if (event.getCause() == BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL && !world.getConfig().isFlintAndSteelEnabled()) {
                event.setCancelled(true);
                return;
            } else if (event.getCause() != BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL && world.getConfig().isIgnitionDisabled()) {
                event.setCancelled(true);
                return;
            } else if (event.getCause() == BlockIgniteEvent.IgniteCause.SPREAD && world.getConfig().isFireSpreadingDisabled()) {
                event.setCancelled(true);
                return;
            }

            Player p = event.getPlayer();
            if (p != null) {
                RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
                if (plot == null)
                    event.setCancelled(true);
                else if (!plot.mayBuild(p) && !p.hasPermission("plotplus.admin.buildanywhere"))
                    event.setCancelled(true);
            }
        }
    }

    /*
     * Entities
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onHangingPlace(HangingPlaceEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
            if (plot == null || !plot.mayBuild(p)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        Entity entity = event.getRemover();
        if (entity instanceof Player) {
            Player p = (Player) entity;
            PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

            if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
                RestrictedArea plot = world.getEffectivePlotAt(event.getEntity().getLocation());
                if (plot == null || !plot.mayBuild(p))
                    event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        Player p = event.getPlayer();

        //Players may always interact with their own animals
        if (event.getRightClicked() instanceof Tameable &&
                ((Tameable) event.getRightClicked()).getOwner() != null &&
                ((Tameable) event.getRightClicked()).getOwner().equals(p)) {
            return;
        }

        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());
        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getRightClicked().getLocation());
            if (event.getRightClicked() instanceof Villager && p.hasPermission("plotplus.user.interact.villager.any")) {
                return;
            } else if (event.getRightClicked() instanceof Minecart && p.hasPermission("plotplus.user.interact.minecart.any")) {
                return;
            }
            if (plot == null || !plot.mayBuild(p)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntityInteract(EntityInteractEvent event) {
        //Farmland protection for horses/pigs (PP-74)
        if (event.getBlock().getType() == Material.SOIL) {
            Entity passenger = event.getEntity().getPassenger();
            if (passenger instanceof Player) {
                Player p = (Player) passenger;

                PlotWorld world = plugin.getWorld(p.getLocation().getWorld());
                if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
                    RestrictedArea plot = world.getEffectivePlotAt(event.getBlock().getLocation());
                    if (plot == null || !plot.mayBuild(p))
                        event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player p = (Player) event.getDamager();

            //Players may always interact with their own animals
            if (event.getEntity() instanceof Tameable &&
                    ((Tameable) event.getEntity()).getOwner() != null &&
                    ((Tameable) event.getEntity()).getOwner().equals(p))
                return;

            //Players may only kill/hurt entities on plots they own
            PlotWorld world = plugin.getWorld(p.getLocation().getWorld());
            if (world != null && world.getConfig().isHitProtectionEnabled()
                    && !p.hasPermission("plotplus.admin.buildanywhere")) {
                RestrictedArea plot = world.getEffectivePlotAt(event.getEntity().getLocation());
                if (plot != null && !(plot.mayHitEntities() || plot.mayBuild(p))) {
                    event.setCancelled(true);
                }
            }
        } else if (event.getDamager() instanceof Projectile) {
            //Prevent players from killing other player's animals or shooting items out of frames using bows.
            ProjectileSource source = ((Projectile) event.getDamager()).getShooter();
            if (source instanceof Player) {
                Player p = (Player) source;
                PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

                if (world != null && world.getConfig().isProjectileProtectionEnabled()
                        && !p.hasPermission("plotplus.admin.buildanywhere")) {
                    RestrictedArea plot = world.getEffectivePlotAt(event.getEntity().getLocation());
                    if (plot != null && !(plot.mayHitEntitiesWithProjectiles() || plot.mayBuild(p))) {
                        event.setDamage(0);
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getEntity().getLocation());
            if (plot == null || !plot.mayBuild(p))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerEggThrow(PlayerEggThrowEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getEgg().getLocation());
            if (plot == null || !plot.mayBuild(p))
                event.setHatching(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockDispense(BlockDispenseEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null && event.getBlock().getType() == Material.DISPENSER
                && world.getConfig().isBlocked(event.getItem())) {
            //block forbidden items to be dropped, i.e. TNT (PP-78)
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        PlotWorld world = plugin.getWorld(event.getBlock().getWorld());
        if (world != null && event.getEntityType() == EntityType.ENDERMAN
                && !world.getConfig().isEndermenChangeBlockEnabled()) {
            event.setCancelled(true);
        }
    }

    //Armor stands, see PP-81
    @EventHandler(priority = EventPriority.HIGH)
    public void onArmorStandManipulated(PlayerArmorStandManipulateEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere")) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getRightClicked().getLocation());
            if (plot == null || !plot.mayBuild(p)) {
                event.setCancelled(true);
            }
        }
    }

    //Chicken eggs, see PP-88
    @EventHandler
    public void onThrowEgg(PlayerEggThrowEvent event) {
        Player p = event.getPlayer();
        PlotWorld world = plugin.getWorld(p.getLocation().getWorld());

        if (world != null && !p.hasPermission("plotplus.admin.buildanywhere") && world.getConfig().isEggThrowingDisabled()) {
            RestrictedArea plot = world.getEffectivePlotAt(event.getEgg().getLocation());
            if (plot == null || !plot.mayBuild(p)) {
                event.setHatching(false); //using only this line does not work
                event.setNumHatches((byte) 0);
                event.getEgg().remove();
            }
        }
    }
}
