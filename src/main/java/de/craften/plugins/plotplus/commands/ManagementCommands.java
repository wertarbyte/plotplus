package de.craften.plugins.plotplus.commands;


import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.CommandHandler;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.economy.PaymentResult;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Biome;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

public class ManagementCommands implements CommandHandler {
    private PlotPlus plugin;

    public ManagementCommands(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Command(value = "clear",
            description = "Clears the plot you stand on.",
            permission = "plotplus.user.clear")
    public boolean clear(Player player) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        Plot plot = world.getPlotAt(player.getLocation());
        if (plot != null) {
            if (plot.mayManage(player) || player.hasPermission("plotplus.admin.clearany")) {
                if (!plot.isProtected()) {
                    world.clearPlot(plot);
                    PlotPlus.prefix().append("Plot cleared.").sendTo(player);
                } else {
                    PlotPlus.prefix().append("This plot is protected and thus cannot be cleared.").red().sendTo(player);
                }
            } else {
                return false;
            }
        } else {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
        }
        return true;
    }

    @Command(value = "reset",
            description = "Removes the plot you stand on.",
            permission = "plotplus.user.reset",
            allowFromConsole = true,
            min = 0,
            max = 2)
    public boolean reset(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            PlotWorld world = plugin.getWorld(player.getWorld());
            Plot plot = world.getPlotAt(player.getLocation());
            if (plot != null) {
                if (plot.mayManage(player) || player.hasPermission("plotplus.admin.resetany")) {
                    if (!plot.isProtected()) {
                        try {
                            world.resetPlot(plot);
                            PlotPlus.prefix().append("Plot removed.").sendTo(player);
                        } catch (PersistenceException e) {
                            PlotPlus.prefix().append("Plot could not be removed.").red().sendTo(player);
                            e.printStackTrace();
                        }
                    } else {
                        PlotPlus.prefix().append("This plot is protected and thus cannot be removed.").red().sendTo(player);
                    }
                } else {
                    PlotPlus.prefix().append("You may not reset this plot.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
            }
            return true;
        } else if (args.length == 2) {
            PlotWorld world = plugin.getWorld(args[0]);
            if (world != null) {
                String[] pos = args[1].split(";");
                Plot plot = world.getPlot(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]));
                if (plot != null) {
                    if (!plot.isProtected()) {
                        try {
                            world.resetPlot(plot);
                            PlotPlus.prefix().append("Plot " + Integer.parseInt(pos[0]) + ";" + Integer.parseInt(pos[1]) + " in " + world.getName() + " removed.").sendTo(sender);
                        } catch (PersistenceException e) {
                            PlotPlus.prefix().append("Plot could not be removed.").red().sendTo(sender);
                            e.printStackTrace();
                        }
                    } else {
                        PlotPlus.prefix().append("This plot is protected and thus cannot be removed.").red().sendTo(sender);
                    }
                } else {
                    world.clearPlot(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]));
                }
            } else {
                PlotPlus.prefix().append("The specified world is not a plot world.").red().sendTo(sender);
            }
            return true;
        }
        return false;
    }

    @Command(value = "block",
            min = 1,
            max = 1,
            description = "Blocks the given player from the plot you stand on.",
            permission = "plotplus.user.block")
    public boolean block(Player player, String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        Plot plot = world.getPlotAt(player.getLocation());
        if (plot != null) {
            if (plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany")) {
                try {
                    plot.addBlockedPlayer(world.getManager().getPlayerCache().getCached(Bukkit.getServer().getOfflinePlayer(args[0])));
                    PlotPlus.prefix().append("Blocked " + args[0] + " from this plot.").sendTo(player);
                } catch (PersistenceException e) {
                    PlotPlus.prefix().append("Could not block " + args[0] + " from this plot.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You may not block players from this plot.").red().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
        }

        return true;
    }

    @Command(value = "unblock",
            min = 1,
            max = 1,
            description = "Unblocks the given player from the plot you stand on.",
            permission = "plotplus.user.block")
    public boolean unblock(Player player, String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        Plot plot = world.getPlotAt(player.getLocation());
        if (plot != null) {
            if ((player.hasPermission("plotplus.user.block") && plot.mayManage(player)) || player.hasPermission("plotplus.moderator.manageany")) {
                try {
                    plot.removeBlockedPlayer(plugin.getServer().getOfflinePlayer(args[0]).getUniqueId());
                    PlotPlus.prefix().append("Unblocked " + args[0] + " from this plot.").sendTo(player);
                } catch (PersistenceException e) {
                    PlotPlus.prefix().append("Could not unblock " + args[0] + " from this plot.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You may not unblock players from this plot.").red().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
        }

        return true;
    }

    @Command(value = "add",
            min = 1,
            max = 1,
            description = "Adds the given player as helper to the plot you stand on.",
            permission = "plotplus.user.managehelpers")
    public boolean addHelper(Player player, String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        Plot plot = world.getPlotAt(player.getLocation());
        if (plot != null) {
            if ((player.hasPermission("plotplus.user.managehelpers") && plot.mayManage(player)) || player.hasPermission("plotplus.moderator.manageany")) {
                try {
                    plot.addHelper(world.getManager().getPlayerCache().getCached(plugin.getServer().getOfflinePlayer(args[0])));
                    PlotPlus.prefix().append("Added " + args[0] + " as helper to this plot.").sendTo(player);
                } catch (PersistenceException e) {
                    PlotPlus.prefix().append("Could not add " + args[0] + " as helper to this plot.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You may not add helpers to this plot.").red().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
        }

        return true;
    }

    @Command(value = "remove",
            min = 1,
            max = 1,
            description = "Removes the given helper from the plot you stand on.",
            permission = "plotplus.user.managehelpers")
    public boolean removeHelper(Player player, String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        Plot plot = world.getPlotAt(player.getLocation());
        if (plot != null) {
            if ((player.hasPermission("plotplus.user.managehelpers") && plot.mayManage(player)) || player.hasPermission("plotplus.moderator.manageany")) {
                try {
                    plot.removeHelper(plugin.getServer().getOfflinePlayer(args[0]).getUniqueId());
                    PlotPlus.prefix().append("Removed " + args[0] + " as helper from this plot.").sendTo(player);
                } catch (PersistenceException e) {
                    PlotPlus.prefix().append("Could not remove " + args[0] + " as helper from this plot.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You may not remove helpers from this plot.").red().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
        }

        return true;
    }

    @Command(value = "leave",
            description = "Removes you from the plot you stand on.",
            permission = "plotplus.user.leave")
    public boolean leavePlot(Player player) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        Plot plot = world.getPlotAt(player.getLocation());
        if (plot != null) {
            if (plot.mayBuild(player)) {
                if (!plot.getOwner().getUniqueId().equals(player.getUniqueId())) {
                    try {
                        plot.removeHelper(player.getUniqueId());
                        PlotPlus.prefix().append("You were removed from this plot.").sendTo(player);
                    } catch (PersistenceException e) {
                        PlotPlus.prefix().append("Could not remove you from this plot.").red().sendTo(player);
                    }
                } else {
                    PlotPlus.prefix().append("You're the owner of this plot and can't leave it.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You're not registered on this plot.").red().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
        }

        return true;
    }

    @Command(value = "protect",
            description = "Enables/disables protection of the plot you stand on.",
            permission = "plotplus.admin.protect")
    public boolean protect(Player player) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null) {
            PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
            return true;
        }

        if (plot.isProtected()) {
            try {
                plot.getConfig().setProtected(false);
                PlotPlus.prefix().append("Disabled protection for this plot.").sendTo(player);
            } catch (PersistenceException e) {
                PlotPlus.prefix().append("Could not disable protection for this plot.").red().sendTo(player);
            }
        } else {
            try {
                plot.getConfig().setProtected(true);
                PlotPlus.prefix().append("Enabled protection for this plot.").sendTo(player);
            } catch (PersistenceException e) {
                PlotPlus.prefix().append("Could not enable protection for this plot.").red().sendTo(player);
            }
        }
        return true;
    }

    @Command(value = "setbiome",
            description = "Changes the biome of the plot you stand on.",
            permission = "plotplus.user.setbiome",
            min = 0,
            max = 1)
    public boolean setBiome(Player player, String[] args) {
        final String supportedBiomes = "Plains, Swampland, Jungle, Mushroom, Desert, Taiga, ColdTaiga, Savanna and Mesa";

        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the biome of this plot.").red().sendTo(player);
            return true;
        }

        if (args.length == 1) {
            final Biome newBiome;
            if (args[0].equalsIgnoreCase("plains"))
                newBiome = Biome.PLAINS;
            else if (args[0].equalsIgnoreCase("swampland"))
                newBiome = Biome.SWAMPLAND;
            else if (args[0].equalsIgnoreCase("jungle"))
                newBiome = Biome.JUNGLE;
            else if (args[0].equalsIgnoreCase("mushroom"))
                newBiome = Biome.MUSHROOM_ISLAND;
            else if (args[0].equalsIgnoreCase("desert"))
                newBiome = Biome.DESERT;
            else if (args[0].equalsIgnoreCase("taiga"))
                newBiome = Biome.TAIGA;
            else if (args[0].equalsIgnoreCase("coldtaiga"))
                newBiome = Biome.TAIGA_COLD;
            else if (args[0].equalsIgnoreCase("savanna"))
                newBiome = Biome.SAVANNA;
            else if (args[0].equalsIgnoreCase("mesa"))
                newBiome = Biome.MESA;
            else {
                PlotPlus.prefix().append("Supported biomes: ").append(supportedBiomes).darkGreen().sendTo(player);
                return true;
            }

            if (!plot.getBiome().equals(newBiome)) {
                double price = player.hasPermission("plotplus.freebiomechange") ? 0 : world.getConfig().getChangeBiomePrice();
                if (price > 0)
                    PlotPlus.prefix().append("That'll be ").gray().append(plugin.getEconomy().formatAmount(price)).darkGreen().append(" please!").gray();
                PaymentResult payment = plugin.getEconomy().withdrawPlayer(price, player, new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        plot.setBiome(newBiome);
                        return true;
                    }
                });
                switch (payment) {
                    case NotEnoughMoney:
                        PlotPlus.prefix().append("You don't have enough money to change the biome.").red().sendTo(player);
                        break;
                    case Failed:
                        PlotPlus.prefix().append("The payment failed.").red().sendTo(player);
                        break;
                    case Success:
                        PlotPlus.prefix().append("This plot's biome was set to ").append(newBiome.name()).darkGreen().sendTo(player);
                }
                return true;
            } else {
                PlotPlus.prefix().append("This plot's biome is already set to ").append(plot.getBiome().name()).darkGreen().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("Supported biomes: ").append(supportedBiomes).darkGreen().sendTo(player);
        }

        return true;
    }

    @Command(value = "rename",
            description = "Changes the name of the plot you stand on.",
            permission = "plotplus.user.namedplots",
            usage = "rename [new name]",
            max = Integer.MAX_VALUE)
    public boolean setName(final Player player, final String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the name of this plot.").red().sendTo(player);
            return true;
        }
        double price = player.hasPermission("plotplus.freerename") ? 0 : world.getConfig().getPlotRenamePrice();
        if (price > 0)
            PlotPlus.prefix().append("That'll be ").gray().append(plugin.getEconomy().formatAmount(price)).darkGreen().append(" please!").gray();

        PaymentResult payment = plugin.getEconomy().withdrawPlayer(price, player, new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                try {
                    if (args.length > 0) {
                        String newName = StringUtil.join(args, " ");
                        plot.getConfig().setName(newName);
                        PlotPlus.prefix().append("Renamed this plot to \"").gray().append(newName).darkGreen().append("\".").gray()
                                .sendTo(player);
                    } else {
                        plot.getConfig().setName(null);
                        PlotPlus.prefix().append("Removed the name of this plot.")
                                .sendTo(player);
                    }
                    return true;
                } catch (PersistenceException e) {
                    PlotPlus.prefix().append("Could not rename this plot.")
                            .sendTo(player);
                    return false;
                }
            }
        });
        switch (payment) {
            case NotEnoughMoney:
                PlotPlus.prefix().append("You don't have enough money to change the biome.").red().sendTo(player);
                break;
            case Failed:
                PlotPlus.prefix().append("The payment failed.").red().sendTo(player);
                break;
            case Reversed:
                PlotPlus.prefix().append("The payment was reversed.").red().sendTo(player);
                break;
        }

        return true;
    }

    @Command(value = "welcomemessage",
            description = "Enabled or disables the welcome message on the plot you stand on.",
            permission = "plotplus.user.editconfig.welcomemessage",
            usage = "welcomemessage on|off",
            min = 1,
            max = 1)
    public boolean setShowWelcomeMessage(final Player player, final String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the configuration of this plot.").red().sendTo(player);
            return true;
        }
        if (args[0].equals("on")) {
            try {
                plot.getConfig().setShowWelcomeMessage(true);
                PlotPlus.prefix().append("Enabled welcome message on this plot.").sendTo(player);
            } catch (PersistenceException e) {
                e.printStackTrace();
                PlotPlus.prefix().append("Could not enable welcome message on this plot.").red().sendTo(player);
            }
        } else {
            try {
                plot.getConfig().setShowWelcomeMessage(false);
                PlotPlus.prefix().append("Disabled welcome message on this plot.").sendTo(player);
            } catch (PersistenceException e) {
                e.printStackTrace();
                PlotPlus.prefix().append("Could not disable welcome message on this plot.").red().sendTo(player);
            }
        }

        return true;
    }

    @Command(value = "projectileprotection",
            description = "Enables or disables projectile protection on the plot you stand on.",
            permission = "plotplus.user.editconfig.projectileprotection",
            usage = "projectileprotection on|off",
            min = 1,
            max = 1)
    public boolean setProjectileProtectionEnabled(final Player player, final String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the configuration of this plot.").red().sendTo(player);
            return true;
        }
        if (args[0].equals("on")) {
            try {
                plot.getConfig().setEnableProjectileProtection(true);
                PlotPlus.prefix().append("Enabled projectile protection on this plot.").sendTo(player);
            } catch (PersistenceException e) {
                e.printStackTrace();
                PlotPlus.prefix().append("Could not enable projectile protection on this plot.").red().sendTo(player);
            }
        } else {
            try {
                plot.getConfig().setEnableProjectileProtection(false);
                PlotPlus.prefix().append("Disabled projectile protection on this plot.").sendTo(player);
            } catch (PersistenceException e) {
                e.printStackTrace();
                PlotPlus.prefix().append("Could not disable projectile protection on this plot.").red().sendTo(player);
            }
        }

        return true;
    }

    @Command(value = "hitprotection",
            description = "Enables or disables projectile protection on the plot you stand on.",
            permission = "plotplus.user.editconfig.hitprotection",
            usage = "projectileprotection on|off",
            min = 1,
            max = 1)
    public boolean setHitProtectionEnabled(final Player player, final String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the configuration of this plot.").red().sendTo(player);
            return true;
        }
        if (args[0].equals("on")) {
            try {
                plot.getConfig().setEnableHitProtection(true);
                PlotPlus.prefix().append("Enabled hit protection on this plot.").sendTo(player);
            } catch (PersistenceException e) {
                e.printStackTrace();
                PlotPlus.prefix().append("Could not enable hit protection on this plot.").red().sendTo(player);
            }
        } else {
            try {
                plot.getConfig().setEnableHitProtection(false);
                PlotPlus.prefix().append("Disabled hit protection on this plot.").sendTo(player);
            } catch (PersistenceException e) {
                e.printStackTrace();
                PlotPlus.prefix().append("Could not disable hit protection on this plot.").red().sendTo(player);
            }
        }

        return true;
    }

    @Command(value = "setowner",
            min = 1,
            max = 1,
            permission = "plotplus.user.setowner")
    public boolean setOwner(Player player, String[] args) {
        Collection<PlotId> plots = SelectionCommands.getSelection(player);
        if (!plots.isEmpty()) {
            OfflinePlayer owner = Bukkit.getOfflinePlayer(args[0]);
            if (owner != null) {
                List<Plot> changedPlots = new ArrayList<>();
                List<SimplePlayer> changedPlotsOwners = new ArrayList<>();
                for (PlotId id : plots) {
                    Plot plot = plugin.getWorld(id.getWorld()).getPlot(id.getIdX(), id.getIdZ());
                    if (plot != null) {
                        if (plot.mayManage(player.getUniqueId()) || player.hasPermission("plotplus.moderator.manageany")) {
                            try {
                                SimplePlayer oldOwner = plot.getOwner();
                                plot.setOwnerSilently(plot.getWorld().getManager().getPlayerCache().getCached(owner));

                                changedPlots.add(plot);
                                changedPlotsOwners.add(oldOwner);
                            } catch (PersistenceException e) {
                                PlotPlus.prefix().append("An error occurred while changing the owner of plot ").red()
                                        .append(id.getIdX() + ";" + id.getIdZ()).white().append(".").red()
                                        .sendTo(player);
                            }
                        } else {
                            PlotPlus.prefix().append("You can't set the owner of plot ")
                                    .append(id.getIdX() + ";" + id.getIdZ()).append(".").red().sendTo(player);
                        }
                    }
                }

                //Now call the API
                for (int i = 0; i < changedPlots.size(); i++) {
                    Plot plot = changedPlots.get(i);
                    plot.getWorld().getApi().onPlotOwnerChanged(plot, changedPlotsOwners.get(i));
                }

                PlotPlus.prefix().append("The owner of the selected plots was set to ")
                        .append(owner.getName()).darkGreen().append(".").gray().sendTo(player);

                SelectionCommands.clearSelection(player);
                PlotPlus.prefix().append("Plot selection cleared").gray().sendTo(player);
            } else {
                PlotPlus.prefix().append("That player doesn't exist.").red().sendTo(player);
            }
        } else {
            Plot plot = plugin.getWorld(player.getWorld()).getPlotAt(player.getLocation());
            if (plot != null) {
                if (plot.mayManage(player.getUniqueId()) || player.hasPermission("plotplus.moderator.manageany")) {
                    OfflinePlayer owner = Bukkit.getOfflinePlayer(args[0]);
                    if (owner != null) {
                        try {
                            plot.setOwner(plot.getWorld().getManager().getPlayerCache().getCached(owner));
                            PlotPlus.prefix().append("The owner of this plot was set to ")
                                    .append(owner.getName()).darkGreen().append(".").gray().sendTo(player);
                        } catch (PersistenceException e) {
                            PlotPlus.prefix().append("An error occurred while changing the owner of this plot.").red()
                                    .sendTo(player);
                        }
                    } else {
                        PlotPlus.prefix().append("That player doesn't exist.").red().sendTo(player);
                    }
                } else {
                    PlotPlus.prefix().append("You can't set the owner of this plot.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
            }
        }
        return true;
    }

    @Command(value = "connect",
            description = "Connects the plot you stand on to an adjacent plot",
            usage = "connect x;z",
            min = 1,
            max = 1,
            permission = "plotplus.user.connect")
    public boolean connectPlot(Player player, String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the configuration of this plot.").red().sendTo(player);
            return true;
        }

        if (args[0].matches("-?\\d+;-?\\d+")) {
            String[] parts = args[0].split(";");
            int idX = Integer.parseInt(parts[0]);
            int idZ = Integer.parseInt(parts[1]);

            if (Math.abs(idX - plot.getIdX()) + Math.abs(idZ - plot.getIdZ()) == 1) {
                Plot linkTo = world.getPlot(idX, idZ);
                if (linkTo != null) {
                    try {
                        world.connectPlot(plot, linkTo);
                        PlotPlus.prefix().append("The plot ").append(plot.getIdX() + ";" + plot.getIdZ()).darkGreen()
                                .append(" was connected to the plot ").gray()
                                .append(linkTo.getIdX() + ";" + linkTo.getIdZ()).darkGreen().append(".").gray()
                                .sendTo(player);
                        if (!linkTo.isConnectedWith(plot)) {
                            PlotPlus.prefix().append("The owner of the other plot needs to accept the " +
                                    "connection by linking their plot to your plot.").sendTo(player);
                        }
                    } catch (PersistenceException e) {
                        PlotPlus.prefix().append("An error occurred while connecting the plots.").red().sendTo(player);
                    }
                } else {
                    PlotPlus.prefix().append("The plot " + idX + ";" + idZ + " is not claimed.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("Only adjacent plots can be connected.").red().sendTo(player);
            }
            return true;
        } else {
            return false;
        }
    }

    @Command(value = "disconnect",
            description = "Disconnects the plot you stand on from a connected adjacent plot",
            usage = "disconnect x;z",
            min = 1,
            max = 1,
            permission = "plotplus.user.connect")
    public boolean disconnectPlot(Player player, String[] args) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null)
            return true;

        final Plot plot = world.getPlotAt(player.getLocation());
        if (plot == null || !(plot.mayManage(player) || player.hasPermission("plotplus.moderator.manageany"))) {
            PlotPlus.prefix().append("You may not change the configuration of this plot.").red().sendTo(player);
            return true;
        }

        if (args[0].matches("-?\\d+;-?\\d+")) {
            String[] parts = args[0].split(";");
            Plot disconnectFrom = world.getPlot(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
            if (disconnectFrom != null && world.getManager().isPlotConnectedTo(plot, disconnectFrom)) {
                try {
                    world.disconnectPlot(plot, disconnectFrom);
                    PlotPlus.prefix().append("The plot ").append(plot.getIdX() + ";" + plot.getIdZ()).darkGreen()
                            .append(" was disconnected from the plot ").gray()
                            .append(disconnectFrom.getIdX() + ";" + disconnectFrom.getIdZ()).darkGreen().append(".").gray()
                            .sendTo(player);
                } catch (PersistenceException e) {
                    PlotPlus.prefix().append("An error occurred while disconnecting the plots.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("The plot " + plot.getIdX() + ";" + plot.getIdZ() +
                        " is not connected to the plot " + parts[0] + ";" + parts[1] + ".").red().sendTo(player);
            }
            return true;
        } else {
            return false;
        }
    }
}
