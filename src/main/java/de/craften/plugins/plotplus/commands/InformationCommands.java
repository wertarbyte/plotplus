package de.craften.plugins.plotplus.commands;


import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.CommandHandler;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class InformationCommands implements CommandHandler {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");
    private PlotPlus plugin;

    public InformationCommands(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Command(value = "info",
            permission = "plotplus.user.info",
            description = "Displays information about the plot you stand on.")
    public boolean info(Player player) {
        PlotWorld world = plugin.getWorld(player.getWorld());
        if (world != null) {
            Plot plot = world.getPlotAt(player.getLocation());
            if (plot != null) {
                TextBuilder.create("=======[").darkGray().append("Plot Information").darkGreen().append("]=======").darkGray().sendTo(player);
                TextBuilder msg1 = TextBuilder.create("ID: ").gray().append(plot.getIdX() + ";" + plot.getIdZ() + "   ").darkGreen();
                if (plot.getConfig().hasName())
                    msg1.append("Name: \"").gray().append(plot.getConfig().getName()).darkGreen().append("\"   ").gray();
                Date lastSeen = new Date(Bukkit.getOfflinePlayer(plot.getOwner().getUniqueId()).getLastPlayed());
                msg1.append("Owner: ").gray().append(plot.getOwner().getDisplayName() + "   ").darkGreen();
                if (lastSeen.getTime() > 0) {
                    msg1.append("(last seen: ").gray().append(DATE_FORMAT.format(lastSeen)).darkGreen().append(")").gray();
                } else {
                    msg1.append("(never seen before)").gray();
                }
                msg1.newLine()
                        .append("Biome: ").gray().append(plot.getBiome().name() + "   ").darkGreen()
                        .append("Protected: ").gray().append(plot.isProtected() ? "Yes" : "No").darkGreen()
                        .sendTo(player);

                if (!plot.getHelpers().isEmpty()) {
                    TextBuilder helpers = TextBuilder.create("Helpers: ").gray();
                    boolean first = true;
                    for (SimplePlayer helper : plot.getHelpers()) {
                        if (!first)
                            helpers.append(", ").gray();
                        else
                            first = false;
                        helpers.append(helper.getDisplayName()).darkGreen();
                    }
                    helpers.sendTo(player);
                }
                if (!plot.getBlockedPlayers().isEmpty()) {
                    TextBuilder blockedPlayers = TextBuilder.create("Blocked players: ").gray();
                    boolean first = true;
                    for (SimplePlayer blocked : plot.getBlockedPlayers()) {
                        if (!first)
                            blockedPlayers.append(", ").gray();
                        else
                            first = false;
                        blockedPlayers.append(blocked.getDisplayName()).darkGreen();
                    }
                    blockedPlayers.sendTo(player);
                }
            } else if (world.isOnPlot(player.getLocation())) {
                PointXZ plotId = world.getScheme().getPlotIdAt(player.getLocation());
                TextBuilder.create("=======[").darkGray().append("Plot information").darkGreen().append("]=======").darkGray().sendTo(player);
                TextBuilder.create()
                        .append("ID: ").gray().append(plotId.getX() + ";" + plotId.getZ() + "   ").darkGreen()
                        .append("Owner: ").gray().append("(not claimed)").darkGreen().sendTo(player);
            } else {
                PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
            }
        }
        return true;
    }

    @Command(value = "border",
            permission = "plotplus.user.info",
            description = "Shows the border of the plot the player currently stands on.",
            usage = "border [time to display the border in seconds]",
            min = 0,
            max = 1)
    public boolean showBorder(final Player player, String[] args) {
        final PlotWorld world = plugin.getWorld(player.getWorld());
        if (world != null) {
            int hideDelay = 10;
            if (args.length == 1 && args[0].matches("\\d+")) {
                hideDelay = Integer.valueOf(args[0]);
            }

            final World w = world.getBukkitWorld();
            Location loc = player.getLocation();
            if (world.getScheme().isInBuildZone(loc)) {
                PointXZ pid = world.getScheme().getPlotIdAt(loc);
                Plot plot = world.getPlot(pid.getX(), pid.getZ());
                final Set<PointXZ> border;
                if (plot == null) {
                    //Plot unclaimed
                    border = world.getScheme().getCoreBuildRegion(pid.getX(), pid.getZ()).getBorder();
                } else {
                    //Plot claimed
                    border = plot.getConnectedBuildRegion().getBorder();
                }
                for (PointXZ point : border) {
                    for (int y = 1; y < w.getMaxHeight(); y += 3) {
                        Block b = w.getBlockAt(point.getX(), y, point.getZ());
                        if (b.getType() == Material.AIR) {
                            player.sendBlockChange(b.getLocation(),
                                    world.getConfig().getBorderDisplayBlock().getId(),
                                    world.getConfig().getBorderDisplayBlock().getData());
                        }
                    }
                }
                plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
                    @Override
                    public void run() {
                        for (PointXZ point : border) {
                            for (int y = 1; y < w.getMaxHeight(); y += 3) {
                                Block b = w.getBlockAt(point.getX(), y, point.getZ());
                                player.sendBlockChange(b.getLocation(), b.getType(), b.getData());
                            }
                        }
                    }
                }, 20 * hideDelay);
            } else {
                PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
            }
        }
        return true;
    }

    @Command(value = "menu",
            description = "Shows a menu.")
    public boolean showGui(Player player) {
        if (plugin.getWorld(player.getWorld()) != null) {
            plugin.getGui().showPlotPlusMenu(player);
        } else {
            PlotPlus.prefix().append("You are not in a plot world.").red().sendTo(player);
        }
        return true;
    }
}
