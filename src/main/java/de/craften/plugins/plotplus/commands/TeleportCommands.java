package de.craften.plugins.plotplus.commands;

import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.CommandHandler;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.entity.Player;

import java.util.List;

public class TeleportCommands implements CommandHandler {
    private PlotPlus plugin;

    public TeleportCommands(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Command(value = "home",
            max = 1,
            permission = "plotplus.user.teleport.home")
    public boolean teleportToHome(Player sender, String[] args) {
        PlotWorld world = plugin.getWorld(sender.getWorld());
        if (world != null) {
            List<Plot> plotsByPlayer = world.getPlotsByOwner(sender.getUniqueId());
            if (plotsByPlayer.size() > 0) {
                if (args.length == 0 || args[0].equals("1")) {
                    plotsByPlayer.get(0).teleportPlayer(sender);
                } else {
                    if (args[0].matches("\\d+")) {
                        int num = Integer.parseInt(args[0]);
                        if (num == 0)
                            num++;
                        if (plotsByPlayer.size() >= num) {
                            plotsByPlayer.get(num - 1).teleportPlayer(sender);
                        } else {
                            PlotPlus.prefix().append("You don't have that many plots in this world.").red().sendTo(sender);
                        }
                    } else {
                        for (Plot plot : plotsByPlayer) {
                            if (plot.getConfig().hasName() && plot.getConfig().getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                                plot.teleportPlayer(sender);
                                return true;
                            }
                        }

                        return false;
                    }
                }
            } else {
                PlotPlus.prefix().append("You don't have any plots in this world.").red().sendTo(sender);
            }
        }
        return true;
    }

    @Command(value = "tp",
            min = 1,
            max = 1,
            permission = "plotplus.user.teleport.others")
    public boolean teleportToPlot(Player player, String[] args) {
        if (args[0].matches("-?\\d+;-?\\d+")) {
            String[] parts = args[0].split(";");
            int plotIdX = Integer.parseInt(parts[0]);
            int plotIdZ = Integer.parseInt(parts[1]);

            PlotWorld world = plugin.getWorld(player.getWorld());
            player.teleport(world.getScheme().getTeleportLocation(world, plotIdX, plotIdZ));
            return true;
        } else {
            return false;
        }
    }
}
