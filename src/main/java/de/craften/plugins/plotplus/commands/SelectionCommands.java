package de.craften.plugins.plotplus.commands;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.CommandHandler;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Collection;
import java.util.UUID;

/**
 * Selection commands.
 */
public class SelectionCommands implements CommandHandler, Listener {
    private PlotPlus plugin;
    private static Multimap<UUID, PlotId> selection = HashMultimap.create();

    public SelectionCommands(PlotPlus plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Command(value = "selection", permission = "plotplus.user.selection")
    public boolean showSelection(Player sender) {
        Collection<PlotId> selected = selection.get(sender.getUniqueId());
        if (selection.isEmpty()) {
            PlotPlus.prefix().append("No plots selected").gray().sendTo(sender);
        } else {
            TextBuilder msg = PlotPlus.prefix().append(selected.size() + " plots selected: ").gray();
            boolean first = true;
            for (PlotId id : selected) {
                if (first) {
                    msg.append(id.getIdX() + ";" + id.getIdZ()).darkGreen();
                    first = false;
                } else {
                    msg.append(", ").gray().append(id.getIdX() + ";" + id.getIdZ()).darkGreen();
                }
            }
            msg.sendTo(sender);
        }
        return true;
    }

    @Command(value = "select", permission = "plotplus.user.selection")
    public boolean select(Player sender) {
        PlotWorld world = plugin.getWorld(sender.getWorld());
        if (world != null) {
            PointXZ id = world.getScheme().getPlotIdAt(sender.getLocation());
            selection.put(sender.getUniqueId(), new PlotId(id.getX(), id.getZ(), world.getName()));
            PlotPlus.prefix().append(selection.get(sender.getUniqueId()).size() + " plots selected").gray().sendTo(sender);
        }
        return true;
    }

    @Command(value = "unselect", permission = "plotplus.user.selection")
    public boolean unselect(Player sender) {
        PlotWorld world = plugin.getWorld(sender.getWorld());
        if (world != null) {
            PointXZ id = world.getScheme().getPlotIdAt(sender.getLocation());
            selection.remove(sender.getUniqueId(), new PlotId(id.getX(), id.getZ(), world.getName()));
            PlotPlus.prefix().append(selection.get(sender.getUniqueId()).size() + " plots selected").gray().sendTo(sender);
        }
        return true;
    }

    @Command(value = "clearselection", permission = "plotplus.user.selection")
    public boolean clearSelectionCommand(Player sender) {
        selection.removeAll(sender.getUniqueId());
        PlotPlus.prefix().append("Plot selection cleared").gray().sendTo(sender);
        return true;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        clearSelection(event.getPlayer());
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (!event.getFrom().getWorld().equals(event.getTo().getWorld())) {
            clearSelection(event.getPlayer());
        }
    }

    /**
     * Gets the selected plots of the given player.
     *
     * @param player player to get the selected plots of
     * @return the selected plots of the given player
     */
    public static Collection<PlotId> getSelection(Player player) {
        return selection.get(player.getUniqueId());
    }

    /**
     * Clears the selection of the given player.
     *
     * @param player player to clear the selection of
     */
    public static void clearSelection(Player player) {
        selection.removeAll(player.getUniqueId());
    }
}
