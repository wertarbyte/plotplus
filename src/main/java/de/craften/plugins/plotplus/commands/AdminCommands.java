package de.craften.plugins.plotplus.commands;

import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.CommandHandler;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class AdminCommands implements CommandHandler {
    private PlotPlus plugin;

    public AdminCommands(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Command(value = "autoclean",
            min = 1,
            max = 2,
            permission = "plotplus.admin.autoclean",
            allowFromConsole = true)
    public boolean autoClean(CommandSender sender, String[] args) {
        PlotWorld world;
        int minimumChanged;

        if (args.length == 1 && args[0].matches("\\d+") && sender instanceof Player) {
            world = plugin.getWorld(((Player) sender).getWorld());
            minimumChanged = Integer.parseInt(args[0]);
        } else if (args.length == 2 && args[1].matches("\\d+")) {
            world = plugin.getWorld(args[0]);
            minimumChanged = Integer.parseInt(args[1]);
        } else {
            return false;
        }

        if (world != null) {
            world.getAutoCleaner(minimumChanged).runTaskTimer(plugin, 0, 10); // one plot every 10 ticks
        } else {
            PlotPlus.prefix().append("The specified world doesn't exist or isn't a PlotPlus world").red()
                    .sendTo(sender);
        }
        return true;
    }
}
