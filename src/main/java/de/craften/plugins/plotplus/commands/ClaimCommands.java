package de.craften.plugins.plotplus.commands;

import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.CommandHandler;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.economy.PaymentResult;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.concurrent.Callable;


public class ClaimCommands implements CommandHandler {
    private PlotPlus plugin;

    public ClaimCommands(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Command(value = "claim",
            max = 1,
            permission = "plotplus.user.claim",
            description = "Claims the plot you currently stand on.")
    public Result claim(final Player player, String[] args) {
        final PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null) return Result.Done;

        final OfflinePlayer owner;
        if (args.length == 1) {
            if (player.hasPermission("plotplus.moderator.claimothers")) {
                owner = Bukkit.getOfflinePlayer(args[0]);
                if (owner == null) {
                    player.sendMessage("That player doesn't exist.");
                    return Result.Done;
                }
            } else {
                return Result.NoPermission;
            }
        } else {
            owner = player;
        }

        if (owner != player || player.hasPermission("plotplus.bypassplotlimit") || world.getConfig().getPlotsPerPlayer() < 0 ||
                world.getPlotsByOwner(player.getUniqueId()).size() < world.getConfig().getPlotsPerPlayer()) {
            if (world.isOnPlot(player.getLocation())) {
                if (world.getPlotAt(player.getLocation()) == null) {
                    double price = player.hasPermission("plotplus.freeplots") ? 0 : world.getConfig().getPlotPrice();

                    PlotClaimEvent event = new PlotClaimEvent(price, player, owner, world);
                    world.getApi().onPlotClaiming(event);
                    price = event.getPrice();

                    if (price > 0)
                        PlotPlus.prefix().append("That'll be ").gray().append(plugin.getEconomy().formatAmount(price)).darkGreen().append(" please!").gray().sendTo(player);
                    PaymentResult payment = plugin.getEconomy().withdrawPlayer(price, player, new Callable<Boolean>() {
                        @Override
                        public Boolean call() throws Exception {
                            try {
                                Plot plot = world.createPlotAt(player.getLocation(), owner);
                                if (owner == player) {
                                    PlotPlus.prefix().append(String.format("Plot %d/%d is now yours!", plot.getIdX(), plot.getIdZ())).sendTo(player);
                                } else {
                                    Player ownerOnline = Bukkit.getPlayer(owner.getUniqueId());
                                    if (ownerOnline != null)
                                        PlotPlus.prefix().append(String.format("Plot %d/%d is now yours!", plot.getIdX(), plot.getIdZ())).sendTo(ownerOnline);
                                    PlotPlus.prefix().append(String.format("Plot %d/%d is now owned by %s!", plot.getIdX(), plot.getIdZ(), owner.getName())).sendTo(player);
                                }
                                return true;
                            } catch (PersistenceException e) {
                                PlotPlus.prefix().append("The plot couldn't be claimed.").red().sendTo(player);
                                return false;
                            }
                        }
                    });
                    switch (payment) {
                        case NotEnoughMoney:
                            PlotPlus.prefix().append("You don't have enough money to claim a plot.").red().sendTo(player);
                            break;
                        case Reversed:
                            PlotPlus.prefix().append("The payment was reversed.").red().sendTo(player);
                            break;
                        case Failed:
                            PlotPlus.prefix().append("The payment failed.").red().sendTo(player);
                            break;
                    }
                } else {
                    PlotPlus.prefix().append("This plot is already claimed.").red().sendTo(player);
                }
            } else {
                PlotPlus.prefix().append("You're not on a plot.").red().sendTo(player);
            }
        } else {
            PlotPlus.prefix().append("You have reached the limit of " + world.getConfig().getPlotsPerPlayer() + " plots in this world and can't claim a new plot.").red().sendTo(player);
        }

        return Result.Done;
    }

    @Command(value = "autoclaim",
            permission = "plotplus.user.claim",
            description = "Claims a random free plot.")
    public boolean autoClaim(final Player player) {
        final PlotWorld world = plugin.getWorld(player.getWorld());
        if (world == null) return true;


        if (player.hasPermission("plotplus.bypassplotlimit") || world.getConfig().getPlotsPerPlayer() < 0 ||
                world.getPlotsByOwner(player.getUniqueId()).size() < world.getConfig().getPlotsPerPlayer()) {
            double price = player.hasPermission("plotplus.freeplots") ? 0 : world.getConfig().getPlotPrice();

            PlotClaimEvent event = new PlotClaimEvent(price, player, player, world);
            world.getApi().onPlotClaiming(event);
            price = event.getPrice();

            if (price > 0)
                PlotPlus.prefix().append("That'll be ").gray().append(plugin.getEconomy().formatAmount(price)).darkGreen().append(" please!").gray().sendTo(player);
            PaymentResult payment = plugin.getEconomy().withdrawPlayer(price, player, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    try {
                        Plot plot = world.claimAutomatically(player);
                        plot.teleportPlayer(player);
                        PlotPlus.prefix().append(String.format("Plot %d;%d is now yours!", plot.getIdX(), plot.getIdZ())).sendTo(player);
                        return true;
                    } catch (PersistenceException e) {
                        PlotPlus.prefix().append("The plot couldn't be claimed.").red().sendTo(player);
                        return false;
                    }
                }
            });
            switch (payment) {
                case NotEnoughMoney:
                    PlotPlus.prefix().append("You don't have enough money to claim a plot.").red().sendTo(player);
                    break;
                case Reversed:
                    PlotPlus.prefix().append("The payment was reversed.").red().sendTo(player);
                    break;
                case Failed:
                    PlotPlus.prefix().append("The payment failed.").red().sendTo(player);
                    break;
            }
        } else {
            PlotPlus.prefix().append("You have reached the limit of " + world.getConfig().getPlotsPerPlayer() + " plots in this world and can't claim a new plot.").red().sendTo(player);
        }

        return true;
    }
}
