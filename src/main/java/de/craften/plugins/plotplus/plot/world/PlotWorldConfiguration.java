package de.craften.plugins.plotplus.plot.world;

import de.craften.plugins.plotplus.generator.blocks.Item;
import de.craften.plugins.plotplus.util.EnumUtil;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A configuration of a plot world.
 */
public class PlotWorldConfiguration {
    private PlotWorldScheme scheme;

    private double plotPrice;
    private int plotsPerPlayer;

    private Biome defaultBiome;
    private boolean connectedPlotsEnabled;

    private List<Item> blockedItems = new ArrayList<>();
    private double changeBiomePrice;
    private double plotRenamePrice;
    private de.craften.plugins.plotplus.generator.blocks.Block borderDisplayBlock;
    private boolean hitProtectionEnabled;
    private boolean projectileProtectionEnabled;
    private boolean ignitionDisabled;
    private boolean flintAndSteelEnabled;
    private boolean fireSpreadingDisabled;
    private boolean showWelcomeMessage;
    private boolean endermenChangeBlockEnabled;
    private boolean eggThrowingDisabled;

    public PlotWorldConfiguration(PlotWorldScheme scheme, ConfigurationSection config) throws IOException {
        this.scheme = scheme;

        plotPrice = config.getDouble("prices.plot", 0);
        changeBiomePrice = config.getDouble("prices.changebiome", 0);
        plotRenamePrice = config.getDouble("prices.renameplot", 0);
        plotsPerPlayer = config.getInt("limits.plotsPerPlayer", -1);
        defaultBiome = EnumUtil.getOrDefault(Biome.class, config.getString("biome"), Biome.PLAINS);
        connectedPlotsEnabled = config.getBoolean("connectedPlotsEnabled", false);
        borderDisplayBlock = new de.craften.plugins.plotplus.generator.blocks.Block(config.getString("borderDisplayBlock", "160:14")); //default is red glass pane
        hitProtectionEnabled = config.getBoolean("protection.hit", true);
        projectileProtectionEnabled = config.getBoolean("protection.projectiles", true);
        ignitionDisabled = config.getBoolean("protection.ignitionDisabled", true);
        flintAndSteelEnabled = config.getBoolean("protection.flintAndSteelEnabled", false);
        fireSpreadingDisabled = config.getBoolean("protection.fireSpreadingDisabled", true);
        showWelcomeMessage = config.getBoolean("showWelcomeMessage", true);
        endermenChangeBlockEnabled = config.getBoolean("endermenChangeBlockEnabled", false);
        eggThrowingDisabled = config.getBoolean("eggThrowingDisabled", true);

        for (String item : config.getStringList("blockedItems"))
            blockedItems.add(new Item(item));
    }

    public PlotWorldConfiguration(PlotWorldScheme scheme, double plotPrice, double changeBiomePrice, int plotsPerPlayer, Biome defaultBiome, boolean connectedPlotsEnabled) {
        this.scheme = scheme;
        this.plotPrice = plotPrice;
        this.changeBiomePrice = changeBiomePrice;
        this.plotsPerPlayer = plotsPerPlayer;
        this.defaultBiome = defaultBiome;
        this.connectedPlotsEnabled = connectedPlotsEnabled;
    }

    public double getPlotPrice() {
        return plotPrice;
    }

    public double getChangeBiomePrice() {
        return changeBiomePrice;
    }

    public double getPlotRenamePrice() {
        return plotRenamePrice;
    }

    public int getPlotsPerPlayer() {
        return plotsPerPlayer;
    }

    public Biome getDefaultBiome() {
        return defaultBiome;
    }

    public de.craften.plugins.plotplus.generator.blocks.Block getBorderDisplayBlock() {
        return borderDisplayBlock;
    }

    public boolean isConnectedPlotsEnabled() {
        return connectedPlotsEnabled;
    }

    public PlotWorldScheme getScheme() {
        return scheme;
    }

    public boolean isEntityExplosionDisabled() {
        return true; //TODO Make this configurable
    }

    public boolean isIgnitionDisabled() {
        return ignitionDisabled;
    }

    public boolean isFlintAndSteelEnabled() {
        return flintAndSteelEnabled;
    }

    public boolean isFireSpreadingDisabled() {
        return fireSpreadingDisabled;
    }

    public boolean isBlocked(ItemStack item) {
        return isBlocked(item.getTypeId(), item.getData().getData());
    }

    public boolean isBlocked(Block block) {
        return isBlocked(block.getTypeId(), block.getData());
    }

    private boolean isBlocked(int typeId, byte data) {
        for (Item i : blockedItems) {
            if (i.getId() == typeId && i.getData() == data)
                return true;
        }
        return false;
    }

    public boolean isHitProtectionEnabled() {
        return hitProtectionEnabled;
    }

    public boolean isProjectileProtectionEnabled() {
        return projectileProtectionEnabled;
    }

    public boolean getShowWelcomeMessage() {
        return showWelcomeMessage;
    }

    public boolean isEndermenChangeBlockEnabled() {
        return endermenChangeBlockEnabled;
    }

    public boolean isEggThrowingDisabled() {
        return eggThrowingDisabled;
    }
}