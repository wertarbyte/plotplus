package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.SimplePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * A restricted area.
 */
public interface RestrictedArea {
    /**
     * Gets the owner of this restricted area.
     *
     * @return the owner of this restricted area
     */
    public SimplePlayer getOwner();

    public boolean mayEnter(Player player);

    public boolean mayEnter(UUID uniqueId);

    public boolean mayBuild(Player player);

    public boolean mayBuild(UUID uniqueId);

    /**
     * Checks if any player may hit entities in this area.
     *
     * @return true if any player may hit entities in this area, false if not
     */
    public boolean mayHitEntities();

    /**
     * Checks if any player may hit entities using projectiles (i.e arrows) in this area.
     *
     * @return true if any player may hit entities using projectiles in this area, false if not
     */
    public boolean mayHitEntitiesWithProjectiles();
}
