package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotData;

/**
 * A plot's configuration.
 */
class PlotConfigurationImpl implements PlotConfiguration {
    private Plot plot;

    private String name;
    private Boolean showWelcomeMessage;
    private Boolean enableProjectileProtection;
    private Boolean enableHitProtection;
    private boolean isProtected;

    public PlotConfigurationImpl() {
    }

    /**
     * Gets the name of this plot, if any.
     *
     * @return The name of this plot or null if it has no name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this plot.
     *
     * @param newName The new name of this plot or null to remove the name
     * @throws de.craften.plugins.plotplus.persistence.PersistenceException If setting the name fails
     */
    public void setName(String newName) throws PersistenceException {
        plot.getWorld().getManager().setName(plot, newName);
        name = newName;
    }

    /**
     * Checks if this plot has a name.
     *
     * @return True if this plot has a name, false if it doesn't
     */
    public boolean hasName() {
        return name != null && !getName().trim().isEmpty();
    }

    public boolean getShowWelcomeMessage() {
        return showWelcomeMessage != null ? showWelcomeMessage : plot.getWorld().getConfig().getShowWelcomeMessage();
    }

    /**
     * Enables or disables the welcome message on this plot.
     *
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if changing this option failed
     */
    public void setShowWelcomeMessage(boolean isEnabled) throws PersistenceException {
        plot.getWorld().getManager().setWelcomeMessageEnabled(plot, isEnabled);
        showWelcomeMessage = isEnabled;
    }

    public boolean getEnableProjectileProtection() {
        return enableProjectileProtection != null ? enableProjectileProtection : plot.getWorld().getConfig().isProjectileProtectionEnabled();
    }

    /**
     * Enables or disables the projectile protection on this plot.
     *
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if changing this option failed
     */
    public void setEnableProjectileProtection(Boolean isEnabled) throws PersistenceException {
        plot.getWorld().getManager().setProjectileProtectionEnabled(plot, isEnabled);
        this.enableProjectileProtection = isEnabled;
    }

    public Boolean getEnableHitProtection() {
        return enableHitProtection != null ? enableHitProtection : plot.getWorld().getConfig().isHitProtectionEnabled();
    }

    /**
     * Enables or disables the hit protection on this plot.
     *
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if changing this option failed
     */
    public void setEnableHitProtection(Boolean isEnabled) throws PersistenceException {
        plot.getWorld().getManager().setHitProtectionEnabled(plot, isEnabled);
        this.enableHitProtection = isEnabled;
    }

    @Override
    public boolean isProtected() {
        return isProtected;
    }

    @Override
    public void setProtected(boolean isProtected) throws PersistenceException {
        plot.getWorld().setProtected(plot, isProtected);
        this.isProtected = isProtected;
    }

    public void setData(PlotData plotData) {
        if (plotData.getName() != null && !plotData.getName().isEmpty()) {
            name = plotData.getName();
        } else {
            name = null;
        }

        showWelcomeMessage = plotData.getIsWelcomeMessageEnabled();
        enableProjectileProtection = plotData.getIsProjectileProtectionEnabled();
        enableHitProtection = plotData.getIsHitProtectionEnabled();
        isProtected = plotData.isProtected();
    }

    public void setPlot(Plot plot) {
        this.plot = plot;
    }
}
