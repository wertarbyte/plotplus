package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.SimplePlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * The area between the core regions of connected plots.
 */
public class MultiPlotArea implements RestrictedArea {
    private List<Plot> plots;

    public MultiPlotArea(List<Plot> plots) {
        this.plots = plots;
    }

    @Override
    public SimplePlayer getOwner() {
        return plots.get(0).getOwner();
    }

    @Override
    public boolean mayEnter(Player player) {
        return mayEnter(player.getUniqueId());
    }

    @Override
    public boolean mayEnter(UUID uniqueId) {
        for (Plot p : plots)
            if (p.mayEnter(uniqueId))
                return true;
        return false;
    }

    @Override
    public boolean mayBuild(Player player) {
        return mayBuild(player.getUniqueId());
    }

    @Override
    public boolean mayBuild(UUID uniqueId) {
        for (Plot p : plots)
            if (p.mayBuild(uniqueId))
                return true;
        return false;
    }

    @Override
    public boolean mayHitEntities() {
        for (Plot p : plots)
            if (!p.mayHitEntities())
                return false;
        return true;
    }

    @Override
    public boolean mayHitEntitiesWithProjectiles() {
        for (Plot p : plots)
            if (!p.mayHitEntitiesWithProjectiles())
                return false;
        return true;
    }
}
