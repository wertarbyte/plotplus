package de.craften.plugins.plotplus.plot.world;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.PlotListener;
import de.craften.plugins.plotplus.api.PlotListenerAdapter;
import de.craften.plugins.plotplus.generator.BlockProvider;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.*;
import de.craften.plugins.plotplus.util.Callback;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.WorldUtil;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import de.craften.plugins.plotplus.util.region.Region;
import de.craften.plugins.plotplus.worldtypes.ChunkResettable;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class PlotWorld {
    private PlotWorldConfiguration configuration;
    private PlotManager manager;
    private String name;
    private PlotListener api;

    /**
     * Creates a new PlotWorld instance that represents a plot world.
     *
     * @param name          The name of this world
     * @param configuration The configuration for this plot world
     * @param manager       Manager to save/load plots to/from
     */
    public PlotWorld(String name, PlotWorldConfiguration configuration, PlotManager manager) {
        this(name, configuration, manager, new PlotListenerAdapter() {
        });
    }

    /**
     * Creates a new PlotWorld instance that represents a plot world.
     *
     * @param name          The name of this world
     * @param configuration The configuration for this plot world
     * @param manager       Manager to save/load plots to/from
     * @param eventHandler  event handler for events in this world
     */
    public PlotWorld(String name, PlotWorldConfiguration configuration, PlotManager manager, PlotListener eventHandler) {
        this.configuration = configuration;
        this.manager = manager;
        this.name = name;
        this.api = eventHandler;

        PlotPlus.getApi().addListener(this, new PlotListenerAdapter() {
            @Override
            public void onPlotClaimed(Plot plot) {
                invalidatePlotsAround(plot);
            }

            @Override
            public void onPlotRemoved(Plot plot) {
                invalidatePlotsAround(plot);
            }

            @Override
            public void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner) {
                invalidatePlotsAround(plot);
            }

            @Override
            public void onPlotConnectionChanged(Plot a, Plot b, boolean linkedNow) {
                invalidatePlotsAround(a);
                invalidatePlotsAround(b);
            }

            private void invalidatePlotsAround(Plot plot) {
                //Invalidate all build regions around the claimed plots as new connections might be created
                doForPlotsAround(plot, new Callback<Plot>() {
                    @Override
                    public void call(Plot plot) {
                        plot.invalidateBuildRegion();
                    }
                });
            }
        });
    }

    /**
     * Gets the scheme of this world.
     *
     * @return The scheme of this world
     */
    public PlotWorldScheme getScheme() {
        return getConfig().getScheme();
    }

    /**
     * Gets the name of this world.
     *
     * @return The name of this world
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the configuration of this world.
     *
     * @return The configuration of this world
     */
    public PlotWorldConfiguration getConfig() {
        return configuration;
    }

    /**
     * Gets the manager that saves and loads plots in this world.
     *
     * @return The manager that saves and loads plots in this world
     */
    public PlotManager getManager() {
        return manager;
    }

    /**
     * Gets the API hub of this world.
     *
     * @return The API hub of this world
     */
    public PlotListener getApi() {
        return api;
    }

    /**
     * Returns the plot with the core build region at the given location.
     *
     * @param location Location to look at
     * @return Plot that has its core build region at the given location
     */
    public Plot getPlotAt(Location location) {
        if (getScheme().isInBuildZone(location)) {
            PointXZ plotId = getScheme().getPlotIdAt(location);
            return manager.getPlot(plotId.getX(), plotId.getZ(), this);
        }
        return null;
    }

    /**
     * Checks if the given location is inside a plot. The plot doesn't need to be claimed.
     *
     * @param location Location to look at
     * @return True if the given location is on a plot
     */
    public boolean isOnPlot(Location location) {
        return getScheme().isInBuildZone(location);
    }

    /**
     * Gets the effective plot area at the given location. On worlds with connected plots enabled, this may be a
     * combination of multiple plots, with blocked players, helpers and owners combined.
     *
     * @param location Location to look at
     * @return The effective plot area at the given location, null if there is no plot area at the given location
     */
    public RestrictedArea getEffectivePlotAt(Location location) {
        PointXZ plotId = getScheme().getPlotIdAt(location);

        Plot firstPlot = manager.getPlot(plotId.getX(), plotId.getZ(), this);

        if (firstPlot == null) {
            return null;
        }

        if (firstPlot.getCoreBuildRegion().contains(location)) {
            return firstPlot;
        }

        if (!getConfig().isConnectedPlotsEnabled()) {
            return null;
        }

        /*
         Okay, but if we have connected plots, 2-4 plots could intersect (meaning that we need to allow ANY helper
         and owner to build and block ANY blocked player from one of the connected plots.
         So we iterate through all plots around our first one and take all that contain this location.
         */
        List<Plot> plots = new ArrayList<>(9);

        for (int x = -1; x <= 1; x++) {
            for (int z = -1; z <= 1; z++) {
                Plot anotherPlot = getPlot(firstPlot.getIdX() + x, firstPlot.getIdZ() + z);
                if (anotherPlot != null && (anotherPlot.isConnectedWith(firstPlot) || firstPlot.equals(anotherPlot)) && anotherPlot.getBuildRegion().contains(location))
                    plots.add(anotherPlot);
            }
        }

        return plots.isEmpty() ? null : new MultiPlotArea(plots);
    }

    public Plot getPlot(int idX, int idZ) {
        return manager.getPlot(idX, idZ, this);
    }

    public Plot getPlotRelative(Plot plot, int relX, int relZ) {
        return manager.getPlot(plot.getIdX() + relX, plot.getIdZ() + relZ, this);
    }

    /**
     * Gets all plots in this world.
     *
     * @return All plots in this world
     */
    public Iterable<Plot> getPlots() {
        return manager.getPlots(this);
    }

    /**
     * Gets all plots in this world that are owned by the player with the given UUID.
     *
     * @param owner Owner of the plots to return
     * @return All plots in this world that are owned by the player with the given UUID
     */
    public List<Plot> getPlotsByOwner(UUID owner) {
        List<Plot> plots = new ArrayList<>();
        for (Plot p : getPlots()) {
            if (p.getOwner().getUniqueId().equals(owner))
                plots.add(p);
        }
        return plots;
    }

    public List<Plot> getPlotsByHelper(UUID uniqueId) {
        List<Plot> plots = new ArrayList<>();
        for (Plot p : getPlots()) {
            if (!p.getOwner().getUniqueId().equals(uniqueId) && p.mayBuild(uniqueId))
                plots.add(p);
        }
        return plots;
    }

    public Plot createPlotAt(Location location, OfflinePlayer player) throws PersistenceException {
        if (getPlotAt(location) == null) {
            PointXZ plotId = getScheme().getPlotIdAt(location);
            Plot claimed = manager.createPlot(plotId.getX(), plotId.getZ(), manager.getPlayerCache().getCached(player), this);

            //Invalidate all build regions around the claimed plots as new connections might be created
            doForPlotsAround(claimed, new Callback<Plot>() {
                @Override
                public void call(Plot plot) {
                    plot.invalidateBuildRegion();
                }
            });

            api.onPlotClaimed(claimed);

            return claimed;
        } else {
            return null;
        }
    }

    private void doForPlotsAround(Plot plot, Callback<Plot> callable) {
        for (int x = -1; x <= 1; x++) {
            for (int z = -1; z <= 1; z++) {
                Plot p = getPlot(plot.getIdX() + x, plot.getIdZ() + z);
                if (p != plot && p != null) {
                    callable.call(p);
                }
            }
        }
    }

    /**
     * Claims the first free plot that is found, starting in the center of the map.
     *
     * @param player Player to claim this plot for
     * @return Plot that was claimed
     */
    public Plot claimAutomatically(Player player) throws PersistenceException {
        SimplePlayer sp = manager.getPlayerCache().getCached(player);
        Plot claimed = null;

        int x = 0;
        int z = 0;
        while (claimed == null) {
            for (int px = -x; px <= x; px++) {
                if (!manager.isPlotClaimed(px, -z, this)) {
                    claimed = manager.createPlot(px, -z, sp, this);
                    break;
                }
                if (!manager.isPlotClaimed(px, z, this)) {
                    claimed = manager.createPlot(px, z, sp, this);
                    break;
                }
            }

            if (claimed == null) {
                for (int pz = -z; pz <= z; pz++) {
                    if (!manager.isPlotClaimed(-x, pz, this)) {
                        claimed = manager.createPlot(-x, pz, sp, this);
                        break;
                    }
                    if (!manager.isPlotClaimed(x, pz, this)) {
                        claimed = manager.createPlot(x, pz, sp, this);
                        break;
                    }
                }
            }

            x++;
            z++;
        }

        api.onPlotClaimed(claimed);

        return claimed;
    }

    /**
     * Clears the given plot if it's not protected.
     *
     * @param plot Plot to clear
     */
    public void clearPlot(Plot plot) {
        if (plot.isProtected())
            return;
        api.onBeforePlotCleared(plot);
        performClearPlot(plot);
        api.onPlotCleared(plot);
    }

    /**
     * Clears the given plot if it's not protected.
     *
     * @param idX x coordinate if a plot
     * @param idZ z coordinate if a plot
     */
    public void clearPlot(int idX, int idZ) {
        Plot plot = getPlot(idX, idZ);
        if (plot == null) {
            plot = new Plot(new PlotId(idX, idZ, getName()), new SimplePlayer(UUID.randomUUID(), "Herobrine"), this,
                    Collections.<UUID, SimplePlayer>emptyMap(), Collections.<UUID, SimplePlayer>emptyMap(),
                    new PlotConfiguration() {
                        @Override
                        public String getName() {
                            return "";
                        }

                        @Override
                        public void setName(String newName) throws PersistenceException {

                        }

                        @Override
                        public boolean hasName() {
                            return false;
                        }

                        @Override
                        public boolean getShowWelcomeMessage() {
                            return false;
                        }

                        @Override
                        public void setShowWelcomeMessage(boolean isEnabled) throws PersistenceException {

                        }

                        @Override
                        public boolean getEnableProjectileProtection() {
                            return false;
                        }

                        @Override
                        public void setEnableProjectileProtection(Boolean enableProjectileProtection) throws PersistenceException {

                        }

                        @Override
                        public Boolean getEnableHitProtection() {
                            return null;
                        }

                        @Override
                        public void setEnableHitProtection(Boolean enableHitProtection) throws PersistenceException {

                        }

                        @Override
                        public boolean isProtected() {
                            return false;
                        }

                        @Override
                        public void setProtected(boolean isProtected) throws PersistenceException {

                        }
                    });
        }
        clearPlot(plot);
    }

    /**
     * Clears the given plot.
     *
     * @param plot Plot to clear
     */
    private void performClearPlot(Plot plot) {
        World world = Bukkit.getWorld(getName());
        Region buildRegion = plot.getBuildRegion();

        if (getScheme().getStructure() instanceof BlockProvider) {
            BlockProvider provider = (BlockProvider) getScheme().getStructure();
            for (PointXZ p : buildRegion)
                for (int y = 0; y < world.getMaxHeight(); y++) {
                    Block b = provider.getBlockAt(p.getX(), y, p.getZ());
                    if (b != null)
                        b.applyOn(world.getBlockAt(p.getX(), y, p.getZ()));
                    else
                        world.getBlockAt(p.getX(), y, p.getZ()).setType(Material.AIR);
                }

            //Manually remove all entities (except players of course)
            for (Entity e : plot.getWorld().getBukkitWorld().getEntities()) {
                if (!(e instanceof Player) && buildRegion.contains(e.getLocation()))
                    e.remove();
            }
        } else if (getScheme().getStructure() instanceof ChunkResettable) {
            //If this isn't the case, the plot can't be reset
            RectangularRegion rbuildRegion = getScheme().getFullPlotRegion(plot.getIdX(), plot.getIdZ());
            int cxMin = (int) Math.floor((double) rbuildRegion.getXMin() / 16);
            int cxMax = (int) Math.floor((double) rbuildRegion.getXMax() / 16);
            int czMin = (int) Math.floor((double) rbuildRegion.getZMin() / 16);
            int czMax = (int) Math.floor((double) rbuildRegion.getZMax() / 16);

            for (int x = cxMin; x <= cxMax; x++) {
                for (int z = czMin; z <= czMax; z++) {
                    WorldUtil.resetChunk(world, x, z);
                }
            }
        }
    }

    /**
     * Resets the given plot if it's not protected.
     *
     * @param plot Plot to reset
     * @throws PersistenceException If removing the plot fails
     */
    public void resetPlot(Plot plot) throws PersistenceException {
        if (plot.isProtected())
            return;

        performClearPlot(plot);
        plot.setBiome(getConfig().getDefaultBiome());

        api.onPlotRemoved(plot);
        manager.removePlot(plot); //TODO Create a snapshot of the plot and call this before actually removing the plot
    }

    public void setOwner(Plot plot, SimplePlayer newOwner) throws PersistenceException {
        manager.setOwner(plot, newOwner);
    }

    /**
     * Gets a {@link BukkitRunnable} that resets all plots in the world that have less than <code>minModified</code>
     * modified blocks. That runnable must be started as synchronous, repeating task and will cancel itself automatically.
     *
     * @param minModified Minimum number of modified blocks a plot must have to not be removed
     * @return bukkit runnable that needs to be started as repeating, synchronous task to start cleaning
     */
    public BukkitRunnable getAutoCleaner(final double minModified) {
        final List<Plot> plots = new ArrayList<>(manager.getPlots(this));

        return new BukkitRunnable() {
            private int deleted = 0;
            private int plotIndex = 0;

            @Override
            public void run() {
                if (plotIndex == 0) {
                    Bukkit.getLogger().info("[PlotPlus] Auto clean started on world '" + getName() + "'");
                }
                if (plotIndex == plots.size()) {
                    cancel();
                    Bukkit.getLogger().info(String.format("[PlotPlus] Auto clean done, %d plots removed", deleted));
                    return;
                }

                Plot plot = plots.get(plotIndex);
                if (!plot.isProtected() && plot.getModifiedBlocksCount() < minModified)
                    try {
                        resetPlot(plot);
                        deleted++;
                    } catch (PersistenceException e) {
                        Bukkit.getLogger().log(Level.WARNING, "Could not reset plot " + plot.getId(), e);
                    }
                plotIndex++;
                Bukkit.getLogger().info(String.format("[PlotPlus] %d of %d plots checked, %d plots removed",
                        plotIndex, plots.size(), deleted));
            }
        };
    }

    public void setProtected(Plot plot, boolean isProtected) throws PersistenceException {
        manager.setProtected(plot, isProtected);
        api.onPlotProtectionChanged(plot, isProtected);
    }

    public void connectPlot(Plot plot, Plot connectTo) throws PersistenceException {
        boolean connectedBefore = plot.isConnectedWith(connectTo);
        manager.connectPlot(plot, connectTo);
        boolean connectedNow = plot.isConnectedWith(connectTo);
        if (!connectedBefore && connectedNow) {
            api.onPlotConnectionChanged(plot, connectTo, true);
        }
    }

    public void disconnectPlot(Plot plot, Plot disconnectFrom) throws PersistenceException {
        boolean connectedBefore = plot.isConnectedWith(disconnectFrom);
        manager.disconnectPlot(plot, disconnectFrom);
        if (connectedBefore) {
            api.onPlotConnectionChanged(plot, disconnectFrom, false);
        }
    }

    /**
     * Returns the bukkit world for of world.
     *
     * @return The bukkit world of this world
     */
    public World getBukkitWorld() {
        return Bukkit.getWorld(getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PlotWorld && ((PlotWorld) obj).getName().equals(getName());
    }

    public void onPlayerNameChanged(SimplePlayer player) {
        api.onPlayerNameChanged(player, getPlotsByOwner(player.getUniqueId()));
    }
}
