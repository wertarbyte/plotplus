package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.persistence.PersistenceException;

public interface PlotConfiguration {
    /**
     * Gets the name of this plot, if any.
     *
     * @return The name of this plot or null if it has no name
     */
    public String getName();

    /**
     * Sets the name of this plot.
     *
     * @param newName The new name of this plot or null to remove the name
     * @throws de.craften.plugins.plotplus.persistence.PersistenceException If setting the name fails
     */
    public void setName(String newName) throws PersistenceException;

    /**
     * Checks if this plot has a name.
     *
     * @return True if this plot has a name, false if it doesn't
     */
    public boolean hasName();

    /**
     * Checks if a welcome message should be displayed when players enter this plot. If this option is not configured for
     * this plot, the world's default value is returned.
     *
     * @return whether to show a welcome message when entering this plot
     */
    public boolean getShowWelcomeMessage();

    /**
     * Enables or disables the welcome message on this plot.
     *
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if changing this option failed
     */
    public void setShowWelcomeMessage(boolean isEnabled) throws PersistenceException;

    public boolean getEnableProjectileProtection();

    public void setEnableProjectileProtection(Boolean enableProjectileProtection) throws PersistenceException;

    public Boolean getEnableHitProtection();

    public void setEnableHitProtection(Boolean enableHitProtection) throws PersistenceException;

    public boolean isProtected();

    public void setProtected(boolean isProtected) throws PersistenceException;
}
