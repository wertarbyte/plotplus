package de.craften.plugins.plotplus.plot;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.PlayerCache;
import de.craften.plugins.plotplus.persistence.PlotPersistence;
import de.craften.plugins.plotplus.persistence.entities.PlotConnection;
import de.craften.plugins.plotplus.persistence.entities.PlotData;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldContainer;

import java.util.*;

public class PlotManager {
    private PlotPersistence persistence;
    private PlotWorldContainer worlds;
    private ListMultimap<String, Plot> plots = ArrayListMultimap.create();
    private Map<PlotId, Plot> plotsById = new HashMap<>();
    private Multimap<PlotId, PlotId> plotConnections = HashMultimap.create();
    private PlayerCache playerCache;

    public PlotManager(PlotPersistence persistence, PlotWorldContainer worlds) {
        this.persistence = persistence;
        this.worlds = worlds;
        playerCache = new PlayerCache(persistence);
    }

    public void initialize() throws PersistenceException {
        persistence.setup();

        for (PlotData plot : persistence.getPlots()) {
            cachePlot(plot);
        }

        for (PlotConnection connection : persistence.getPlotConnections()) {
            plotConnections.put(connection.getA(), connection.getB());
        }
    }

    private Plot cachePlot(PlotData plotData) {
        Map<UUID, SimplePlayer> helpers = new HashMap<>();
        Map<UUID, SimplePlayer> blockedPlayers = new HashMap<>();

        for (SimplePlayer helper : plotData.getHelpers())
            helpers.put(helper.getUniqueId(), playerCache.getCached(helper));

        for (SimplePlayer blocked : plotData.getBlockedPlayers())
            blockedPlayers.put(blocked.getUniqueId(), playerCache.getCached(blocked));

        PlotConfigurationImpl plotConfiguration = new PlotConfigurationImpl();
        Plot plot = new Plot(
                plotData.getId(),
                playerCache.getCachedWithoutUpdate(plotData.getOwner()),
                worlds.getWorld(plotData.getId().getWorld()),
                helpers, blockedPlayers,
                plotConfiguration);
        plotConfiguration.setData(plotData);
        plotConfiguration.setPlot(plot);

        plots.put(plotData.getId().getWorld(), plot);

        plotsById.put(plot.getId(), plot);

        return plot;
    }

    private void uncachePlot(Plot plot) {
        plots.get(plot.getWorld().getName()).remove(plot);
        plotsById.remove(plot.getId());
    }

    public List<Plot> getPlots(PlotWorld world) {
        List<Plot> plots = this.plots.get(world.getName());
        if (plots == null)
            plots = Collections.emptyList();
        return Collections.unmodifiableList(plots);
    }

    public List<Plot> getPlotsByPlayer(String player) {
        List<Plot> plots = new ArrayList<>();
        for (Plot p : plotsById.values()) {
            if (player.equals(p.getOwner().getDisplayName()))
                plots.add(p);
        }
        return plots;
    }

    public List<Plot> getPlotsByHelper(UUID helper) {
        List<Plot> plots = new ArrayList<>();
        for (Plot p : plotsById.values()) {
            if (!p.getOwner().getUniqueId().equals(helper) && p.mayBuild(helper))
                plots.add(p);
        }
        return plots;
    }

    public List<Plot> getPlotsByHelper(String helper) {
        List<Plot> plots = new ArrayList<>();
        for (Plot p : plotsById.values()) {
            for (SimplePlayer h : p.getHelpers()) {
                if (helper.equals(h.getDisplayName())) {
                    plots.add(p);
                    break;
                }
            }
        }
        return plots;
    }

    public Plot getPlot(int idX, int idZ, PlotWorld world) {
        return plotsById.get(new PlotId(idX, idZ, world.getName()));
    }

    public Plot createPlot(int idX, int idZ, SimplePlayer owner, PlotWorld world) throws PersistenceException {
        PlotData plot = new PlotData(idX, idZ, world.getName(), owner);
        persistence.createPlot(plot);
        return cachePlot(plot);
    }

    public boolean isPlotClaimed(int idX, int idZ, PlotWorld world) {
        return plotsById.containsKey(new PlotId(idX, idZ, world.getName()));
    }

    public void removePlot(Plot plot) throws PersistenceException {
        persistence.removePlot(plot.getId());
        uncachePlot(plot);

        persistence.deletePlotConnections(plot.getId());
        plotConnections.removeAll(plot.getId());
        for (Map.Entry<PlotId, PlotId> connection : new ArrayList<>(plotConnections.entries())) {
            if (connection.getKey().equals(plot.getId()) || connection.getValue().equals(plot.getId())) {
                plotConnections.remove(connection.getKey(), connection.getValue());
            }
        }
    }

    public void addHelper(Plot plot, SimplePlayer helper) throws PersistenceException {
        persistence.addHelper(plot.getId(), helper);

    }

    public void removeHelper(Plot plot, UUID helper) throws PersistenceException {
        persistence.removeHelper(plot.getId(), helper);
    }

    public void addBlockedPlayer(Plot plot, SimplePlayer blockedPlayer) throws PersistenceException {
        persistence.addBlockedPlayer(plot.getId(), blockedPlayer);
    }

    public void removeBlockedPlayer(Plot plot, UUID blockedPlayer) throws PersistenceException {
        persistence.removeBlockedPlayer(plot.getId(), blockedPlayer);
    }

    /**
     * Sets the owner of the given plot.
     *
     * @param plot  Plot to set the owner of
     * @param owner New owner
     * @throws PersistenceException If setting the owner fails
     */
    public void setOwner(Plot plot, SimplePlayer owner) throws PersistenceException {
        persistence.setOwner(plot.getId(), owner);
    }

    public void setProtected(Plot plot, boolean isProtected) throws PersistenceException {
        persistence.setProtected(plot.getId(), isProtected);
    }

    public void setName(Plot plot, String newName) throws PersistenceException {
        persistence.setName(plot.getId(), newName);
    }

    /**
     * Enables or disables the welcome message on the given plot.
     *
     * @param plot      plot to enable/disable the welcome message on
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if setting this option failed
     */
    public void setWelcomeMessageEnabled(Plot plot, boolean isEnabled) throws PersistenceException {
        persistence.setWelcomeMessageEnabled(plot.getId(), isEnabled);
    }

    /**
     * Enables or disables the projectile protection on the given plot.
     *
     * @param plot      plot to enable/disable the projectile protection on
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if setting this option failed
     */
    public void setProjectileProtectionEnabled(Plot plot, boolean isEnabled) throws PersistenceException {
        persistence.setProjectileProtectionEnabled(plot.getId(), isEnabled);
    }

    /**
     * Enables or disables the hit protection on the given plot.
     *
     * @param plot      plot to enable/disable the projectile protection on
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException if setting this option failed
     */
    public void setHitProtectionEnabled(Plot plot, boolean isEnabled) throws PersistenceException {
        persistence.setHitProtectionEnabled(plot.getId(), isEnabled);
    }

    public void updatePlayer(UUID uuid, String player) throws PersistenceException {
        persistence.savePlayer(playerCache.getCached(uuid, player));
    }

    public PlayerCache getPlayerCache() {
        return playerCache;
    }

    /**
     * Checks if the given plots are connected. This requires plot a to be connected to be <i>and</i>
     * plot b to be connected to plot a.
     *
     * @param a a plot
     * @param b another plot
     * @return true if the plots are connected, false otherwise
     */
    public boolean checkPlotsConnected(Plot a, Plot b) {
        return plotConnections.containsEntry(a.getId(), b.getId()) && plotConnections.containsEntry(b.getId(), a.getId());
    }

    /**
     * Checks if plot a is connected to plot b, in that direction.
     *
     * @param a a plot
     * @param b another plot
     * @return true if plot a is connected to plot b
     */
    public boolean isPlotConnectedTo(Plot a, Plot b) {
        return plotConnections.containsEntry(a.getId(), b.getId());
    }

    /**
     * Connects the given plot a to plot b, but only in this direction.
     *
     * @param a a plot
     * @param b another plot
     * @throws PersistenceException if connecting the plots failed
     */
    public void connectPlot(Plot a, Plot b) throws PersistenceException {
        persistence.createPlotConnection(a.getId(), b.getId());
        plotConnections.put(a.getId(), b.getId());
    }

    /**
     * Disconnects the given plot a from plot b, but only in this direction.
     *
     * @param a a plot
     * @param b another plot
     * @throws PersistenceException if disconnecting the plots failed
     */
    public void disconnectPlot(Plot a, Plot b) throws PersistenceException {
        persistence.deletePlotConnections(a.getId(), b.getId());
        plotConnections.remove(a.getId(), b.getId());
    }
}
