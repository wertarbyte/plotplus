package de.craften.plugins.plotplus.plot.world;

import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.WorldUtil;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public abstract class PlotWorldScheme {
    private PlotStructure structure;

    public PlotWorldScheme(PlotStructure structure) {
        this.structure = structure;
    }

    public PlotStructure getStructure() {
        return structure;
    }

    public abstract int getNorthOffset();

    public abstract int getEastOffset();

    public abstract int getSouthOffset();

    public abstract int getWestOffset();

    public boolean isInBuildZone(Location location) {
        PointXZ relPos = getStructure().getRelativePosition(location.getBlockX(), location.getBlockZ());
        return relPos.getX() >= getWestOffset() && relPos.getX() < (getStructure().getWidth() - getEastOffset()) &&
                relPos.getZ() >= getNorthOffset() && relPos.getZ() < (getStructure().getLength() - getSouthOffset());
    }

    public PointXZ getPlotIdAt(Location location) {
        int x = location.getBlockX() / getStructure().getWidth();
        if (location.getBlockX() < 0) {
            x -= 1;
        }

        int z = location.getBlockZ() / getStructure().getLength();
        if (location.getBlockZ() < 0) {
            z -= 1;
        }

        return new PointXZ(x, z);
    }

    /**
     * Calculates the core build region of the plot with the given coordinate.
     *
     * @param idX X-coordinate of the plot
     * @param idZ Z-coordinate of the plot
     * @return Core build region of the plot
     */
    public RectangularRegion getCoreBuildRegion(int idX, int idZ) {
        return new RectangularRegion(
                idX * getStructure().getWidth() + getWestOffset(),
                idZ * getStructure().getLength() + getNorthOffset(),
                idX * getStructure().getWidth() + getStructure().getWidth() - getEastOffset() - 1,
                idZ * getStructure().getLength() + getStructure().getLength() - getSouthOffset() - 1
        );
    }

    /**
     * Gets the full region of the plot with the given coordinate, including padding.
     *
     * @param idX X-coordinate of the plot
     * @param idZ Z-coordinate of the plot
     * @return Full region of the plot
     */
    public RectangularRegion getFullPlotRegion(int idX, int idZ) {
        return RectangularRegion.byStartAndSize(
                idX * getStructure().getWidth(),
                idZ * getStructure().getLength(),
                getStructure().getWidth(),
                getStructure().getLength()
        );
    }

    /**
     * Gets called after the world that uses this scheme had been loaded.
     *
     * @param world the loaded world that uses this scheme
     */
    public abstract void onLoaded(PlotWorld world);

    /**
     * Gets the location to teleport a player to when he teleports to the given plot.
     *
     * @param world world of the plot
     * @param idX   x-ID of the plot
     * @param idZ   z-ID of the plot
     * @return location to teleport a player to when he teleports to the given plot
     */
    public Location getTeleportLocation(PlotWorld world, int idX, int idZ) {
        World bukkitWorld = world.getBukkitWorld();
        RectangularRegion cr = getCoreBuildRegion(idX, idZ);
        double x = (cr.getXMin() + cr.getXMax()) / 2;
        double z = cr.getZMin() + 2;
        double y = WorldUtil.topmostSafeLocation(bukkitWorld, x, z);

        return new Location(bukkitWorld, x, y, z).setDirection(new Vector(0, 0, 1));
    }
}
