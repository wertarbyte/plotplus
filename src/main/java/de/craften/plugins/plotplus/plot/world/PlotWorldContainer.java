package de.craften.plugins.plotplus.plot.world;

import org.bukkit.World;

/**
 * A container for plot worlds,
 */
public interface PlotWorldContainer {
    /**
     * Gets the PlotWorld instance of the given world.
     *
     * @param world World to get the PlotWorld instance of
     * @return PlotWorld instance of the given world
     */
    public PlotWorld getWorld(World world);

    /**
     * Gets the PlotWorld instance of the world with the given name.
     *
     * @param name Name of the world to get the PlotWorld instance of
     * @return PlotWorld instance of the world with the given name
     */
    public PlotWorld getWorld(String name);
}
