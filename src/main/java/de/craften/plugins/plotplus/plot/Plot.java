package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.generator.BlockProvider;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.region.CombinedRegion;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import de.craften.plugins.plotplus.util.region.Region;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;

import java.lang.ref.SoftReference;
import java.util.*;

public class Plot implements RestrictedArea {
    private PlotWorld world;
    private PlotId position;
    private SimplePlayer owner;

    private PlotConfiguration config;

    private Map<UUID, SimplePlayer> helpers;
    private Map<UUID, SimplePlayer> blockedPlayers;

    private SoftReference<Region> buildRegion = null;

    public Plot(PlotId position, SimplePlayer owner, PlotWorld world,
                Map<UUID, SimplePlayer> helpers, Map<UUID, SimplePlayer> blockedPlayers, PlotConfiguration config) {
        this.position = position;
        this.owner = owner;
        this.world = world;

        this.helpers = helpers != null ? helpers : new HashMap<UUID, SimplePlayer>();
        this.blockedPlayers = blockedPlayers != null ? blockedPlayers : new HashMap<UUID, SimplePlayer>();

        this.config = config;
    }

    @Override
    public boolean mayEnter(Player player) {
        return mayEnter(player.getUniqueId());
    }

    @Override
    public boolean mayEnter(UUID uniqueId) {
        return !blockedPlayers.containsKey(uniqueId);
    }

    @Override
    public boolean mayBuild(Player player) {
        return mayBuild(player.getUniqueId());
    }

    @Override
    public boolean mayBuild(UUID uniqueId) {
        return mayManage(uniqueId) || helpers.containsKey(uniqueId);
    }

    @Override
    public boolean mayHitEntities() {
        return !getConfig().getEnableHitProtection();
    }

    @Override
    public boolean mayHitEntitiesWithProjectiles() {
        return !getConfig().getEnableProjectileProtection();
    }

    public boolean mayManage(Player player) {
        return mayManage(player.getUniqueId());
    }

    public boolean mayManage(UUID uniqueId) {
        return getOwner().getUniqueId().equals(uniqueId);
    }

    @Override
    public String toString() {
        return String.format("Plot %d;%d", getIdX(), getIdZ());
    }

    /**
     * Gets the effective build region of this plot, including regions between this plot and connected plots.
     *
     * @return The effective build region of this plot
     */
    public Region getBuildRegion() {
        Region buildRegion = this.buildRegion != null ? this.buildRegion.get() : null;

        if (buildRegion == null) {
            PlotWorldScheme s = world.getScheme();
            RectangularRegion mainRegion = getCoreBuildRegion();

            CombinedRegion region = new CombinedRegion();
            region.add(mainRegion);

            if (getWorld().getConfig().isConnectedPlotsEnabled()) {
                int idX = getIdX();
                int idZ = getIdZ();

                boolean cWest = isConnectedWithRelative(-1, 0);
                boolean cEast = isConnectedWithRelative(1, 0);
                boolean cNorth = isConnectedWithRelative(0, -1);
                boolean cSouth = isConnectedWithRelative(0, 1);

                if (cWest)
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMin() - s.getWestOffset() - s.getEastOffset(),
                            mainRegion.getZMin(),
                            s.getEastOffset() + s.getWestOffset(),
                            s.getStructure().getLength() - s.getNorthOffset() - s.getSouthOffset()
                    ));
                if (cEast)
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMax() + 1,
                            mainRegion.getZMin(),
                            s.getEastOffset() + s.getWestOffset(),
                            s.getStructure().getLength() - s.getNorthOffset() - s.getSouthOffset()
                    ));
                if (cNorth)
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMin(),
                            mainRegion.getZMin() - s.getNorthOffset() - s.getSouthOffset(),
                            s.getStructure().getWidth() - s.getEastOffset() - s.getWestOffset(),
                            s.getSouthOffset() + s.getNorthOffset()
                    ));
                if (cSouth)
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMin(),
                            mainRegion.getZMax() + 1,
                            s.getStructure().getWidth() - s.getEastOffset() - s.getWestOffset(),
                            s.getSouthOffset() + s.getNorthOffset()
                    ));

                if (cNorth && cEast && isConnectedWithRelative(1, -1))
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMax() + 1,
                            mainRegion.getZMin() - s.getNorthOffset() - s.getSouthOffset(),
                            s.getWestOffset() + s.getEastOffset(),
                            s.getNorthOffset() + s.getSouthOffset()
                    ));
                if (cEast && cSouth && isConnectedWithRelative(1, 1))
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMax() + 1,
                            mainRegion.getZMax() + 1,
                            s.getWestOffset() + s.getEastOffset(),
                            s.getNorthOffset() + s.getSouthOffset()
                    ));
                if (cWest && cNorth && isConnectedWithRelative(-1, -1))
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMin() - s.getWestOffset() - s.getEastOffset(),
                            mainRegion.getZMin() - s.getNorthOffset() - s.getSouthOffset(),
                            s.getWestOffset() + s.getEastOffset(),
                            s.getNorthOffset() + s.getSouthOffset()
                    ));
                if (cSouth && cWest && isConnectedWithRelative(-1, 1))
                    region.add(RectangularRegion.byStartAndSize(
                            mainRegion.getXMin() - s.getWestOffset() - s.getEastOffset(),
                            mainRegion.getZMax() + 1,
                            s.getWestOffset() + s.getEastOffset(),
                            s.getNorthOffset() + s.getSouthOffset()
                    ));
            }

            this.buildRegion = new SoftReference<Region>(region);
            return region;
        } else {
            return buildRegion;
        }
    }

    /**
     * Forces the cached build region to be recalculated. Call this when plot connections change.
     */
    public void invalidateBuildRegion() {
        buildRegion = null;
    }

    public Region getConnectedBuildRegion() {
        if (getWorld().getConfig().isConnectedPlotsEnabled()) {
            CombinedRegion region = new CombinedRegion();
            getConnectedBuildRegion(region, new HashSet<PlotId>());
            return region;
        } else {
            return getCoreBuildRegion();
        }
    }

    private void getConnectedBuildRegion(CombinedRegion regionToAddTo, Set<PlotId> alreadyIncluded) {
        if (alreadyIncluded.contains(getId()))
            return;
        regionToAddTo.add(getBuildRegion());
        alreadyIncluded.add(getId());
        for (int x = -1; x <= 1; x++)
            for (int z = -1; z <= 1; z++) {
                if (x == z) //no diagonal connections
                    continue;
                Plot other = getWorld().getPlot(getIdX() + x, getIdZ() + z);
                if (isConnectedWith(other)) {
                    other.getConnectedBuildRegion(regionToAddTo, alreadyIncluded);
                }
            }
    }

    /**
     * Gets the core region of this plot, excluding regions between this plot and connected plots.
     *
     * @return The core region of this plot
     */
    public RectangularRegion getCoreBuildRegion() {
        return getWorld().getScheme().getCoreBuildRegion(getIdX(), getIdZ());
    }

    /**
     * Gets the owner of this plot.
     *
     * @return The owner of this plot
     */
    @Override
    public SimplePlayer getOwner() {
        return owner;
    }

    /**
     * Gets the name of this plot, if any.
     *
     * @return The name of this plot or null if it has no name
     */
    @Deprecated
    public String getName() {
        return config.getName();
    }

    /**
     * Checks if this plot has a name.
     *
     * @return True if this plot has a name, false if it doesn't
     */
    @Deprecated
    public boolean hasName() {
        return config.hasName();
    }

    /**
     * Sets the owner of this plot.
     *
     * @param owner The new owner of this plot
     * @throws PersistenceException If setting the owner fails
     */
    public void setOwner(SimplePlayer owner) throws PersistenceException {
        SimplePlayer oldOwner = getOwner();
        setOwnerSilently(owner);
        getWorld().getApi().onPlotOwnerChanged(this, oldOwner);
    }

    /**
     * Sets the owner of this plot without calling listeners.
     * Usually you should use the {@link #setOwner(de.craften.plugins.plotplus.SimplePlayer)} method.
     *
     * @param owner The new owner of this plot
     * @throws PersistenceException If setting the owner fails
     */
    public void setOwnerSilently(SimplePlayer owner) throws PersistenceException {
        getWorld().setOwner(this, owner);
        this.owner = owner;
    }

    public PlotId getId() {
        return position;
    }

    public int getIdX() {
        return position.getIdX();
    }

    public int getIdZ() {
        return position.getIdZ();
    }

    public void addBlockedPlayer(SimplePlayer blocked) throws PersistenceException {
        getWorld().getManager().addBlockedPlayer(this, blocked);
        blockedPlayers.put(blocked.getUniqueId(), blocked);
    }

    /**
     * Unblocks the player with the given UUID from this plot.
     *
     * @param uuid UUID of the player to unblock
     * @throws PersistenceException If removing the blocked player fails
     */
    public void removeBlockedPlayer(UUID uuid) throws PersistenceException {
        getWorld().getManager().removeBlockedPlayer(this, uuid);
        blockedPlayers.remove(uuid);
    }

    public void addHelper(SimplePlayer helper) throws PersistenceException {
        getWorld().getManager().addHelper(this, helper);
        helpers.put(helper.getUniqueId(), helper);
    }

    public void removeHelper(UUID uuid) throws PersistenceException {
        getWorld().getManager().removeHelper(this, uuid);
        helpers.remove(uuid);
    }

    public Collection<SimplePlayer> getHelpers() {
        return helpers.values();
    }

    public Collection<SimplePlayer> getBlockedPlayers() {
        return blockedPlayers.values();
    }

    public boolean isProtected() {
        return getConfig().isProtected();
    }

    /**
     * Sets the biome in the build region of this plot.
     *
     * @param biome New biome type to set
     */
    public void setBiome(Biome biome) {
        Set<PointXZ> chunks = new HashSet<>();

        for (PointXZ p : getBuildRegion()) {
            getWorld().getBukkitWorld().setBiome(p.getX(), p.getZ(), biome);
            chunks.add(new PointXZ(p.getX() >> 4, p.getZ() >> 4));
        }

        for (PointXZ chunk : chunks)
            getWorld().getBukkitWorld().refreshChunk(chunk.getX(), chunk.getZ());
    }

    /**
     * Gets the biome of this plot.
     *
     * @return The biome of this plot
     */
    public Biome getBiome() {
        RectangularRegion r = getCoreBuildRegion();
        return getWorld().getBukkitWorld().getBiome(r.getXMin(), r.getZMin());
    }

    public boolean isConnectedWith(Plot plot) {
        return plot != null && (world.getConfig().isConnectedPlotsEnabled())
                && (plot.getOwner().equals(getOwner()) || getWorld().getManager().checkPlotsConnected(this, plot))
                && (Math.abs(plot.getIdX() - getIdX()) + Math.abs(plot.getIdZ() - getIdZ()) == 1);
    }

    /**
     * Checks if this plot is connected with the plot with the given ID
     *
     * @param idX X-component of the plot
     * @param idZ Z-component of the plot
     * @return True if the two plots are connected, false if not
     */
    public boolean isConnectedWith(int idX, int idZ) {
        return isConnectedWith(world.getPlot(idX, idZ));
    }

    public boolean isConnectedWithRelative(int relX, int relZ) {
        if (relX != 0 && relZ != 0) {
            return isConnectedWithRelative(relX, 0)
                    && isConnectedWithRelative(0, relZ)
                    && getWorld().getPlotRelative(this, relX, 0).isConnectedWithRelative(0, relZ)
                    && getWorld().getPlotRelative(this, 0, relZ).isConnectedWithRelative(relX, 0);
        } else {
            return isConnectedWith(getIdX() + relX, getIdZ() + relZ);
        }
    }

    /**
     * Checks if this plot is connected with any other plot.
     *
     * @return true if this plot is connected with any other plot, false if not
     */
    public boolean isConnected() {
        if (!getWorld().getConfig().isConnectedPlotsEnabled())
            return false;

        for (int x = -1; x <= 1; x++) {
            for (int z = -1; z <= 1; z++) {
                if (x != z && isConnectedWithRelative(x, z)) {
                    return true;
                }
            }
        }

        return false;
    }

    public PlotWorld getWorld() {
        return world;
    }

    public PlotConfiguration getConfig() {
        return config;
    }

    /**
     * Teleports the given player to this plot.
     *
     * @param player Player to teleport to this plot
     */
    public void teleportPlayer(Player player) {
        player.teleport(getWorld().getScheme().getTeleportLocation(getWorld(), getIdX(), getIdZ()));
    }

    /**
     * Gets the number of blocks that are different from the generated world.
     * Only works for worlds with schematic plot structures.
     *
     * @return Number of blocks that are different from the generated world or <code>Integer.MAX_VALUE</code> if this is not supported by the world
     */
    public int getModifiedBlocksCount() {
        if (getWorld().getScheme().getStructure() instanceof BlockProvider) {
            int modified = 0;

            World world = getWorld().getBukkitWorld();
            for (PointXZ p : getBuildRegion()) {
                for (int y = 0; y < world.getMaxHeight(); y++) {
                    Block original = ((BlockProvider) getWorld().getScheme().getStructure()).getBlockAt(p.getX(), y, p.getZ());
                    Material now = world.getBlockAt(p.getX(), y, p.getZ()).getType();
                    if ((original == null && now != Material.AIR) || (original != null && now.getId() != original.getId()))
                        modified++;
                }
            }
            return modified;
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public int hashCode() {
        return getIdX() + getIdZ() * 11 + 13 * getWorld().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Plot) {
            Plot other = (Plot) obj;
            return getIdX() == other.getIdX() && getIdZ() == other.getIdZ() && getWorld().equals(other.getWorld());
        }
        return false;
    }
}
