package de.craften.plugins.plotplus.persistence.entities;

/**
 * A connection between two plots.
 */
public class PlotConnection {
    private PlotId a;
    private PlotId b;

    public PlotConnection(PlotId a, PlotId b) {
        this.a = a;
        this.b = b;
    }

    public PlotId getA() {
        return a;
    }

    public PlotId getB() {
        return b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlotConnection that = (PlotConnection) o;

        return a.equals(that.a) && b.equals(that.b);

    }

    @Override
    public int hashCode() {
        return 31 * a.hashCode() + b.hashCode();
    }
}
