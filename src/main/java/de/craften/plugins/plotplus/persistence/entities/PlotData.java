package de.craften.plugins.plotplus.persistence.entities;

import de.craften.plugins.plotplus.SimplePlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * The information about a plot that is saved.
 */
public class PlotData {
    private final PlotId id;
    private final SimplePlayer owner;
    private final boolean isProtected;
    private final String name;

    private List<SimplePlayer> helpers;
    private List<SimplePlayer> blockedPlayers;

    private final Boolean isWelcomeMessageEnabled;
    private final Boolean isProjectileProtectionEnabled;
    private final Boolean isHitProtectionEnabled;

    public PlotData(int idX, int idZ, String world, SimplePlayer owner, boolean isProtected, String name,
                    Boolean isWelcomeMessageEnabled, Boolean isProjectileProtectionEnabled,
                    Boolean isHitProtectionEnabled) {
        this.id = new PlotId(idX, idZ, world);
        this.owner = owner;
        this.helpers = new ArrayList<>();
        this.blockedPlayers = new ArrayList<>();
        this.isProtected = isProtected;
        this.name = name;
        this.isWelcomeMessageEnabled = isWelcomeMessageEnabled;
        this.isProjectileProtectionEnabled = isProjectileProtectionEnabled;
        this.isHitProtectionEnabled = isHitProtectionEnabled;
    }

    public PlotData(int idX, int idZ, String world, SimplePlayer owner) {
        this(idX, idZ, world, owner, false, null, null, null, null);
    }

    public PlotId getId() {
        return id;
    }

    public SimplePlayer getOwner() {
        return owner;
    }

    public List<SimplePlayer> getHelpers() {
        return helpers;
    }

    public List<SimplePlayer> getBlockedPlayers() {
        return blockedPlayers;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PlotData) {
            PlotData other = (PlotData) obj;
            return other.id.equals(id) && other.owner.equals(owner)
                    && other.helpers.equals(helpers) && other.blockedPlayers.equals(blockedPlayers)
                    && other.isProtected == isProtected
                    && ((other.name == null && name == null) || (other.name != null && other.name.equals(name)));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hc = id.hashCode() + 11 * owner.hashCode() + 17 * helpers.hashCode()
                + 23 * blockedPlayers.hashCode();
        if (name != null)
            hc += 29 * name.hashCode();
        return hc;
    }

    public Boolean getIsWelcomeMessageEnabled() {
        return isWelcomeMessageEnabled;
    }

    public Boolean getIsProjectileProtectionEnabled() {
        return isProjectileProtectionEnabled;
    }

    public Boolean getIsHitProtectionEnabled() {
        return isHitProtectionEnabled;
    }
}
