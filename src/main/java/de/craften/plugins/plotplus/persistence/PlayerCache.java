package de.craften.plugins.plotplus.persistence;


import de.craften.plugins.plotplus.SimplePlayer;
import org.bukkit.OfflinePlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerCache {
    private Map<UUID, MutableSimplePlayer> players = new HashMap<>();
    private PlayerPersistence persistence;

    public PlayerCache(PlayerPersistence persistence) {
        this.persistence = persistence;
    }

    public SimplePlayer getCached(UUID uuid, String displayName) {
        MutableSimplePlayer sp = players.get(uuid);
        if (sp == null) {
            sp = new MutableSimplePlayer(uuid, displayName);
            players.put(sp.getUniqueId(), sp);
            try {
                persistence.savePlayer(sp);
            } catch (PersistenceException ignore) {
            }
        } else if (displayName != null && !displayName.equals(sp.getDisplayName())) {
            sp.setDisplayName(displayName);
            try {
                persistence.savePlayer(sp);
            } catch (PersistenceException ignore) {
            }
        }
        return sp;
    }

    private MutableSimplePlayer putPlayer(UUID uuid, String displayName) {
        MutableSimplePlayer player = new MutableSimplePlayer(uuid, displayName);
        players.put(uuid, player);
        return player;
    }

    public SimplePlayer getCached(SimplePlayer player) {
        if (player instanceof MutableSimplePlayer)
            return player;

        return getCached(player.getUniqueId(), player.getDisplayName());
    }

    public SimplePlayer getCachedWithoutUpdate(SimplePlayer player) {
        if (player instanceof MutableSimplePlayer)
            return player;

        return putPlayer(player.getUniqueId(), player.getDisplayName());
    }

    public SimplePlayer getCached(OfflinePlayer player) {
        return getCached(player.getUniqueId(), player.getName());
    }

    /**
     * Updates the display name of the given player, if necessary.
     *
     * @param player Player to update the display name of
     * @return True if the nickname was changed and the player was updated, false if not
     * @throws PersistenceException If saving the new display name failed
     */
    public boolean updatePlayer(OfflinePlayer player) throws PersistenceException {
        MutableSimplePlayer p = (MutableSimplePlayer) getCached(player);
        if (!p.getDisplayName().equals(player.getName())) {
            p.setDisplayName(player.getName());
            persistence.savePlayer(p);
            return true;
        }
        return false;
    }

    private static class MutableSimplePlayer extends SimplePlayer {
        /**
         * Creates a new player instance.
         *
         * @param uuid        UUID of the player
         * @param displayName Display name of the player (i.e. the nickname)
         */
        public MutableSimplePlayer(UUID uuid, String displayName) {
            super(uuid, displayName);
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }
}
