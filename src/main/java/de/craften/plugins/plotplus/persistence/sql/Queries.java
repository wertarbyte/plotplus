package de.craften.plugins.plotplus.persistence.sql;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * Manages sql queries for MySQL and SQLite persistence.
 */
class Queries {
    private Properties queries;
    private String tablePrefix;

    /**
     * Creates a new query manager.
     *
     * @param propertiesFile Filename of the properties file to read the queries from
     * @throws IOException If reading the query resource file failed
     */
    public Queries(String propertiesFile) throws IOException {
        this(propertiesFile, "");
    }

    /**
     * Creates a new query manager.
     *
     * @param propertiesFile Filename of the properties file to read the queries from
     * @param tablePrefix    Table prefix
     * @throws IOException If reading the query resource file failed
     */
    public Queries(String propertiesFile, String tablePrefix) throws IOException {
        this.queries = readProperties(propertiesFile);
        this.tablePrefix = tablePrefix;
    }

    /**
     * Gets the query with the given name. {0} is automatically replaced with the table prefix.
     *
     * @param name Name of the query to get
     * @return The query with the given name
     */
    public String get(String name) {
        return MessageFormat.format(queries.getProperty(name), tablePrefix);
    }

    /**
     * Reads the properties file with the given name.
     *
     * @param filename Name of the properties file
     * @return Properties read from the file with the given name
     * @throws IOException If reading the properties file failed
     */
    private Properties readProperties(String filename) throws IOException {
        Properties properties = new Properties();
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename);
        properties.load(is);
        return properties;
    }
}
