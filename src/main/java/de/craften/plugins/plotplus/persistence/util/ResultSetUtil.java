package de.craften.plugins.plotplus.persistence.util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Contains utility methods for reading {@link java.sql.ResultSet}s.
 */
public class ResultSetUtil {
    private ResultSetUtil() {
    }

    public static Boolean getBoolean(ResultSet rs, String columnName) throws SQLException {
        return rs.getObject(columnName) != null ? rs.getBoolean(columnName) : null;
    }
}
