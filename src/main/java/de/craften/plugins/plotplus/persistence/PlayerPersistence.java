package de.craften.plugins.plotplus.persistence;

import de.craften.plugins.plotplus.SimplePlayer;

/**
 * Persistence that saves player-uuid mappings.
 */
public interface PlayerPersistence {
    /**
     * Saves the given player or updates it if it already exists.
     *
     * @param player The player to save
     * @throws PersistenceException If saving the player fails
     */
    public void savePlayer(SimplePlayer player) throws PersistenceException;
}
