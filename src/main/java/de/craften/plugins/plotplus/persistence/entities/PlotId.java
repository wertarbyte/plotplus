package de.craften.plugins.plotplus.persistence.entities;

/**
 * A plot's position.
 */
public class PlotId {
    private int idX;
    private int idZ;
    private String world;

    public PlotId(int idX, int idZ, String world) {
        this.idX = idX;
        this.idZ = idZ;
        this.world = world;
    }

    public int getIdX() {
        return idX;
    }

    public int getIdZ() {
        return idZ;
    }

    public String getWorld() {
        return world;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PlotId) {
            PlotId other = (PlotId) obj;
            return other.getIdX() == getIdX() && other.getIdZ() == getIdZ() && other.getWorld().equals(getWorld());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 7 * idX + 13 * idZ + getWorld().hashCode();
    }

    @Override
    public String toString() {
        return world + ": (" + idX + ";" + idZ + ")";
    }
}
