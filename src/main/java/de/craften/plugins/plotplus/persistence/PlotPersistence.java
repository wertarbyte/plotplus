package de.craften.plugins.plotplus.persistence;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.entities.PlotConnection;
import de.craften.plugins.plotplus.persistence.entities.PlotData;
import de.craften.plugins.plotplus.persistence.entities.PlotId;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * A persistence for plots and related data as helpers and blocked players.
 */
public interface PlotPersistence extends PlayerPersistence {
    /**
     * Initializes the database.
     *
     * @throws PersistenceException If initializing the database fails
     */
    void setup() throws PersistenceException;

    /**
     * Gets all plots.
     *
     * @return All plots
     * @throws PersistenceException If loading the plots fails
     */
    List<PlotData> getPlots() throws PersistenceException;

    /**
     * Creates the given plot.
     *
     * @param plot Plot to create
     * @throws PersistenceException If creating the plot fails
     */
    void createPlot(PlotData plot) throws PersistenceException;

    void createPlots(List<PlotData> plots) throws PersistenceException;

    /**
     * Removes the plot with the given ID.
     *
     * @param plot ID of the plot to remove
     * @throws PersistenceException If removing the plot fails
     */
    void removePlot(PlotId plot) throws PersistenceException;

    /**
     * Adds a helper to the plot with the given ID.
     *
     * @param plot   ID of the plot to add the helper to
     * @param helper UUID of the helper
     * @throws PersistenceException If adding the helper fails
     */
    void addHelper(PlotId plot, SimplePlayer helper) throws PersistenceException;

    /**
     * Removes a helper from the plot with the given ID.
     *
     * @param plot   ID of the plot to remove the helper from
     * @param helper UUID of the helper
     * @throws PersistenceException If removing the helper fails
     */
    void removeHelper(PlotId plot, UUID helper) throws PersistenceException;

    /**
     * Adds a player to the list of blocked players of the plot with the given ID.
     *
     * @param plot    ID of the plot to add the blocked player to
     * @param blocked UUID of the player to block
     * @throws PersistenceException If saving the blocked player fails
     */
    void addBlockedPlayer(PlotId plot, SimplePlayer blocked) throws PersistenceException;

    /**
     * Removes a player from the list of blocked players of the plot with the given ID.
     *
     * @param plot    ID of the plot to remove the blocked player from
     * @param blocked UUID of the player to unblock
     * @throws PersistenceException If removing the unblocked player fails
     */
    void removeBlockedPlayer(PlotId plot, UUID blocked) throws PersistenceException;

    /**
     * Updates the protection of the plot with the given ID.
     *
     * @param plot        ID of the plot to update
     * @param isProtected Whether the plot should be protected or not
     * @throws PersistenceException If saving the protection fails
     */
    void setProtected(PlotId plot, boolean isProtected) throws PersistenceException;

    /**
     * Updates the owner of the plot with the given ID.
     *
     * @param plot  ID of the plot to update
     * @param owner UUID of the new owner
     * @throws PersistenceException If saving the new owner fails
     */
    void setOwner(PlotId plot, SimplePlayer owner) throws PersistenceException;

    /**
     * Sets the name of the plot with the given ID.
     *
     * @param plot ID of the plot to update
     * @param name New name of the plot
     * @throws PersistenceException If saving the new name fails
     */
    void setName(PlotId plot, String name) throws PersistenceException;

    /**
     * Enables or disables the welcome message on the plot with the given ID.
     *
     * @param plot      ID of the plot to update
     * @param isEnabled true to enable the welcome message, false to disable it
     * @throws PersistenceException If saving this option fails
     */
    void setWelcomeMessageEnabled(PlotId plot, boolean isEnabled) throws PersistenceException;

    /**
     * Enables or disables the projectile protection on the plot with the given ID.
     *
     * @param plot      ID of the plot to update
     * @param isEnabled true to enable the projectile protection, false to disable it
     * @throws PersistenceException If saving this option fails
     */
    void setProjectileProtectionEnabled(PlotId plot, boolean isEnabled) throws PersistenceException;


    /**
     * Enables or disables the hit protection on the plot with the given ID.
     *
     * @param plot      ID of the plot to update
     * @param isEnabled true to enable the hit protection, false to disable it
     * @throws PersistenceException If saving this option fails
     */
    void setHitProtectionEnabled(PlotId plot, boolean isEnabled) throws PersistenceException;

    /**
     * Gets all plot connections.
     *
     * @return all plot connections
     * @throws PersistenceException if getting the plot connections fails
     */
    Collection<PlotConnection> getPlotConnections() throws PersistenceException;

    /**
     * Connects plot a to plot b, but only in that direction.
     *
     * @param a a plot
     * @param b another plot
     * @throws PersistenceException if saving the plot connection fails
     */
    void createPlotConnection(PlotId a, PlotId b) throws PersistenceException;

    /**
     * Disconnects plot a from plot b, but only in that direction.
     *
     * @param a a plot
     * @param b another plot
     * @throws PersistenceException if removing the plot connection fails
     */
    void deletePlotConnections(PlotId a, PlotId b) throws PersistenceException;

    /**
     * Disconnects all connected plots from the given plot.
     *
     * @param plot a plot
     * @throws PersistenceException if removing the plot connections fails
     */
    void deletePlotConnections(PlotId plot) throws PersistenceException;
}
