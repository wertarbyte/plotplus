package de.craften.plugins.plotplus.persistence;

/**
 * An exception that indicates that either loading or saving something from/to the persistence failed.
 */
public class PersistenceException extends Exception {
    public PersistenceException(String msg, Exception inner) {
        super(msg, inner);
    }

    public PersistenceException(String msg) {
        super(msg);
    }
}
