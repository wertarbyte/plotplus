package de.craften.plugins.plotplus.persistence.sql;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.PlotPersistence;
import de.craften.plugins.plotplus.persistence.entities.PlotConnection;
import de.craften.plugins.plotplus.persistence.entities.PlotData;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.persistence.util.ResultSetUtil;
import de.craften.plugins.plotplus.persistence.util.SimpleMySqlPool;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class MySqlPersistence implements PlotPersistence {
    private static final int SCHEME_VERSION = 3;

    private Queries queries;
    private SimpleMySqlPool pool;

    public MySqlPersistence(String host, int port, String database, String user, String password, String tablePrefix) throws PersistenceException {
        try {
            queries = new Queries("queries-mysql.properties", tablePrefix);
        } catch (IOException e) {
            throw new PersistenceException("Could not read queries", e);
        }

        try {
            pool = new SimpleMySqlPool(host, port, database, user, password);
        } catch (ClassNotFoundException e) {
            throw new PersistenceException("Could not find mysql JDBC driver", e);
        }
    }

    private Connection getConnection() throws SQLException {
        return pool.getConnection();
    }

    @Override
    public void setup() throws PersistenceException {
        if (hasTables()) {
            upgradeDatabase();
        } else {
            try (Statement stmt = getConnection().createStatement()) {
                stmt.execute(queries.get("createPlotsTable"));
                stmt.execute(queries.get("createHelpersTable"));
                stmt.execute(queries.get("createBlockedTable"));
                stmt.execute(queries.get("createPlayersTable"));
                stmt.execute(queries.get("createVersionTable"));
                stmt.execute(queries.get("upgrade-0to1-SetVersion"));
                upgradeDatabase();
            } catch (SQLException e) {
                throw new PersistenceException("Could not initialize database", e);
            }
        }
    }

    @Override
    public List<PlotData> getPlots() throws PersistenceException {
        try {
            Connection connection = getConnection();

            try (PreparedStatement stmt = connection.prepareStatement(queries.get("getPlots"))) {
                List<PlotData> plots = new ArrayList<>();
                ResultSet result = stmt.executeQuery();
                PlotData currentPlot = null;
                while (result.next()) {
                    if (currentPlot == null) {
                        SimplePlayer owner = new SimplePlayer(
                                UUID.fromString(result.getString("owner")),
                                result.getString("ownerName"));

                        currentPlot = new PlotData(
                                result.getInt("idX"), result.getInt("idZ"), result.getString("world"),
                                owner, result.getBoolean("protected"), result.getString("name"),
                                ResultSetUtil.getBoolean(result, "showWelcomeMessage"),
                                ResultSetUtil.getBoolean(result, "projectileProtection"),
                                ResultSetUtil.getBoolean(result, "hitProtection"));
                    } else if (!(currentPlot.getId().getWorld().equals(result.getString("world"))
                            && currentPlot.getId().getIdX() == result.getInt("idX")
                            && currentPlot.getId().getIdZ() == result.getInt("idZ"))) {
                        plots.add(currentPlot);

                        SimplePlayer owner = new SimplePlayer(
                                UUID.fromString(result.getString("owner")),
                                result.getString("ownerName"));

                        currentPlot = new PlotData(result.getInt("idX"), result.getInt("idZ"), result.getString("world"),
                                owner, result.getBoolean("protected"), result.getString("name"),
                                ResultSetUtil.getBoolean(result, "showWelcomeMessage"),
                                ResultSetUtil.getBoolean(result, "projectileProtection"),
                                ResultSetUtil.getBoolean(result, "hitProtection"));
                    }
                    if (result.getObject("helper") != null) {
                        SimplePlayer helper = new SimplePlayer(
                                UUID.fromString(result.getString("helper")),
                                result.getString("helperName"));

                        currentPlot.getHelpers().add(helper);
                    }
                    if (result.getObject("blocked") != null) {
                        SimplePlayer blocked = new SimplePlayer(
                                UUID.fromString(result.getString("blocked")),
                                result.getString("blockedName"));

                        currentPlot.getBlockedPlayers().add(blocked);
                    }
                }
                if (currentPlot != null)
                    plots.add(currentPlot);
                return plots;
            }
        } catch (SQLException e) {
            throw new PersistenceException("Loading plots failed", e);
        }
    }

    @Override
    public void createPlot(PlotData plot) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("insertPlot"))) {
                stmt.setInt(1, plot.getId().getIdX());
                stmt.setInt(2, plot.getId().getIdZ());
                stmt.setString(3, plot.getId().getWorld());
                stmt.setString(4, plot.getOwner().getUniqueId().toString());
                stmt.setBoolean(5, plot.isProtected());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Creating plot failed", e);
        }
    }

    @Override
    public void createPlots(List<PlotData> plots) throws PersistenceException {
        try {
            Connection connection = getConnection();
            connection.setAutoCommit(false);
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("insertPlot"))) {
                int i = 0;
                for (PlotData plot : plots) {
                    i++;
                    stmt.setInt(1, plot.getId().getIdX());
                    stmt.setInt(2, plot.getId().getIdZ());
                    stmt.setString(3, plot.getId().getWorld());
                    stmt.setString(4, plot.getOwner().getUniqueId().toString());
                    stmt.setBoolean(5, plot.isProtected());
                    stmt.addBatch();
                    if ((i + 1) % 100 == 0) {
                        stmt.executeBatch();
                    }
                }
                stmt.executeBatch();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new PersistenceException("Creating plot failed", e);
        }
    }

    @Override
    public void removePlot(PlotId plot) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("deletePlotHelpers"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.execute();
            }
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("deletePlotBlocked"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.execute();
            }
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("deletePlot"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Deleting plot failed", e);
        }
    }

    @Override
    public void addHelper(PlotId plot, SimplePlayer helper) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("insertHelper"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.setString(4, helper.getUniqueId().toString());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Adding helper failed", e);
        }
    }

    @Override
    public void removeHelper(PlotId plot, UUID helper) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("deleteHelper"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.setString(4, helper.toString());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Deleting helper failed", e);
        }
    }

    @Override
    public void addBlockedPlayer(PlotId plot, SimplePlayer blocked) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("insertBlocked"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.setString(4, blocked.getUniqueId().toString());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Adding blocked player failed", e);
        }
    }

    @Override
    public void removeBlockedPlayer(PlotId plot, UUID blocked) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("deleteBlocked"))) {
                stmt.setInt(1, plot.getIdX());
                stmt.setInt(2, plot.getIdZ());
                stmt.setString(3, plot.getWorld());
                stmt.setString(4, blocked.toString());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Deleting blocked player failed", e);
        }
    }

    @Override
    public void setProtected(PlotId plot, boolean isProtected) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("updatePlotProtection"))) {
                stmt.setBoolean(1, isProtected);
                stmt.setInt(2, plot.getIdX());
                stmt.setInt(3, plot.getIdZ());
                stmt.setString(4, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Changing protection failed", e);
        }
    }

    @Override
    public void setOwner(PlotId plot, SimplePlayer owner) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("updatePlotOwner"))) {
                stmt.setString(1, owner.getUniqueId().toString());
                stmt.setInt(2, plot.getIdX());
                stmt.setInt(3, plot.getIdZ());
                stmt.setString(4, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Changing owner failed", e);
        }
    }

    @Override
    public void setName(PlotId plot, String name) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("updatePlotName"))) {
                stmt.setString(1, name);
                stmt.setInt(2, plot.getIdX());
                stmt.setInt(3, plot.getIdZ());
                stmt.setString(4, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Changing name failed", e);
        }
    }

    @Override
    public void setWelcomeMessageEnabled(PlotId plot, boolean isEnabled) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("updatePlotWelcomeMessageEnabled"))) {
                stmt.setBoolean(1, isEnabled);
                stmt.setInt(2, plot.getIdX());
                stmt.setInt(3, plot.getIdZ());
                stmt.setString(4, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Changing option failed", e);
        }
    }

    @Override
    public void setProjectileProtectionEnabled(PlotId plot, boolean isEnabled) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("updatePlotProjectileProtectionEnabled"))) {
                stmt.setBoolean(1, isEnabled);
                stmt.setInt(2, plot.getIdX());
                stmt.setInt(3, plot.getIdZ());
                stmt.setString(4, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Changing option failed", e);
        }
    }

    @Override
    public void setHitProtectionEnabled(PlotId plot, boolean isEnabled) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("updatePlotHitProtectionEnabled"))) {
                stmt.setBoolean(1, isEnabled);
                stmt.setInt(2, plot.getIdX());
                stmt.setInt(3, plot.getIdZ());
                stmt.setString(4, plot.getWorld());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Changing option failed", e);
        }
    }

    @Override
    public Collection<PlotConnection> getPlotConnections() throws PersistenceException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(queries.get("getPlotConnections"));
             ResultSet result = stmt.executeQuery()) {
            List<PlotConnection> connections = new ArrayList<>();
            while (result.next()) {
                connections.add(new PlotConnection(
                        new PlotId(result.getInt("a_idX"), result.getInt("a_idZ"), result.getString("a_world")),
                        new PlotId(result.getInt("b_idX"), result.getInt("b_idZ"), result.getString("b_world"))
                ));
            }
            return connections;
        } catch (SQLException e) {
            throw new PersistenceException("Getting the plot connections failed", e);
        }
    }

    @Override
    public void createPlotConnection(PlotId a, PlotId b) throws PersistenceException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(queries.get("createPlotConnection"))) {
            stmt.setInt(1, a.getIdX());
            stmt.setInt(2, a.getIdZ());
            stmt.setString(3, a.getWorld());
            stmt.setInt(4, b.getIdX());
            stmt.setInt(5, b.getIdZ());
            stmt.setString(6, b.getWorld());
            stmt.execute();
        } catch (SQLException e) {
            throw new PersistenceException("Connecting the plots failed", e);
        }
    }

    @Override
    public void deletePlotConnections(PlotId a, PlotId b) throws PersistenceException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(queries.get("deletePlotConnection"))) {
            stmt.setInt(1, a.getIdX());
            stmt.setInt(2, a.getIdZ());
            stmt.setString(3, a.getWorld());
            stmt.setInt(4, b.getIdX());
            stmt.setInt(5, b.getIdZ());
            stmt.setString(6, b.getWorld());
            stmt.execute();
        } catch (SQLException e) {
            throw new PersistenceException("Disconnecting the plots failed", e);
        }
    }

    @Override
    public void deletePlotConnections(PlotId plot) throws PersistenceException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(queries.get("deleteAllPlotConnections"))) {
            stmt.setInt(1, plot.getIdX());
            stmt.setInt(2, plot.getIdZ());
            stmt.setString(3, plot.getWorld());
            stmt.setInt(4, plot.getIdX());
            stmt.setInt(5, plot.getIdZ());
            stmt.setString(6, plot.getWorld());
            stmt.execute();
        } catch (SQLException e) {
            throw new PersistenceException("Disconnecting the plot failed", e);
        }
    }

    @Override
    public void savePlayer(SimplePlayer player) throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("insertOrUpdatePlayer"))) {
                stmt.setString(1, player.getUniqueId().toString());
                stmt.setString(2, player.getDisplayName());
                stmt.setString(3, player.getDisplayName());
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new PersistenceException("Saving player failed", e);
        }
    }

    private void upgradeDatabase() throws PersistenceException {
        if (hasSchemeVersion()) {
            upgradeDatabase(getSchemeVersion());
        } else {
            upgradeDatabase(0);
        }
    }

    private void upgradeDatabase(int currentSchemeVersion) throws PersistenceException {
        if (currentSchemeVersion == SCHEME_VERSION)
            return;

        if (currentSchemeVersion == 0) {
            //Upgrade von Version 0 to 1
            try (Statement stmt = getConnection().createStatement()) {
                stmt.execute(queries.get("createVersionTable"));
                stmt.execute(queries.get("upgrade-0to1-AlterPlots1"));
                stmt.execute(queries.get("upgrade-0to1-AlterPlots2"));
                stmt.execute(queries.get("upgrade-0to1-AlterPlots3"));
                stmt.execute(queries.get("upgrade-0to1-SetVersion"));
            } catch (SQLException e) {
                throw new PersistenceException("Could not upgrade database", e);
            }
            currentSchemeVersion = 1;
        }

        if (currentSchemeVersion == 1) {
            //Upgrade von Version 1 to 2
            try (Statement stmt = getConnection().createStatement()) {
                stmt.execute(queries.get("upgrade-1to2-AlterPlayers"));
                stmt.execute(queries.get("upgrade-1to2-UpdatePlayers"));
                stmt.execute(queries.get("upgrade-1to2-CleanupPlayers"));
                stmt.execute(queries.get("upgrade-1to2-AlterHelpers"));
                stmt.execute(queries.get("upgrade-1to2-UpdateHelpers"));
                stmt.execute(queries.get("upgrade-1to2-CleanupHelpers"));
                stmt.execute(queries.get("upgrade-1to2-AlterBlocked"));
                stmt.execute(queries.get("upgrade-1to2-UpdateBlocked"));
                stmt.execute(queries.get("upgrade-1to2-CleanupBlocked"));
                stmt.execute(queries.get("upgrade-1to2-AlterPlots"));
                stmt.execute(queries.get("upgrade-1to2-UpdatePlots"));
                stmt.execute(queries.get("upgrade-1to2-CleanupPlots"));
                stmt.execute(queries.get("upgrade-1to2-SetVersion"));
            } catch (SQLException e) {
                throw new PersistenceException("Could not upgrade database", e);
            }
            currentSchemeVersion = 2;
        }

        if (currentSchemeVersion == 2) {
            //Upgrade von Version 2 to 3
            try (Statement stmt = getConnection().createStatement()) {
                stmt.execute(queries.get("upgrade-2to3-CreateLinksTable"));
                stmt.execute(queries.get("upgrade-2to3-SetVersion"));
            } catch (SQLException e) {
                throw new PersistenceException("Could not upgrade database", e);
            }
            currentSchemeVersion = 3;
        }
    }

    private boolean hasSchemeVersion() throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("upgradeHasVersionTable"))) {
                try (ResultSet result = stmt.executeQuery()) {
                    return result.next();
                }
            }
        } catch (SQLException e) {
            throw new PersistenceException("Could not get scheme version", e);
        }
    }

    private int getSchemeVersion() throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("upgradeGetVersion"))) {
                try (ResultSet result = stmt.executeQuery()) {
                    result.next();
                    return result.getInt("version");
                }
            }
        } catch (SQLException e) {
            throw new PersistenceException("Could not get scheme version", e);
        }
    }

    private boolean hasTables() throws PersistenceException {
        try {
            Connection connection = getConnection();
            try (PreparedStatement stmt = connection.prepareStatement(queries.get("setupHasPlotsTable"))) {
                try (ResultSet result = stmt.executeQuery()) {
                    return result.next();
                }
            }
        } catch (SQLException e) {
            throw new PersistenceException("Could not get scheme version", e);
        }
    }
}
