package de.craften.plugins.plotplus.persistence.util;

import com.evilmidget38.UUIDFetcher;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.PlotPersistence;
import de.craften.plugins.plotplus.persistence.entities.PlotData;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Logger;

public class PlotmeDatabaseImporter {
    private PlotPersistence persistence;
    private SimpleMySqlPool plotme;

    public PlotmeDatabaseImporter(PlotPersistence persistence, String host, int port, String database, String username, String password) throws ClassNotFoundException {
        this.persistence = persistence;
        this.plotme = new SimpleMySqlPool(host, port, database, username, password);
    }

    public void convert(Logger log) throws SQLException {
        log.info("Importing players...");
        importPlayers(log);
        log.info("Importing plots...");
        importPlots(log);
        log.info("Importing helpers...");
        importHelpers(log);
        log.info("Importing blocked players...");
        importBlockedPlayers(log);
        log.info("Done");
    }

    private void importPlayers(Logger log) throws SQLException {
        try (Connection c = plotme.getConnection();
             Statement stmt = c.createStatement();) {
            int count = 0;
            Set<String> nicknames = new HashSet<>();

            ResultSet result = stmt.executeQuery("SELECT owner FROM plotmePlots");
            while (result.next()) {
                nicknames.add(result.getString("owner"));
            }
            result.close();

            result = stmt.executeQuery("SELECT player FROM plotmeAllowed");
            while (result.next()) {
                nicknames.add(result.getString("player"));
            }
            result.close();

            result = stmt.executeQuery("SELECT player FROM plotmeDenied");
            while (result.next()) {
                nicknames.add(result.getString("player"));
            }
            result.close();

            List<String> nicknameList = new ArrayList<>(nicknames);
            log.info(nicknameList.size() + " players found");
            while (nicknameList.size() >= 100) {
                try {
                    List<String> sublist = nicknameList.subList(0, 100);
                    Map<String, UUID> playerData = new UUIDFetcher(sublist).call();
                    for (Map.Entry<String, UUID> entry : playerData.entrySet()) {
                        SimplePlayer p = new SimplePlayer(entry.getValue(), entry.getKey());
                        playerCache.put(p.getDisplayName().toLowerCase(), p);
                        persistence.savePlayer(p);
                    }
                    sublist.clear();
                    count += 100;
                    log.info(count + " players imported");
                    log.info("Sleeping for 100s (Mojang API restriction)");
                    Thread.sleep(100 * 1000);
                } catch (Exception e) {
                    log.severe("Could not get UUIDs!");
                    e.printStackTrace();
                }
            }

            try {
                Map<String, UUID> playerData = new UUIDFetcher(nicknameList).call();
                for (Map.Entry<String, UUID> entry : playerData.entrySet()) {
                    SimplePlayer p = new SimplePlayer(entry.getValue(), entry.getKey());
                    playerCache.put(p.getDisplayName().toLowerCase(), p);
                    persistence.savePlayer(p);
                }
                count += nicknameList.size();
                log.info(count + " players imported");
            } catch (Exception e) {
                log.severe("Could not get UUIDs!");
            }
        }
    }

    private void importPlots(Logger log) throws SQLException {
        try (Connection c = plotme.getConnection();
             Statement stmt = c.createStatement();
             ResultSet result = stmt.executeQuery("SELECT * FROM plotmePlots")) {
            int count = 0;
            while (result.next()) {
                try {
                    persistence.createPlot(new PlotData(
                            result.getInt("idX"), result.getInt("idZ"),
                            result.getString("world"),
                            toSimplePlayer(result.getString("owner")),
                            result.getBoolean("protected"),
                            null, null, null, null
                    ));
                    count++;
                    if (count % 25 == 0)
                        log.info(count + " plots imported");
                } catch (PersistenceException e) {
                    log.severe("Could not create a plot!");
                }
            }
            log.info(count + " plots imported");
        }
    }

    private void importHelpers(Logger log) throws SQLException {
        try (Connection c = plotme.getConnection();
             Statement stmt = c.createStatement();
             ResultSet result = stmt.executeQuery("SELECT * FROM plotmeAllowed")) {
            int count = 0;
            while (result.next()) {
                try {
                    persistence.addHelper(
                            new PlotId(result.getInt("idX"), result.getInt("idZ"), result.getString("world")),
                            toSimplePlayer(result.getString("player"))
                    );
                    count++;
                    if (count % 25 == 0)
                        log.info(count + " helpers imported");
                } catch (PersistenceException e) {
                    log.severe("Could not add a helper to a plot!");
                }
            }
            log.info(count + " helpers imported");
        }
    }

    private void importBlockedPlayers(Logger log) throws SQLException {
        try (Connection c = plotme.getConnection();
             Statement stmt = c.createStatement();
             ResultSet result = stmt.executeQuery("SELECT * FROM plotmeDenied")) {
            int count = 0;
            while (result.next()) {
                try {
                    persistence.addBlockedPlayer(
                            new PlotId(result.getInt("idX"), result.getInt("idZ"), result.getString("world")),
                            toSimplePlayer(result.getString("player"))
                    );
                    count++;
                    if (count % 25 == 0)
                        log.info(count + " blocked players imported");
                } catch (PersistenceException e) {
                    log.severe("Could not add a blocked player to a plot!");
                }
            }
            log.info(count + " blocked players imported");
        }
    }

    private Map<String, SimplePlayer> playerCache = new HashMap<>();

    private SimplePlayer toSimplePlayer(String nickname) throws PersistenceException {
        SimplePlayer cached = playerCache.get(nickname.toLowerCase());
        if (cached == null) {
            OfflinePlayer p = Bukkit.getOfflinePlayer(nickname);
            return new SimplePlayer(p.getUniqueId(), p.getName());
        }
        return cached;
    }
}
