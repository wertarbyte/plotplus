package de.craften.plugins.plotplus.persistence.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SimpleMySqlPool {
    private final String user;
    private final String database;
    private final String password;
    private final int port;
    private final String hostname;

    private Connection connection;

    /**
     * Creates a new MySQL instance
     *
     * @param hostname Name of the host
     * @param port     Port number
     * @param database Database name
     * @param username Username
     * @param password Password
     * @throws ClassNotFoundException
     */
    public SimpleMySqlPool(String hostname, int port, String database,
                           String username, String password) throws ClassNotFoundException {
        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.user = username;
        this.password = password;
        this.connection = null;

        Class.forName("com.mysql.jdbc.Driver");
    }

    private void openConnection() throws SQLException {
        Properties p = new Properties();
        p.put("user", this.user);
        p.put("password", this.password);
        connection = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database, p);
    }

    public Connection getConnection() throws SQLException {
        if (connection == null || !connection.isValid(10))
            openConnection();
        return connection;
    }
}
