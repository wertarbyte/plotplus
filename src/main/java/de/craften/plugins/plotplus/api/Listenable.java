package de.craften.plugins.plotplus.api;

public interface Listenable<E> {
    public void addListener(E listener);

    public void removeListener(E listener);
}
