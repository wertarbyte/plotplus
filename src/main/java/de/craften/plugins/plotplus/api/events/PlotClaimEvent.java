package de.craften.plugins.plotplus.api.events;

import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 * Event that is fired when claiming a plot.
 */
public class PlotClaimEvent {
    private double price;
    private Player claimer;
    private OfflinePlayer owner;
    private PlotId plot;
    private PlotWorld world;

    public PlotClaimEvent(double price, Player claimer, OfflinePlayer owner, PlotWorld world, PlotId plot) {
        this(price, claimer, owner, world);
        this.plot = plot;
    }

    public PlotClaimEvent(double price, Player claimer, OfflinePlayer owner, PlotWorld world) {
        this.price = price;
        this.claimer = claimer;
        this.owner = owner;
        this.world = world;
    }

    public double getPrice() {
        return Math.max(price, 0);
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Player getClaimer() {
        return claimer;
    }

    public OfflinePlayer getOwner() {
        return owner;
    }

    public PlotId getPlot() {
        return plot;
    }

    public PlotWorld getWorld() {
        return world;
    }
}
