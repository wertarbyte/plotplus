package de.craften.plugins.plotplus.api;

import de.craften.plugins.plotplus.PlotPlus;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;

import java.util.logging.Level;

/**
 * An add-on for PlotPlus.
 */
public abstract class Addon {
    private String name;
    private String[] dependencies;
    private PlotPlus plugin;
    private boolean isEnabled = false;

    /**
     * Creates a new add-on instance.
     *
     * @param name         Unique name of this add-on
     * @param dependencies Names of plugins this add-on depends on
     */
    public Addon(String name, String... dependencies) {
        this.name = name;
        this.dependencies = dependencies;
    }

    /**
     * Gets the name of this add-on.
     *
     * @return The name of this add-on
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the PlotPlus instance of this add-on.
     *
     * @return The PlotPlus instance of this add-on
     */
    protected PlotPlus getPlugin() {
        return plugin;
    }

    /**
     * Checks if this add-on is enabled.
     *
     * @return True if this add-on is enabled, false if not
     */
    public boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Tries to enable this add-on. If all dependencies are enabled, it will be enabled immediately. If not, it will
     * be enabled as soon as all dependencies are enabled.
     *
     * @param plugin PlotPlus instance to use with this add-on
     */
    public final void enable(PlotPlus plugin) {
        if (isEnabled()) return;

        this.plugin = plugin;
        tryEnable();

        plugin.getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onPluginEnabled(PluginEnableEvent event) {
                if (!isEnabled()) {
                    for (String dependency : dependencies) {
                        if (dependency.equalsIgnoreCase(event.getPlugin().getName()))
                            tryEnable();
                    }
                }
            }
        }, plugin);
    }

    /**
     * Enables this add-on if all dependencies are enabled.
     *
     * @return True if this add-on was enabled, false if not.
     */
    private boolean tryEnable() {
        for (String dependency : dependencies) {
            Plugin dep = plugin.getServer().getPluginManager().getPlugin(dependency);
            if (dep == null || !dep.isEnabled())
                return false;
        }

        isEnabled = true;
        try {
            onEnabled();
            getPlugin().getLogger().info("Add-on '" + getName() + "' enabled");
            return true;
        } catch (Exception e) {
            getPlugin().getLogger().log(Level.WARNING, "Could not enable add-on '" + getName() + "'", e);
            isEnabled = false;
            return false;
        }
    }

    /**
     * Disables this add-on.
     */
    public final void disable() {
        isEnabled = false;
        onDisabling();
        getPlugin().getLogger().info("Add-On '" + getName() + "' disabled");
        this.plugin = null;
    }

    /**
     * Gets the API.
     *
     * @return the API
     */
    protected final PlotPlusApi getApi() {
        return PlotPlus.getApi();
    }

    /**
     * Gets called after the add-on was enabled.
     */
    protected abstract void onEnabled();

    /**
     * Gets called before the add-on is disabled.
     */
    protected abstract void onDisabling();
}
