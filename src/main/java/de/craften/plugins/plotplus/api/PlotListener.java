package de.craften.plugins.plotplus.api;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;

import java.util.Collection;

/**
 * A listener for plot changes.
 */
public interface PlotListener {
    /**
     * Gets called after a world had been loaded.
     *
     * @param world loaded world
     */
    void onWorldLoaded(PlotWorld world);

    /**
     * Gets called when claiming a plot.
     *
     * @param event the event
     */
    void onPlotClaiming(PlotClaimEvent event);

    /**
     * Gets called after a plot was claimed.
     *
     * @param plot The plot that was claimed
     */
    void onPlotClaimed(Plot plot);

    /**
     * Gets called after a plot was removed.
     *
     * @param plot The plot that was removed
     */
    void onPlotRemoved(Plot plot);

    /**
     * Gets called before a plot is cleared.
     *
     * @param plot The plot that will be cleared
     */
    void onBeforePlotCleared(Plot plot);

    /**
     * Gets called after a plot was cleared.
     *
     * @param plot The plot that was cleared
     */
    void onPlotCleared(Plot plot);

    /**
     * Gets called after the protection of a plot was enabled or disabled.
     *
     * @param plot        The plot that the protection was changed of
     * @param isProtected Shows whether the plot is now protected or not
     */
    void onPlotProtectionChanged(Plot plot, boolean isProtected);

    /**
     * Gets called after the owner of a plot has been changed.
     *
     * @param plot     The plot that the owner was changed of
     * @param oldOwner The old owner
     */
    void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner);

    /**
     * Gets called after the connection of two plots changed.
     *
     * @param a         a plot
     * @param b         another plot
     * @param linkedNow whether the plots are linked now
     */
    void onPlotConnectionChanged(Plot a, Plot b, boolean linkedNow);

    /**
     * Gets called after the nickname of a player was changed.
     *
     * @param player Player the nickname was changed of
     * @param plots  Plots that the player owns
     */
    void onPlayerNameChanged(SimplePlayer player, Collection<Plot> plots);
}
