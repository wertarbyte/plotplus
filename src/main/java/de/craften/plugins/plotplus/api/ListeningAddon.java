package de.craften.plugins.plotplus.api;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;

import java.util.Collection;

/**
 * An addon that is automatically registered as listener for all worlds.
 */
public abstract class ListeningAddon extends Addon implements PlotListener {
    /**
     * Creates a new add-on instance.
     *
     * @param name         Unique name of this add-on
     * @param dependencies Names of plugins this add-on depends on
     */
    public ListeningAddon(String name, String... dependencies) {
        super(name, dependencies);
        getApi().addListener(this);
    }

    @Override
    public final void onWorldLoaded(PlotWorld world) {
    }

    @Override
    public void onPlotClaiming(PlotClaimEvent event) {

    }

    @Override
    public void onPlotClaimed(Plot plot) {

    }

    @Override
    public void onPlotRemoved(Plot plot) {

    }

    @Override
    public void onBeforePlotCleared(Plot plot) {

    }

    @Override
    public void onPlotCleared(Plot plot) {

    }

    @Override
    public void onPlotProtectionChanged(Plot plot, boolean isProtected) {

    }

    @Override
    public void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner) {

    }

    @Override
    public void onPlayerNameChanged(SimplePlayer player, Collection<Plot> plots) {

    }
}
