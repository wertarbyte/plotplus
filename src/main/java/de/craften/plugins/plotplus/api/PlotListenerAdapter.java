package de.craften.plugins.plotplus.api;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;

import java.util.Collection;

/**
 * An adapter for the {@link de.craften.plugins.plotplus.api.PlotListener}. Simply override the methods you need.
 */
public abstract class PlotListenerAdapter implements PlotListener {
    @Override
    public void onWorldLoaded(PlotWorld world) {

    }

    @Override
    public void onPlotClaiming(PlotClaimEvent event) {

    }

    @Override
    public void onPlotClaimed(Plot plot) {

    }

    @Override
    public void onPlotRemoved(Plot plot) {

    }

    @Override
    public void onBeforePlotCleared(Plot plot) {

    }

    @Override
    public void onPlotCleared(Plot plot) {

    }

    @Override
    public void onPlotProtectionChanged(Plot plot, boolean isProtected) {

    }

    @Override
    public void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner) {

    }

    @Override
    public void onPlayerNameChanged(SimplePlayer player, Collection<Plot> plots) {

    }

    @Override
    public void onPlotConnectionChanged(Plot a, Plot b, boolean linkedNow) {

    }
}
