package de.craften.plugins.plotplus.api;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ApiHub implements PlotListener, Listenable<PlotListener> {
    private List<PlotListener> listeners;

    public ApiHub() {
        listeners = new ArrayList<>();
    }

    @Override
    public void addListener(PlotListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public void removeListener(PlotListener listener) {
        this.listeners.remove(listener);
    }

    @Override
    public void onWorldLoaded(PlotWorld world) {
        for (PlotListener listener : listeners)
            listener.onWorldLoaded(world);
    }

    @Override
    public void onPlotClaiming(PlotClaimEvent event) {
        for (PlotListener listener : listeners)
            listener.onPlotClaiming(event);
    }

    @Override
    public void onPlotClaimed(Plot plot) {
        for (PlotListener listener : listeners)
            listener.onPlotClaimed(plot);
    }

    @Override
    public void onPlotRemoved(Plot plot) {
        for (PlotListener listener : listeners)
            listener.onPlotRemoved(plot);
    }

    @Override
    public void onBeforePlotCleared(Plot plot) {
        for (PlotListener listener : listeners)
            listener.onBeforePlotCleared(plot);
    }

    @Override
    public void onPlotCleared(Plot plot) {
        for (PlotListener listener : listeners)
            listener.onPlotCleared(plot);
    }

    @Override
    public void onPlotProtectionChanged(Plot plot, boolean isProtected) {
        for (PlotListener listener : listeners)
            listener.onPlotProtectionChanged(plot, isProtected);
    }

    @Override
    public void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner) {
        for (PlotListener listener : listeners)
            listener.onPlotOwnerChanged(plot, oldOwner);
    }

    @Override
    public void onPlotConnectionChanged(Plot a, Plot b, boolean linkedNow) {
        for (PlotListener listener : listeners) {
            listener.onPlotConnectionChanged(a, b, linkedNow);
        }
    }

    @Override
    public void onPlayerNameChanged(SimplePlayer player, Collection<Plot> plots) {
        for (PlotListener listener : listeners)
            listener.onPlayerNameChanged(player, plots);
    }
}
