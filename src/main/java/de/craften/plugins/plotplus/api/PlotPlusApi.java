package de.craften.plugins.plotplus.api;

import de.craften.plugins.plotplus.economy.EconomyManager;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import org.bukkit.World;

/**
 * The main API hook for PlotPlus add-ons.
 */
public interface PlotPlusApi extends Listenable<PlotListener> {
    /**
     * Registers the given add-on. If PlotPlus is already enabled, the addon will be enabled immediately (or after
     * dependencies are loaded). If PlotPlus is not yet enabled, the add-on will be enabled later.
     * <p/>
     * Notice that the add-on's names are unique. Re-registering an add-on will disable the registered add-on and
     * enable the new one.
     *
     * @param addon Add-on to register
     */
    public void registerAddon(Addon addon);

    /**
     * Unregisters the add-on with the given name. If the add-on is enabled, it will be disabled.
     *
     * @param name Name of the add-on to unregister
     * @return True if the add-on was registered previously, false if not
     */
    public boolean unregisterAddon(String name);

    /**
     * Registers the given world scheme with the given name.
     *
     * @param name   name of the world scheme (case insensitive)
     * @param scheme scheme
     */
    public void registerWorldScheme(String name, Class<? extends PlotWorldScheme> scheme);

    /**
     * Gets the economy  of PlotPlus.
     *
     * @return economy or a dummy economy if it's disabled
     */
    public EconomyManager getEconomy();

    /**
     * Gets the {@link de.craften.plugins.plotplus.plot.world.PlotWorld} instance for the given world. If that world
     * isn't a plot world, null is returned.
     *
     * @param world world to get the {@link de.craften.plugins.plotplus.plot.world.PlotWorld} instance of
     * @return {@link de.craften.plugins.plotplus.plot.world.PlotWorld} instance of that world
     */
    public PlotWorld getWorld(World world);

    /**
     * Gets the {@link de.craften.plugins.plotplus.plot.world.PlotWorld} instance for the world with the given name.
     * If that world doesn't exist or isn't a plot world, null is returned.
     *
     * @param world world to get the {@link de.craften.plugins.plotplus.plot.world.PlotWorld} instance of
     * @return {@link de.craften.plugins.plotplus.plot.world.PlotWorld} instance of that world
     */
    public PlotWorld getWorld(String world);

    /**
     * Adds an event listener that is only invoked for events in a specific world and global events.
     *
     * @param world    world to listen for events
     * @param listener listener to invoke
     */
    public void addListener(PlotWorld world, PlotListener listener);
}
