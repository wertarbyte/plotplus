package de.craften.plugins.plotplus.worldtypes.generic;

import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.worldtypes.ChunkResettable;

/**
 * A generic plot structure that will simply take a world and split it into plots that have a size of 16*n x 16*m
 * blocks, where m,n are natural numbers greater than zero.
 */
class GenericPlotStructure extends PlotStructure implements ChunkResettable {
    private int width;
    private int length;

    /**
     * @param width  Width of a plot in chunks
     * @param length Length of a plot in chunks
     */
    public GenericPlotStructure(int width, int length) {
        this.width = width * 16;
        this.length = length * 16;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getLength() {
        return length;
    }
}
