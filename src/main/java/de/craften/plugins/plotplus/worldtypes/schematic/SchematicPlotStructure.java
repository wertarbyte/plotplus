package de.craften.plugins.plotplus.worldtypes.schematic;

import de.craften.plugins.plotplus.generator.BlockProvider;
import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.generator.blocks.SignBlock;
import de.craften.plugins.plotplus.util.PointXZ;
import org.jnbt.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SchematicPlotStructure extends PlotStructure implements BlockProvider {
    private int width;
    private int length;

    private Map<Integer, Map<String, Tag>> idxEntitiesMap;
    private byte[] blocks;
    private byte[] data;

    public SchematicPlotStructure(File schematic) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(schematic);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        NBTInputStream nis = new NBTInputStream(fis);
        CompoundTag master = (CompoundTag) nis.readTag();
        nis.close();
        Map<String, Tag> masterMap = master.getValue();

        width = ((ShortTag) masterMap.get("Width")).getValue();
        length = ((ShortTag) masterMap.get("Length")).getValue();
        //height = ((ShortTag) masterMap.get("Height")).getValue();

        blocks = ((ByteArrayTag) masterMap.get("Blocks")).getValue();
        data = ((ByteArrayTag) masterMap.get("Data")).getValue();

        // get tile entities
        List<Tag> entities = ((ListTag) masterMap.get("TileEntities")).getValue();
        idxEntitiesMap = new TreeMap<>();
        for (Tag tag : entities) {
            Map<String, Tag> cmpMap = ((CompoundTag) tag).getValue();
            int y = ((IntTag) cmpMap.get("z")).getValue(); // mc's y and z are not the same
            int z = ((IntTag) cmpMap.get("y")).getValue();
            int x = ((IntTag) cmpMap.get("x")).getValue();

            int blockIndex = x + (y + z * width) * length;
            idxEntitiesMap.put(blockIndex, cmpMap);
        }
    }

    @Override
    public Block getBlockAt(int rx, int y, int rz) {
        PointXZ relPos = getRelativePosition(rx, rz);
        int index = y * width * length + relPos.getZ() * width + relPos.getX();

        if (blocks.length > index) {
            short id = (short) (blocks[index] & 0xFF);

            if (id == 63 || id == 68) {
                // sign
                Map<String, Tag> tileEntity = idxEntitiesMap.get(index);
                if (tileEntity == null) {
                    return new SignBlock(id, data[index]);
                } else {
                    String text[] = new String[4];
                    text[0] = ((StringTag) tileEntity.get("Text1")).getValue();
                    text[1] = ((StringTag) tileEntity.get("Text2")).getValue();
                    text[2] = ((StringTag) tileEntity.get("Text3")).getValue();
                    text[3] = ((StringTag) tileEntity.get("Text4")).getValue();
                    return new SignBlock(id, data[index], text);
                }
            }
            return new Block(id, data[index]);
        }
        return Block.AIR;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getLength() {
        return length;
    }
}
