package de.craften.plugins.plotplus.worldtypes.plotme;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.api.PlotListener;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.generator.blocks.SignBlock;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.Direction;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.SubDirection;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

import java.util.Collection;


class PlotmeWorldListener implements PlotListener {
    @Override
    public void onWorldLoaded(PlotWorld world) {
    }

    @Override
    public void onPlotClaiming(PlotClaimEvent event) {
    }

    @Override
    public void onPlotClaimed(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();

        RectangularRegion r = plot.getCoreBuildRegion();
        BlockState bs = plot.getWorld().getBukkitWorld().getBlockAt(r.getXMin() - 1, structure.getRoadHeight() + 1, r.getZMin() - 2).getState();
        if (bs instanceof Sign) {
            Sign sign = (Sign) bs;
            sign.setLine(2, plot.getOwner().getDisplayName());
            sign.update(true);
        }

        if (plot.getWorld().getConfig().isConnectedPlotsEnabled()) {
            updatePlotBorders(plot);
        }
    }

    private void updatePlotBorders(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion r = plot.getCoreBuildRegion();

        if (plot.isConnectedWithRelative(-1, 0)) {
            removeWestBorder(plot);
        } else {
            restoreWestBorder(plot);
        }

        if (plot.isConnectedWithRelative(+1, 0)) {
            removeEastBorder(plot);
        } else {
            restoreEastBorder(plot);
        }

        if (plot.isConnectedWithRelative(0, -1)) {
            removeNorthBorder(plot);
        } else {
            restoreNorthBorder(plot);
        }

        if (plot.isConnectedWithRelative(0, +1)) {
            removeSouthBorder(plot);
        } else {
            restoreSouthBorder(plot);
        }

        //If four plots are connected as a square, the cross in the middle needs to be removed
        if (plot.isConnectedWithRelative(-1, -1)) {
            for (int x = r.getXMin() - structure.getPathWidth() - 2; x < r.getXMin(); x++) {
                for (int z = r.getZMin() - structure.getPathWidth() - 2; z < r.getZMin(); z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else {
            updateCrossroadAt(plot, SubDirection.NORTH_WEST, false);
        }
        if (plot.isConnectedWithRelative(1, -1)) {
            for (int x = r.getXMax() + 1; x <= r.getXMax() + structure.getPathWidth() + 2; x++) {
                for (int z = r.getZMin() - structure.getPathWidth() - 2; z < r.getZMin(); z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else {
            updateCrossroadAt(plot, SubDirection.NORTH_EAST, false);
        }
        if (plot.isConnectedWithRelative(-1, 1)) {
            for (int x = r.getXMin() - structure.getPathWidth() - 2; x < r.getXMin(); x++) {
                for (int z = r.getZMax() + 1; z <= r.getZMax() + structure.getPathWidth() + 2; z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else {
            updateCrossroadAt(plot, SubDirection.SOUTH_WEST, false);
        }
        if (plot.isConnectedWithRelative(1, 1)) {
            for (int x = r.getXMax() + 1; x <= r.getXMax() + structure.getPathWidth() + 2; x++) {
                for (int z = r.getZMax() + 1; z <= r.getZMax() + structure.getPathWidth() + 2; z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else {
            updateCrossroadAt(plot, SubDirection.SOUTH_EAST, false);
        }
    }

    /**
     * Removes the north-side border of the given plot.
     *
     * @param plot Plot to remove the border of
     */

    private void removeNorthBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        int oz = region.getZMin() - structure.getPathWidth() - 2;
        for (int x = region.getXMin(); x <= region.getXMax(); x++) {
            //Remove walls
            plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, region.getZMin() - 1).setType(Material.AIR);
            plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, oz).setType(Material.AIR);

            //Replace street
            for (int z = oz; z <= region.getZMin() - 1; z++)
                structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
        }
    }

    private void restoreNorthBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        //Restore street
        int oz = region.getZMin() - structure.getPathWidth() - 2;
        for (int x = region.getXMin(); x <= region.getXMax(); x++) {
            for (int z = oz; z <= region.getZMin() - 1; z++) {
                for (int y = 0; y <= plot.getWorld().getBukkitWorld().getMaxHeight(); y++) {
                    structure.getBlockAt(x, y, z).applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, y, z));
                }
            }
        }

        buildWalls(plot, Direction.NORTH);
    }

    /**
     * Removes the east-side border of the given plot.
     *
     * @param plot Plot to remove the border of
     */
    private void removeEastBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        int ox = region.getXMax() + structure.getPathWidth() + 2;
        for (int z = region.getZMin(); z <= region.getZMax(); z++) {
            //Remove walls
            plot.getWorld().getBukkitWorld().getBlockAt(region.getXMax() + 1, structure.getRoadHeight() + 1, z).setType(Material.AIR);
            plot.getWorld().getBukkitWorld().getBlockAt(ox, structure.getRoadHeight() + 1, z).setType(Material.AIR);

            //Replace street
            for (int x = region.getXMax(); x <= ox; x++)
                structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
        }
    }

    private void restoreEastBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        //Restore street

        int ox = region.getXMax() + structure.getPathWidth() + 2;
        for (int z = region.getZMin(); z <= region.getZMax(); z++) {
            for (int x = region.getXMax(); x <= ox; x++) {
                for (int y = 0; y <= plot.getWorld().getBukkitWorld().getMaxHeight(); y++) {
                    structure.getBlockAt(x, y, z).applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, y, z));
                }
            }
        }

        buildWalls(plot, Direction.EAST);
    }

    /**
     * Removes the south-side border of the given plot.
     *
     * @param plot Plot to remove the border of
     */
    private void removeSouthBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        int oz = region.getZMax() + structure.getPathWidth() + 2;
        for (int x = region.getXMin(); x <= region.getXMax(); x++) {
            //Remove walls
            plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, region.getZMax() + 1).setType(Material.AIR);
            plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, oz).setType(Material.AIR);

            //Replace street
            for (int z = region.getZMax(); z <= oz; z++)
                structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
        }
    }

    private void restoreSouthBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        //Restore street

        int oz = region.getZMax() + structure.getPathWidth() + 2;
        for (int x = region.getXMin(); x <= region.getXMax(); x++) {
            for (int z = region.getZMax(); z <= oz; z++) {
                for (int y = 0; y <= plot.getWorld().getBukkitWorld().getMaxHeight(); y++) {
                    structure.getBlockAt(x, y, z).applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, y, z));
                }
            }
        }

        buildWalls(plot, Direction.SOUTH);
    }

    /**
     * Removes the west-side border of the given plot.
     *
     * @param plot Plot to remove the border of
     */
    private void removeWestBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        int ox = region.getXMin() - structure.getPathWidth() - 2;
        for (int z = region.getZMin(); z <= region.getZMax(); z++) {
            //Remove walls
            plot.getWorld().getBukkitWorld().getBlockAt(region.getXMin() - 1, structure.getRoadHeight() + 1, z).setType(Material.AIR);
            plot.getWorld().getBukkitWorld().getBlockAt(ox, structure.getRoadHeight() + 1, z).setType(Material.AIR);

            //Replace street
            for (int x = ox; x <= region.getXMin() - 1; x++)
                structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
        }
    }

    private void restoreWestBorder(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion region = plot.getCoreBuildRegion();

        //Restore street
        int ox = region.getXMin() - structure.getPathWidth() - 2;
        for (int z = region.getZMin(); z <= region.getZMax(); z++) {
            for (int x = ox; x <= region.getXMin() - 1; x++) {
                for (int y = 0; y <= plot.getWorld().getBukkitWorld().getMaxHeight(); y++) {
                    structure.getBlockAt(x, y, z).applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, y, z));
                }
            }
        }

        buildWalls(plot, Direction.WEST);
    }

    @Override
    public void onPlotRemoved(Plot plot) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion r = plot.getCoreBuildRegion();

        if (plot.getWorld().getConfig().isConnectedPlotsEnabled()) {
            buildWalls(plot, Direction.WEST);
            buildWalls(plot, Direction.EAST);
            buildWalls(plot, Direction.NORTH);
            buildWalls(plot, Direction.SOUTH);

            //re-set sign for that plot
            new SignBlock(68, 2, "ID:" + plot.getIdX() + ";" + (plot.getIdZ() + 1), "", plot.getOwner().getDisplayName())
                    .applyOn(plot.getWorld().getBukkitWorld().getBlockAt(r.getXMin() - 1, structure.getRoadHeight() + 1, r.getZMax() + structure.getPathWidth() + 1));

            updateCrossroadAt(plot, SubDirection.NORTH_WEST, true);
            updateCrossroadAt(plot, SubDirection.NORTH_EAST, true);
            updateCrossroadAt(plot, SubDirection.SOUTH_WEST, true);
            updateCrossroadAt(plot, SubDirection.SOUTH_EAST, true);
        }

        new SignBlock(68, 2, "ID:" + plot.getIdX() + ";" + plot.getIdZ())
                .applyOn(plot.getWorld().getBukkitWorld().getBlockAt(r.getXMin() - 1, structure.getRoadHeight() + 1, r.getZMin() - 2));
    }

    @Override
    public void onBeforePlotCleared(Plot plot) {
    }

    @Override
    public void onPlotCleared(Plot plot) {
        if (plot.getWorld().getConfig().isConnectedPlotsEnabled())
            updatePlotBorders(plot);
    }

    @Override
    public void onPlotProtectionChanged(Plot plot, boolean isProtected) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        Block wall = isProtected ? structure.getProtectedWall() : structure.getWall();
        RectangularRegion r = plot.getCoreBuildRegion();
        World world = plot.getWorld().getBukkitWorld();

        if (hasRoadAt(plot, Direction.WEST))
            for (int z = r.getZMin() - 1; z <= r.getZMax() + 1; z++) {
                wall.applyOn(world.getBlockAt(r.getXMin() - 1, structure.getRoadHeight() + 1, z));
            }

        if (hasRoadAt(plot, Direction.EAST))
            for (int z = r.getZMin() - 1; z <= r.getZMax() + 1; z++) {
                wall.applyOn(world.getBlockAt(r.getXMax() + 1, structure.getRoadHeight() + 1, z));
            }

        if (hasRoadAt(plot, Direction.NORTH))
            for (int x = r.getXMin() - 1; x <= r.getXMax() + 1; x++) {
                wall.applyOn(world.getBlockAt(x, structure.getRoadHeight() + 1, r.getZMin() - 1));
            }

        if (hasRoadAt(plot, Direction.SOUTH))
            for (int x = r.getXMin() - 1; x <= r.getXMax() + 1; x++) {
                wall.applyOn(world.getBlockAt(x, structure.getRoadHeight() + 1, r.getZMax() + 1));
            }
    }

    @Override
    public void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();

        //Update the sign
        RectangularRegion r = plot.getCoreBuildRegion();
        BlockState bs = plot.getWorld().getBukkitWorld().getBlockAt(r.getXMin() - 1, structure.getRoadHeight() + 1, r.getZMin() - 2).getState();
        if (bs instanceof Sign) {
            Sign sign = (Sign) bs;
            sign.setLine(2, plot.getOwner().getDisplayName());
            sign.update(true);
        }

        updatePlotBorders(plot);
    }

    @Override
    public void onPlotConnectionChanged(Plot a, Plot b, boolean linkedNow) {
        // a and b are adjacent plots
        if (linkedNow) {
            // plots are now connected
            if (a.getIdX() == b.getIdX() - 1) { // a west of b
                removeEastBorder(a);
                removeWestBorder(b);
            } else if (a.getIdX() == b.getIdX() + 1) { // b west of a
                removeWestBorder(a);
                removeEastBorder(b);
            } else if (a.getIdZ() == b.getIdZ() - 1) { // a north of b
                removeSouthBorder(a);
                removeNorthBorder(b);
            } else if (a.getIdZ() == b.getIdZ() + 1) { // b north of a
                removeNorthBorder(a);
                removeSouthBorder(b);
            }

            // if four plots are connected as a square, the cross in the middle needs to be removed
            if (a.getIdX() == b.getIdX() - 1) { // a west of b
                if (a.isConnectedWithRelative(1, -1)) {
                    removeCrossroadAt(a, SubDirection.NORTH_EAST);
                }
                if (a.isConnectedWithRelative(1, 1)) {
                    removeCrossroadAt(a, SubDirection.SOUTH_EAST);
                }
            } else if (a.getIdX() == b.getIdX() + 1) { // b west of a
                if (a.isConnectedWithRelative(-1, -1)) {
                    removeCrossroadAt(a, SubDirection.NORTH_WEST);
                }
                if (a.isConnectedWithRelative(-1, 1)) {
                    removeCrossroadAt(a, SubDirection.SOUTH_WEST);
                }
            } else if (a.getIdZ() == b.getIdZ() - 1) { // a north of b
                if (a.isConnectedWithRelative(-1, 1)) {
                    removeCrossroadAt(a, SubDirection.SOUTH_WEST);
                }
                if (a.isConnectedWithRelative(1, 1)) {
                    removeCrossroadAt(a, SubDirection.SOUTH_EAST);
                }
            } else if (a.getIdZ() == b.getIdZ() + 1) { // b north of a
                if (a.isConnectedWithRelative(-1, -1)) {
                    removeCrossroadAt(a, SubDirection.NORTH_WEST);
                }
                if (a.isConnectedWithRelative(1, -1)) {
                    removeCrossroadAt(a, SubDirection.NORTH_EAST);
                }
            }
        } else {
            // plots are now disconnected
            if (a.getIdX() == b.getIdX() - 1) { // a west of b
                restoreEastBorder(a);
                restoreWestBorder(b);
            } else if (a.getIdX() == b.getIdX() + 1) { // b west of a
                restoreWestBorder(a);
                restoreEastBorder(b);
            } else if (a.getIdZ() == b.getIdZ() - 1) { // a north of b
                restoreSouthBorder(a);
                restoreNorthBorder(b);
            } else if (a.getIdZ() == b.getIdZ() + 1) { // b north of a
                restoreNorthBorder(a);
                restoreSouthBorder(b);
            }

            // if four plots were connected as a square, the cross in the middle needs to be restored
            if (a.getIdX() == b.getIdX() - 1) { // a west of b
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.NORTH_EAST)) {
                    updateCrossroadAt(a, SubDirection.NORTH_EAST, false);
                }
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.SOUTH_EAST)) {
                    updateCrossroadAt(a, SubDirection.SOUTH_EAST, false);
                }
            } else if (a.getIdX() == b.getIdX() + 1) { // b west of a
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.NORTH_WEST)) {
                    updateCrossroadAt(a, SubDirection.NORTH_WEST, false);
                }
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.SOUTH_WEST)) {
                    updateCrossroadAt(a, SubDirection.SOUTH_WEST, false);
                }
            } else if (a.getIdZ() == b.getIdZ() - 1) { // a north of b
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.SOUTH_WEST)) {
                    updateCrossroadAt(a, SubDirection.SOUTH_WEST, false);
                }
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.SOUTH_EAST)) {
                    updateCrossroadAt(a, SubDirection.SOUTH_EAST, false);
                }
            } else if (a.getIdZ() == b.getIdZ() + 1) { // b north of a
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.NORTH_WEST)) {
                    updateCrossroadAt(a, SubDirection.NORTH_WEST, false);
                }
                if (wouldHaveNoCrossroadAt(a, b, SubDirection.NORTH_EAST)) {
                    updateCrossroadAt(a, SubDirection.NORTH_EAST, false);
                }
            }
        }
    }

    @Override
    public void onPlayerNameChanged(SimplePlayer player, Collection<Plot> plots) {
        for (Plot plot : plots) {
            PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();

            RectangularRegion r = plot.getCoreBuildRegion();
            BlockState bs = plot.getWorld().getBukkitWorld().getBlockAt(r.getXMin() - 1, structure.getRoadHeight() + 1, r.getZMin() - 2).getState();
            if (bs instanceof Sign) {
                Sign sign = (Sign) bs;
                sign.setLine(2, player.getDisplayName());
                sign.update(true);
            }
        }
    }

    private boolean hasRoadAt(Plot plot, Direction direction) {
        switch (direction) {
            case NORTH:
                return !plot.isConnectedWithRelative(0, -1);
            case EAST:
                return !plot.isConnectedWithRelative(1, 0);
            case SOUTH:
                return !plot.isConnectedWithRelative(0, 1);
            case WEST:
                return !plot.isConnectedWithRelative(-1, 0);
            default:
                return false;
        }
    }

    private void buildWalls(Plot plot, Direction direction) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        Block wall = plot.isProtected() ? structure.getProtectedWall() : structure.getWall();
        RectangularRegion region = plot.getCoreBuildRegion();
        Plot other;
        Block otherWall;

        switch (direction) {
            case NORTH:
                other = plot.getWorld().getPlot(plot.getIdX(), plot.getIdZ() - 1);
                otherWall = other != null && other.isProtected() ? structure.getProtectedWall() : structure.getWall();

                for (int x = region.getXMin() - 1; x <= region.getXMax() + 1; x++) {
                    wall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, region.getZMin() - 1));
                    otherWall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, region.getZMin() - 2 - structure.getPathWidth()));
                }
                break;
            case EAST:
                other = plot.getWorld().getPlot(plot.getIdX() + 1, plot.getIdZ());
                otherWall = other != null && other.isProtected() ? structure.getProtectedWall() : structure.getWall();

                for (int z = region.getZMin() - 1; z <= region.getZMax() + 1; z++) {
                    wall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(region.getXMax() + 1, structure.getRoadHeight() + 1, z));
                    otherWall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(region.getXMax() + 2 + structure.getPathWidth(), structure.getRoadHeight() + 1, z));
                }
                break;
            case SOUTH:
                other = plot.getWorld().getPlot(plot.getIdX(), plot.getIdZ() + 1);
                otherWall = other != null && other.isProtected() ? structure.getProtectedWall() : structure.getWall();

                for (int x = region.getXMin() - 1; x <= region.getXMax() + 1; x++) {
                    wall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, region.getZMax() + 1));
                    otherWall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, region.getZMax() + 2 + structure.getPathWidth()));
                }
                break;
            case WEST:
                other = plot.getWorld().getPlot(plot.getIdX() - 1, plot.getIdZ());
                otherWall = other != null && other.isProtected() ? structure.getProtectedWall() : structure.getWall();

                for (int z = region.getZMin() - 1; z <= region.getZMax() + 1; z++) {
                    wall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(region.getXMin() - 1, structure.getRoadHeight() + 1, z));
                    otherWall.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(region.getXMin() - 2 - structure.getPathWidth(), structure.getRoadHeight() + 1, z));
                }
                break;
        }
    }

    private void updateCrossroadAt(Plot plot, SubDirection direction, boolean plotGotRemoved) {
        /*
          A || B
          ==[]==
          C || D
         */
        Plot a, b, c, d;
        PointXZ swCrossroad; //south-west crossroad-position
        RectangularRegion plotRegion = plot.getCoreBuildRegion();
        PlotmePlotStructure structure = ((PlotmeWorldScheme) plot.getWorld().getScheme()).getStructure();
        int roadWidth = structure.getPathWidth();
        switch (direction) {
            case NORTH_EAST:
                c = plotGotRemoved ? null : plot;
                a = plot.getWorld().getPlot(plot.getIdX(), plot.getIdZ() - 1);
                b = plot.getWorld().getPlot(plot.getIdX() + 1, plot.getIdZ() - 1);
                d = plot.getWorld().getPlot(plot.getIdX() + 1, plot.getIdZ());
                swCrossroad = new PointXZ(plotRegion.getXMax() + 2, plotRegion.getZMin() - 1 - roadWidth);
                break;
            case NORTH_WEST:
                d = plotGotRemoved ? null : plot;
                a = plot.getWorld().getPlot(plot.getIdX() - 1, plot.getIdZ() - 1);
                b = plot.getWorld().getPlot(plot.getIdX(), plot.getIdZ() - 1);
                c = plot.getWorld().getPlot(plot.getIdX() - 1, plot.getIdZ());
                swCrossroad = new PointXZ(plotRegion.getXMin() - 1 - roadWidth, plotRegion.getZMin() - 1 - roadWidth);
                break;
            case SOUTH_EAST:
                a = plotGotRemoved ? null : plot;
                b = plot.getWorld().getPlot(plot.getIdX() + 1, plot.getIdZ());
                c = plot.getWorld().getPlot(plot.getIdX(), plot.getIdZ() + 1);
                d = plot.getWorld().getPlot(plot.getIdX() + 1, plot.getIdZ() + 1);
                swCrossroad = new PointXZ(plotRegion.getXMax() + 2, plotRegion.getZMax() + 2);
                break;
            case SOUTH_WEST:
                b = plotGotRemoved ? null : plot;
                a = plot.getWorld().getPlot(plot.getIdX() - 1, plot.getIdZ());
                c = plot.getWorld().getPlot(plot.getIdX() - 1, plot.getIdZ() + 1);
                d = plot.getWorld().getPlot(plot.getIdX(), plot.getIdZ() + 1);
                swCrossroad = new PointXZ(plotRegion.getXMin() - 1 - roadWidth, plotRegion.getZMax() + 2);
                break;
            default:
                return;
        }

        for (int i = 0; i < roadWidth + 2; i++) {
            for (int j = 0; j < roadWidth + 2; j++) {
                for (int y = 0; y <= plot.getWorld().getBukkitWorld().getMaxHeight(); y++) {
                    structure.getBlockAt(swCrossroad.getX() - 1 + i, y, swCrossroad.getZ() - 1 + j)
                            .applyOn(plot.getWorld().getBukkitWorld().getBlockAt(swCrossroad.getX() - 1 + i, y, swCrossroad.getZ() - 1 + j));
                }
            }
        }

        World world = plot.getWorld().getBukkitWorld();
        if (isRoadBetween(a, b)) {
            for (int i = 0; i < roadWidth; i++)
                Block.AIR.applyOn(world
                        .getBlockAt(swCrossroad.getX() + i, structure.getRoadHeight() + 1, swCrossroad.getZ() - 1));
        } else {
            for (int i = 0; i < roadWidth; i++)
                structure.getWall().applyOn(world
                        .getBlockAt(swCrossroad.getX() + i, structure.getRoadHeight() + 1, swCrossroad.getZ() - 1));
        }

        if (isRoadBetween(c, d)) {
            for (int i = 0; i < roadWidth; i++)
                Block.AIR.applyOn(world
                        .getBlockAt(swCrossroad.getX() + i, structure.getRoadHeight() + 1, swCrossroad.getZ() + roadWidth));
        } else {
            for (int i = 0; i < roadWidth; i++)
                structure.getWall().applyOn(world
                        .getBlockAt(swCrossroad.getX() + i, structure.getRoadHeight() + 1, swCrossroad.getZ() + roadWidth));
        }

        if (isRoadBetween(a, c)) {
            for (int i = 0; i < roadWidth; i++)
                Block.AIR.applyOn(world
                        .getBlockAt(swCrossroad.getX() - 1, structure.getRoadHeight() + 1, swCrossroad.getZ() + i));
        } else {
            for (int i = 0; i < roadWidth; i++)
                structure.getWall().applyOn(world
                        .getBlockAt(swCrossroad.getX() - 1, structure.getRoadHeight() + 1, swCrossroad.getZ() + i));
        }

        if (isRoadBetween(b, d)) {
            for (int i = 0; i < roadWidth; i++)
                Block.AIR.applyOn(world
                        .getBlockAt(swCrossroad.getX() + roadWidth, structure.getRoadHeight() + 1, swCrossroad.getZ() + i));
        } else {
            for (int i = 0; i < roadWidth; i++)
                structure.getWall().applyOn(world
                        .getBlockAt(swCrossroad.getX() + roadWidth, structure.getRoadHeight() + 1, swCrossroad.getZ() + i));
        }
    }

    private void removeCrossroadAt(Plot plot, SubDirection direction) {
        PlotmePlotStructure structure = (PlotmePlotStructure) plot.getWorld().getScheme().getStructure();
        RectangularRegion r = plot.getCoreBuildRegion();

        if (direction == SubDirection.NORTH_WEST) {
            for (int x = r.getXMin() - structure.getPathWidth() - 2; x < r.getXMin(); x++) {
                for (int z = r.getZMin() - structure.getPathWidth() - 2; z < r.getZMin(); z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else if (direction == SubDirection.NORTH_EAST) {
            for (int x = r.getXMax() + 1; x <= r.getXMax() + structure.getPathWidth() + 2; x++) {
                for (int z = r.getZMin() - structure.getPathWidth() - 2; z < r.getZMin(); z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else if (direction == SubDirection.SOUTH_WEST) {
            for (int x = r.getXMin() - structure.getPathWidth() - 2; x < r.getXMin(); x++) {
                for (int z = r.getZMax() + 1; z <= r.getZMax() + structure.getPathWidth() + 2; z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        } else if (direction == SubDirection.SOUTH_EAST) {
            for (int x = r.getXMax() + 1; x <= r.getXMax() + structure.getPathWidth() + 2; x++) {
                for (int z = r.getZMax() + 1; z <= r.getZMax() + structure.getPathWidth() + 2; z++) {
                    structure.getPlotFloor().applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight(), z));
                    Block.AIR.applyOn(plot.getWorld().getBukkitWorld().getBlockAt(x, structure.getRoadHeight() + 1, z));
                }
            }
        }
    }

    private boolean isRoadBetween(Plot a, Plot b) {
        return a == null || b == null || !a.isConnectedWith(b);
    }

    /**
     * Checks if the plot a would not have a crossroad at the given corner if it was connected to the adjacent plot b.
     *
     * @param a      plot a
     * @param b      plot b
     * @param corner corner to check
     * @return true if the plot a would have no crossroad at the specified corner if it was connected to b, false otherwise
     */
    private boolean wouldHaveNoCrossroadAt(Plot a, Plot b, SubDirection corner) {
        // we can use symmetry here
        if (a.getIdX() > b.getIdX())
            return wouldHaveNoCrossroadAt(b, a, corner.mirrorWestEast());
        if (a.getIdZ() > b.getIdZ())
            return wouldHaveNoCrossroadAt(b, a, corner.mirrorNorthSouth());

        if (a.getIdX() == b.getIdX() - 1) { // a west of b
            if (corner == SubDirection.NORTH_EAST) {
                return a.isConnectedWithRelative(0, -1)
                        && a.getWorld().getPlotRelative(a, 0, -1).isConnectedWithRelative(1, 0)
                        && a.getWorld().getPlotRelative(a, 1, -1).isConnectedWithRelative(0, 1);
            } else if (corner == SubDirection.SOUTH_EAST) {
                return a.isConnectedWithRelative(0, 1)
                        && a.getWorld().getPlotRelative(a, 0, 1).isConnectedWithRelative(1, 0)
                        && a.getWorld().getPlotRelative(a, 1, 1).isConnectedWithRelative(0, -1);
            }
        }

        if (a.getIdZ() == b.getIdZ() - 1) { // a north of b
            if (corner == SubDirection.SOUTH_WEST) {
                return a.isConnectedWithRelative(-1, 0)
                        && a.getWorld().getPlotRelative(a, -1, 0).isConnectedWithRelative(0, 1)
                        && a.getWorld().getPlotRelative(a, -1, 1).isConnectedWithRelative(1, 0);
            } else if (corner == SubDirection.SOUTH_EAST) {
                return a.isConnectedWithRelative(1, 0)
                        && a.getWorld().getPlotRelative(a, 1, 0).isConnectedWithRelative(0, 1)
                        && a.getWorld().getPlotRelative(a, 1, 1).isConnectedWithRelative(-1, 0);
            }
        }

        return false;
    }
}
