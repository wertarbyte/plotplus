package de.craften.plugins.plotplus.worldtypes.plotme;


import de.craften.plugins.plotplus.generator.BlockProvider;
import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.generator.blocks.SignBlock;
import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

/**
 * A structure that is compatible to PlotMe structures.
 */
class PlotmePlotStructure extends PlotStructure implements BlockProvider {
    private int plotsize;
    private int pathwidth; //without border around plots
    private byte bottom;
    private Block wall;
    private Block protectedWall;
    private Block plotfloor;
    private Block filling;
    private Block floor1;
    private Block floor2;
    private int roadheight;

    public PlotmePlotStructure(ConfigurationSection configuration) {
        if (configuration != null) {
            plotsize = configuration.getInt("plotsize", 32);
            pathwidth = configuration.getInt("pathwidth", 5);
            bottom = (byte) configuration.getInt("bottom", Material.BEDROCK.getId());
            wall = new Block(configuration.getString("wall", "44:0"));
            protectedWall = new Block(configuration.getString("protectedWall", "44:4"));
            plotfloor = new Block(configuration.getInt("plotfloor", Material.GRASS.getId()));
            filling = new Block(configuration.getInt("filling", Material.DIRT.getId()));
            floor1 = new Block(configuration.getString("floor1", "5:2"));
            floor2 = new Block(configuration.getString("floor2", "5:0"));
            roadheight = (byte) configuration.getInt("roadheight", 64);
        } else {
            plotsize = 32;
            pathwidth = 5;
            bottom = (byte) Material.BEDROCK.getId();
            wall = new Block(44);
            protectedWall = new Block(44, 4);
            plotfloor = new Block(Material.GRASS.getId());
            filling = new Block(Material.DIRT.getId());
            floor1 = new Block(Material.WOOD.getId(), 2);
            floor2 = new Block(Material.WOOD.getId(), 0);
            roadheight = 64;
        }
    }

    @Override
    public Block getBlockAt(int x, int y, int z) {
        PointXZ relPos = getRelativePosition(x, z);

        if (y == 0) {
            return new Block(bottom, 0);
        } else if (y == roadheight) {
            if (relPos.getX() < Math.floor(pathwidth / 2) || relPos.getX() > getWidth() - Math.ceil(pathwidth / 2)) {
                if (relPos.getZ() == Math.floor(pathwidth / 2) || relPos.getZ() == getLength() - Math.ceil(pathwidth / 2))
                    return floor2;
                if ((relPos.getX() == Math.floor(pathwidth / 2) - 1 || relPos.getX() == getWidth() - Math.ceil(pathwidth / 2) + 1) &&
                        (relPos.getZ() == Math.floor(pathwidth / 2) - 1 || relPos.getZ() == getLength() - Math.ceil(pathwidth / 2) + 1))
                    return floor2;
                return floor1;
            } else if (relPos.getZ() < Math.floor(pathwidth / 2) || relPos.getZ() > getLength() - Math.ceil(pathwidth / 2)) {
                if (relPos.getX() == Math.floor(pathwidth / 2) || relPos.getX() == getWidth() - Math.ceil(pathwidth / 2))
                    return floor2;
                return floor1;
            } else {
                if (relPos.getX() == Math.floor(pathwidth / 2) || relPos.getX() == getWidth() - Math.ceil(pathwidth / 2)) {
                    if (relPos.getZ() > Math.floor(pathwidth / 2) + 1 && relPos.getZ() < getLength() - Math.ceil(pathwidth / 2) - 1)
                        return floor2;
                    return floor1;
                } else if (relPos.getZ() == Math.floor(pathwidth / 2) || relPos.getZ() == getLength() - Math.ceil(pathwidth / 2)) {
                    if (relPos.getX() > Math.floor(pathwidth / 2) + 1 && relPos.getX() < getWidth() - Math.ceil(pathwidth / 2) - 1)
                        return floor2;
                    return floor1;
                }
            }
            return plotfloor;
        } else if (y == roadheight + 1) {
            if ((relPos.getX() == Math.floor(pathwidth / 2) + 1 || relPos.getX() == getWidth() - Math.ceil(pathwidth / 2) - 1)
                    && !(relPos.getZ() <= Math.floor(pathwidth / 2) || relPos.getZ() >= getLength() - Math.ceil(pathwidth / 2))) {
                return wall;
            } else if ((relPos.getZ() == Math.floor(pathwidth / 2) + 1 || relPos.getZ() == getLength() - Math.ceil(pathwidth / 2) - 1)
                    && !(relPos.getX() <= Math.floor(pathwidth / 2) || relPos.getX() >= getWidth() - Math.ceil(pathwidth / 2))) {
                return wall;
            } else if (relPos.getZ() == Math.floor(pathwidth / 2) && relPos.getX() == Math.floor(pathwidth / 2) + 1) {
                int idX = (int) Math.floor((double) x / getWidth());
                int idZ = (int) Math.floor((double) z / getLength());
                return new SignBlock(68, 2, "ID:" + idX + ";" + idZ);
            }
        } else if (y < roadheight) {
            return filling;
        }

        return Block.AIR;
    }

    @Override
    public int getWidth() {
        return pathwidth + plotsize + 2;
    }

    @Override
    public int getLength() {
        return pathwidth + plotsize + 2;
    }

    /**
     * Gets the path width. This doesn't include the border around plots.
     *
     * @return The path width
     */
    public int getPathWidth() {
        return pathwidth;
    }

    /**
     * Gets the road height.
     *
     * @return The road height
     */
    public int getRoadHeight() {
        return roadheight;
    }

    /**
     * Gets the material of the wall around the plots.
     *
     * @return The material of the wall around the plots
     */
    public Block getWall() {
        return wall;
    }

    /**
     * Gets the material of the wall around protected plots.
     *
     * @return The material of the wall around protected plots
     */
    public Block getProtectedWall() {
        return protectedWall;
    }

    /**
     * Gets the material of the plot's floor.
     *
     * @return The material of the plot's floor
     */
    public Block getPlotFloor() {
        return plotfloor;
    }
}
