package de.craften.plugins.plotplus.worldtypes.schematic;

import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.io.IOException;

public class SchematicPlotWorldScheme extends PlotWorldScheme {
    private final int northOffset;
    private final int eastOffset;
    private final int southOffset;
    private final int westOffset;

    public SchematicPlotWorldScheme(ConfigurationSection config) throws IOException {
        super(new SchematicPlotStructure(new File(Bukkit.getPluginManager().getPlugin("PlotPlus").getDataFolder(), config.getString("schematic"))));

        northOffset = config.getInt("padding.north", 0);
        eastOffset = config.getInt("padding.east", 0);
        southOffset = config.getInt("padding.south", 0);
        westOffset = config.getInt("padding.west", 0);
    }

    @Override
    public int getNorthOffset() {
        return northOffset;
    }

    @Override
    public int getEastOffset() {
        return eastOffset;
    }

    @Override
    public int getSouthOffset() {
        return southOffset;
    }

    @Override
    public int getWestOffset() {
        return westOffset;
    }

    @Override
    public void onLoaded(PlotWorld world) {

    }
}
