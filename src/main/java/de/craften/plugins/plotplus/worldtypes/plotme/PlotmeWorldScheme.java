package de.craften.plugins.plotplus.worldtypes.plotme;


import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.WorldUtil;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.util.Vector;

public class PlotmeWorldScheme extends PlotWorldScheme {
    public PlotmeWorldScheme(ConfigurationSection configuration) {
        super(new PlotmePlotStructure(configuration.getConfigurationSection("structure")));
    }

    @Override
    public PointXZ getPlotIdAt(Location location) {
        PointXZ original = super.getPlotIdAt(location);
        return new PointXZ(original.getX() + 1, original.getZ() + 1);
    }

    @Override
    public PlotmePlotStructure getStructure() {
        return (PlotmePlotStructure) super.getStructure();
    }

    @Override
    public int getNorthOffset() {
        return (int) Math.floor(getStructure().getPathWidth() / 2) + 2;
    }

    @Override
    public int getEastOffset() {
        return (int) Math.ceil(getStructure().getPathWidth() / 2) + 1;
    }

    @Override
    public int getSouthOffset() {
        return getEastOffset();
    }

    @Override
    public int getWestOffset() {
        return getNorthOffset();
    }

    @Override
    public RectangularRegion getCoreBuildRegion(int idX, int idZ) {
        return super.getCoreBuildRegion(idX - 1, idZ - 1);
    }

    @Override
    public RectangularRegion getFullPlotRegion(int idX, int idZ) {
        return super.getFullPlotRegion(idX - 1, idZ - 1);
    }

    @Override
    public void onLoaded(PlotWorld world) {
        PlotPlus.getApi().addListener(world, new PlotmeWorldListener());
    }

    @Override
    public Location getTeleportLocation(PlotWorld world, int idX, int idZ) {
        World bukkitWorld = world.getBukkitWorld();
        RectangularRegion cr = getCoreBuildRegion(idX, idZ);
        double x = (cr.getXMin() + cr.getXMax()) / 2;
        double z = cr.getZMin() - 2;
        double y = WorldUtil.topmostSafeLocation(bukkitWorld, x, z);

        return new Location(bukkitWorld, x, y, z).setDirection(new Vector(0, 0, 1));
    }
}
