package de.craften.plugins.plotplus.worldtypes;

/**
 * Interface implemented by a {@link de.craften.plugins.plotplus.generator.PlotStructure } if its plots can be reset
 * by just regenerating chunks (i.e. if a plot is always exactly contained in n*m chunks).
 */
public interface ChunkResettable {
}
