package de.craften.plugins.plotplus.worldtypes.generic;

import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import org.bukkit.configuration.ConfigurationSection;

public class GenericPlotWorldScheme extends PlotWorldScheme {
    private final int northOffset;
    private final int eastOffset;
    private final int southOffset;
    private final int westOffset;

    public GenericPlotWorldScheme(ConfigurationSection config) {
        super(new GenericPlotStructure(config.getInt("size.width", 1), config.getInt("size.length", 1)));

        northOffset = config.getInt("padding.north", 0);
        eastOffset = config.getInt("padding.east", 0);
        southOffset = config.getInt("padding.south", 0);
        westOffset = config.getInt("padding.west", 0);
    }

    public GenericPlotWorldScheme(int width, int length, int paddingNorth, int paddingEast, int paddingSouth, int paddingWest) {
        super(new GenericPlotStructure(width, length));

        northOffset = paddingNorth;
        eastOffset = paddingEast;
        southOffset = paddingSouth;
        westOffset = paddingWest;
    }

    @Override
    public int getNorthOffset() {
        return northOffset;
    }

    @Override
    public int getEastOffset() {
        return eastOffset;
    }

    @Override
    public int getSouthOffset() {
        return southOffset;
    }

    @Override
    public int getWestOffset() {
        return westOffset;
    }

    @Override
    public void onLoaded(PlotWorld world) {

    }
}
