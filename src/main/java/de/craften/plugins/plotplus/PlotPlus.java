package de.craften.plugins.plotplus;

import de.craften.plugins.bkcommandapi.Command;
import de.craften.plugins.bkcommandapi.SubCommandHandler;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.addons.dynmap.DynmapAddon;
import de.craften.plugins.plotplus.addons.jsonapi.JsonApiAddon;
import de.craften.plugins.plotplus.api.Addon;
import de.craften.plugins.plotplus.api.PlotPlusApi;
import de.craften.plugins.plotplus.commands.*;
import de.craften.plugins.plotplus.economy.EconomyManager;
import de.craften.plugins.plotplus.economy.PlaceboEconomyManager;
import de.craften.plugins.plotplus.economy.VaultEconomyManager;
import de.craften.plugins.plotplus.generator.PlotWorldGenerator;
import de.craften.plugins.plotplus.gui.PlotPlusGui;
import de.craften.plugins.plotplus.listeners.PlotBlockedPlayersListener;
import de.craften.plugins.plotplus.listeners.PlotProtectionListener;
import de.craften.plugins.plotplus.listeners.PlotVisitListener;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.PlotPersistence;
import de.craften.plugins.plotplus.persistence.sql.MySqlPersistence;
import de.craften.plugins.plotplus.persistence.sql.SqlitePersistence;
import de.craften.plugins.plotplus.persistence.util.PlotmeDatabaseImporter;
import de.craften.plugins.plotplus.plot.PlotManager;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldConfiguration;
import de.craften.plugins.plotplus.plot.world.PlotWorldContainer;
import de.craften.plugins.plotplus.worldtypes.generic.GenericPlotWorldScheme;
import de.craften.plugins.plotplus.worldtypes.plotme.PlotmeWorldScheme;
import de.craften.plugins.plotplus.worldtypes.schematic.SchematicPlotWorldScheme;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Main class of the PlotPlus plugin.
 *
 * @author leMaik
 */
public class PlotPlus extends JavaPlugin implements PlotWorldContainer {
    private static final ApiImpl api = new ApiImpl();
    private Map<String, PlotWorld> worlds;
    private PlotManager manager;
    private EconomyManager economy;
    private PlotPlusGui gui;

    public static TextBuilder prefix() {
        return TextBuilder.create("[").darkGray().append("PlotPlus").darkGreen().append("] ").darkGray();
    }

    /**
     * Gets the API.
     * <strong>Do not save this instance!</strong>
     *
     * @return PlotPlus API
     */
    public static PlotPlusApi getApi() {
        return api;
    }

    @Override
    public PlotWorld getWorld(World world) {
        if (worlds == null) {
            loadWorlds();
        }
        return worlds.get(world.getName());
    }

    @Override
    public PlotWorld getWorld(String worldName) {
        if (worlds == null) {
            loadWorlds();
        }
        return worlds.get(worldName);
    }

    /**
     * Gets all plot worlds.
     *
     * @return All plot worlds
     */
    public Collection<PlotWorld> getWorlds() {
        if (worlds == null) {
            loadWorlds();
        }
        return worlds.values();
    }

    /**
     * Gets the plot manager.
     *
     * @return The plot manager
     */
    public PlotManager getManager() {
        return manager;
    }

    @Override
    public void onEnable() {
        api.setPlugin(this);

        saveDefaultConfig();
        if (setupEconomy())
            getLogger().info("Vault found.");
        else
            getLogger().info("No Vault found.");

        if (getConfig().getBoolean("enablePlotVisitListeners", true)) {
            getServer().getPluginManager().registerEvents(new PlotBlockedPlayersListener(this), this);
            getServer().getPluginManager().registerEvents(new PlotVisitListener(this), this);
        } else {
            getLogger().warning("Plot visit listeners disabled. Performance might be better but " +
                    "blocking players and welcome messages won't work.");
        }
        getServer().getPluginManager().registerEvents(new PlotProtectionListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerNameChangeListener(), this);

        try {
            final PlotPersistence persistence;
            if (getConfig().getString("database.type").equals("mysql")) {
                persistence = new MySqlPersistence(
                        getConfig().getString("database.host"),
                        getConfig().getInt("database.port", 3306),
                        getConfig().getString("database.dbName"),
                        getConfig().getString("database.user"),
                        getConfig().getString("database.password"),
                        getConfig().getString("database.prefix", ""));
            } else {
                persistence = new SqlitePersistence(new File(getDataFolder(), "plots.sql"));
            }

            if (getConfig().getBoolean("importPlotme.enabled", false)) {
                getServer().getScheduler().runTaskLater(this, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            PlotmeDatabaseImporter importer = new PlotmeDatabaseImporter(
                                    persistence,
                                    getConfig().getString("importPlotme.host"),
                                    getConfig().getInt("importPlotme.port", 3306),
                                    getConfig().getString("importPlotme.dbName"),
                                    getConfig().getString("importPlotme.user"),
                                    getConfig().getString("importPlotme.password")
                            );
                            importer.convert(getLogger());
                        } catch (SQLException | ClassNotFoundException e) {
                            getLogger().severe("Could not import plotme data.");
                        }
                        getConfig().set("importPlotme.enabled", false);
                        saveConfig();
                    }
                }, 100);
            }

            manager = new PlotManager(persistence, this);
        } catch (Exception e) {
            getLogger().severe("Could not initialize persistence!");
            e.printStackTrace();
            if (e instanceof NullPointerException) {
                getLogger().severe("This error occurs when using 'reload' after updating PlotPlus. RESTART THE SERVER TO FIX THIS!");
            }
        }

        //Register included schemes
        getApi().registerWorldScheme("schematic", SchematicPlotWorldScheme.class);
        getApi().registerWorldScheme("generic", GenericPlotWorldScheme.class);
        getApi().registerWorldScheme("plotme", PlotmeWorldScheme.class);

        //TODO Seperate these two add-ons into two plugins that depend on dynmap/jsonapi.
        try {
            if (getConfig().getBoolean("enableDynmapLayer"))
                getApi().registerAddon(new DynmapAddon());
        } catch (NoClassDefFoundError e) {
            getLogger().warning("Dynmap add-on is enabled but dynmap is not installed");
        }

        try {
            if (getConfig().getBoolean("enableJsonApi"))
                getApi().registerAddon(new JsonApiAddon());
        } catch (NoClassDefFoundError e) {
            getLogger().warning("jsonapi add-on is enabled but jsonapi is not installed");
        }

        this.gui = new PlotPlusGui(this);

        for (Addon addon : api.getAddons())
            addon.enable(this);

        getCommand("plotplus").setExecutor(new PlotPlusCommandHandler(this));
    }

    @Override
    public void onDisable() {
        getGui().closeAll();

        for (Addon addon : api.getAddons())
            addon.disable();
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        if (worlds == null) {
            loadWorlds();
        }
        if (!worlds.containsKey(worldName)) {
            getLogger().warning(worldName + " is not configured as plot world");
        }
        return new PlotWorldGenerator();
    }

    public PlotPlusGui getGui() {
        return gui;
    }

    /**
     * Initializes the economy instance.
     *
     * @return True if the server has an economy plugin, false if not
     */

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            getLogger().info("Vault not enabled");
        } else {
            try {
                RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
                if (economyProvider != null) {
                    economy = new VaultEconomyManager(economyProvider.getProvider());
                    return true;
                }
            } catch (NoClassDefFoundError ignore) {
                ignore.printStackTrace();
            }
        }
        getLogger().info("Economy is disabled");
        economy = new PlaceboEconomyManager();
        return false;
    }

    private void loadWorlds() {
        worlds = new HashMap<>();
        ConfigurationSection configuredWorlds = getConfig().getConfigurationSection("worlds");
        for (String worldName : configuredWorlds.getKeys(false)) {
            ConfigurationSection worldConfig = configuredWorlds.getConfigurationSection(worldName);
            try {
                PlotWorld world = new PlotWorld(worldName,
                        new PlotWorldConfiguration(api.getWorldScheme(worldConfig.getString("type"), worldConfig), worldConfig),
                        manager, api.getListenerHub());
                world.getScheme().onLoaded(world);
                worlds.put(worldName, world);
            } catch (IOException e) {
                getLogger().warning("Could not setup plot world " + worldName + "!");
            }
        }

        try {
            manager.initialize();
        } catch (PersistenceException e) {
            getLogger().warning("Initializing the plot manager failed");
            e.printStackTrace();
        }
    }

    /**
     * Gets the economy of this server.
     *
     * @return Economy, never null
     */
    public EconomyManager getEconomy() {
        return economy;
    }

    private class PlayerNameChangeListener implements Listener {
        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent event) {
            try {
                if (manager.getPlayerCache().updatePlayer(event.getPlayer())) {
                    getLogger().info(event.getPlayer().getName() + " changed the nickname");
                    for (PlotWorld world : getWorlds())
                        world.onPlayerNameChanged(manager.getPlayerCache().getCached(event.getPlayer()));
                }
            } catch (PersistenceException e) {
                getLogger().warning("Failed to save new nickname of " + event.getPlayer().getName());
            }
        }
    }

    private static class PlotPlusCommandHandler extends SubCommandHandler {
        public PlotPlusCommandHandler(PlotPlus plugin) {
            super("plotplus");

            addHandlers(
                    new ClaimCommands(plugin),
                    new TeleportCommands(plugin),
                    new ManagementCommands(plugin),
                    new AdminCommands(plugin),
                    new InformationCommands(plugin),
                    new SelectionCommands(plugin));
        }

        @Override
        public void onInvalidCommand(CommandSender sender) {
            if (sender instanceof ConsoleCommandSender)
                sender.sendMessage("Unknown command. Type \"plotplus help\" for help.");
            else
                sender.sendMessage("Unknown command. Type \"/plotplus help\" for help.");
        }

        @Override
        protected void onPermissionDenied(CommandSender sender, org.bukkit.command.Command command, String[] args) {
            TextBuilder.create("You don't have permission to use that command.").red().sendTo(sender);
        }

        @Override
        protected void sendHelpLine(CommandSender sender, Command command) {
            String mainCmd = sender instanceof Player ? "/plotplus " : "plotplus ";
            TextBuilder.create(mainCmd + command.value()[0]).darkGreen().append(" " + command.description()).gray().sendTo(sender);
        }

        @Override
        protected void sendUsageHelp(CommandSender sender, Command command) {
            String mainCmd = sender instanceof Player ? "/plotplus " : "plotplus ";
            TextBuilder.create(mainCmd + command.value()[0]).darkGreen().sendTo(sender);

            if (!command.description().isEmpty())
                TextBuilder.create(command.description()).gray().sendTo(sender);
            if (command.usage().length > 0) {
                for (String usage : command.usage())
                    TextBuilder.create("* ").darkGray().append(mainCmd + usage).gray().sendTo(sender);
            } else {
                TextBuilder.create("* ").darkGray().append(mainCmd + command.value()[0]).gray().sendTo(sender);
            }
        }
    }
}
