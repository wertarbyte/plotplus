package de.craften.plugins.plotplus;

import de.craften.plugins.plotplus.api.Addon;
import de.craften.plugins.plotplus.api.ApiHub;
import de.craften.plugins.plotplus.api.PlotListener;
import de.craften.plugins.plotplus.api.PlotPlusApi;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;
import de.craften.plugins.plotplus.economy.EconomyManager;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of the PlotPlus API.
 */
class ApiImpl implements PlotPlusApi {
    private PlotPlus plugin;
    private static Map<String, Addon> addons = new HashMap<>();
    private Map<String, Class<? extends PlotWorldScheme>> worldSchemes = new HashMap<String, Class<? extends PlotWorldScheme>>();
    private ApiHub listenerHub = new ApiHub();

    /**
     * Gets all add-ons.
     *
     * @return all add-ons
     */
    public Collection<Addon> getAddons() {
        return addons.values();
    }

    @Override
    public void registerAddon(Addon addon) {
        Addon previous = addons.get(addon.getName());
        if (previous != null) {
            if (previous.isEnabled())
                previous.disable();
        }

        addons.put(addon.getName(), addon);
        if (plugin != null && plugin.isEnabled())
            addon.enable(plugin);
    }

    @Override
    public boolean unregisterAddon(String name) {
        Addon removed = addons.remove(name);
        if (removed != null) {
            if (removed.isEnabled())
                removed.disable();
            return true;
        }
        return false;
    }

    @Override
    public void registerWorldScheme(String name, Class<? extends PlotWorldScheme> scheme) {
        worldSchemes.put(name.toLowerCase(), scheme);
    }

    @Override
    public EconomyManager getEconomy() {
        return plugin.getEconomy();
    }

    @Override
    public PlotWorld getWorld(World world) {
        return plugin.getWorld(world);
    }

    @Override
    public PlotWorld getWorld(String world) {
        return plugin.getWorld(world);
    }

    /**
     * Gets a world scheme.
     *
     * @param name   name of the world scheme (case insensitive)
     * @param config configuration for that scheme
     * @return new instance of the world scheme
     */
    public PlotWorldScheme getWorldScheme(String name, ConfigurationSection config) {
        try {
            return worldSchemes.get(name.toLowerCase()).getDeclaredConstructor(ConfigurationSection.class).newInstance(config);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setPlugin(PlotPlus plugin) {
        this.plugin = plugin;
    }

    @Override
    public void addListener(PlotListener listener) {
        listenerHub.addListener(listener);
    }

    @Override
    public void addListener(PlotWorld world, PlotListener listener) {
        listenerHub.addListener(new SingleWorldListener(world.getName(), listener));
    }

    @Override
    public void removeListener(PlotListener listener) {
        listenerHub.removeListener(listener);
    }

    public PlotListener getListenerHub() {
        return listenerHub;
    }

    /**
     * A listener that forwards events of one world.
     */
    private class SingleWorldListener implements PlotListener {
        private final PlotListener listener;
        private final String worldName;

        public SingleWorldListener(String worldName, PlotListener listener) {
            this.listener = listener;
            this.worldName = worldName;
        }

        @Override
        public void onWorldLoaded(PlotWorld world) {
            if (world.getName().equals(worldName)) {
                listener.onWorldLoaded(world);
            }
        }

        @Override
        public void onPlotClaiming(PlotClaimEvent event) {
            if (event.getWorld().getName().equals(worldName)) {
                listener.onPlotClaiming(event);
            }
        }

        @Override
        public void onPlotClaimed(Plot plot) {
            if (plot.getWorld().getName().equals(worldName)) {
                listener.onPlotClaimed(plot);
            }
        }

        @Override
        public void onPlotRemoved(Plot plot) {
            if (plot.getWorld().getName().equals(worldName)) {
                listener.onPlotRemoved(plot);
            }
        }

        @Override
        public void onBeforePlotCleared(Plot plot) {
            if (plot.getWorld().getName().equals(worldName)) {
                listener.onBeforePlotCleared(plot);
            }
        }

        @Override
        public void onPlotCleared(Plot plot) {
            if (plot.getWorld().getName().equals(worldName)) {
                listener.onPlotCleared(plot);
            }
        }

        @Override
        public void onPlotProtectionChanged(Plot plot, boolean isProtected) {
            if (plot.getWorld().getName().equals(worldName)) {
                listener.onPlotProtectionChanged(plot, isProtected);
            }
        }

        @Override
        public void onPlotOwnerChanged(Plot plot, SimplePlayer oldOwner) {
            if (plot.getWorld().getName().equals(worldName)) {
                listener.onPlotOwnerChanged(plot, oldOwner);
            }
        }

        @Override
        public void onPlotConnectionChanged(Plot a, Plot b, boolean linkedNow) {
            if (a.getWorld().getName().equals(worldName)) {
                listener.onPlotConnectionChanged(a, b, linkedNow);
            }
        }

        @Override
        public void onPlayerNameChanged(SimplePlayer player, Collection<Plot> plots) {
            listener.onPlayerNameChanged(player, plots);
        }
    }
}
