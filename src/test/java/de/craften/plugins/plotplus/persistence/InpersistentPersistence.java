package de.craften.plugins.plotplus.persistence;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.entities.PlotConnection;
import de.craften.plugins.plotplus.persistence.entities.PlotData;
import de.craften.plugins.plotplus.persistence.entities.PlotId;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class InpersistentPersistence implements PlotPersistence {
    private List<PlotData> plots = new ArrayList<>();
    private List<PlotConnection> plotConnections = new ArrayList<>();

    @Override
    public void setup() throws PersistenceException {

    }

    @Override
    public List<PlotData> getPlots() throws PersistenceException {
        return plots;
    }

    @Override
    public void createPlot(PlotData plot) {
        plots.add(plot);
    }

    @Override
    public void createPlots(List<PlotData> plots) throws PersistenceException {

    }

    @Override
    public void removePlot(PlotId plot) {
        plots.remove(plot);
    }

    @Override
    public void addHelper(PlotId plot, SimplePlayer helper) {

    }

    @Override
    public void removeHelper(PlotId plot, UUID helper) {

    }

    @Override
    public void addBlockedPlayer(PlotId plot, SimplePlayer blocked) {

    }

    @Override
    public void removeBlockedPlayer(PlotId plot, UUID blocked) {

    }

    @Override
    public void setProtected(PlotId plot, boolean isProtected) throws PersistenceException {

    }

    @Override
    public void setOwner(PlotId plot, SimplePlayer owner) throws PersistenceException {

    }

    @Override
    public void setName(PlotId plot, String name) throws PersistenceException {

    }

    @Override
    public void setWelcomeMessageEnabled(PlotId plot, boolean isEnabled) throws PersistenceException {

    }

    @Override
    public void setProjectileProtectionEnabled(PlotId plot, boolean isEnabled) throws PersistenceException {

    }

    @Override
    public void setHitProtectionEnabled(PlotId plot, boolean isEnabled) throws PersistenceException {

    }

    @Override
    public Collection<PlotConnection> getPlotConnections() throws PersistenceException {
        return plotConnections;
    }

    @Override
    public void createPlotConnection(PlotId a, PlotId b) throws PersistenceException {
        plotConnections.add(new PlotConnection(a, b));
    }

    @Override
    public void deletePlotConnections(PlotId a, PlotId b) throws PersistenceException {
        plotConnections.remove(new PlotConnection(a, b));
    }

    @Override
    public void deletePlotConnections(PlotId plot) throws PersistenceException {
        for (PlotConnection connection : new ArrayList<>(plotConnections)) {
            if (connection.getA().equals(plot) || connection.getB().equals(plot)) {
                plotConnections.remove(connection);
            }
        }
    }

    @Override
    public void savePlayer(SimplePlayer player) throws PersistenceException {

    }
}
