package de.craften.plugins.plotplus.persistence.sql;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotData;
import de.craften.plugins.plotplus.plot.PlotManager;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldConfiguration;
import de.craften.plugins.plotplus.testhelpers.SimpleWorldContainer;
import de.craften.plugins.plotplus.worldtypes.generic.GenericPlotWorldScheme;
import org.bukkit.block.Biome;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class SqlitePersistenceTest {
    private SimpleWorldContainer worlds;
    private SqlitePersistence persistence;
    private PlotManager manager;

    private static class MemorySqlitePersistence extends SqlitePersistence {
        protected MemorySqlitePersistence() throws PersistenceException {
            super(":memory:");
        }
    }

    @Before
    public void setUp() throws Exception {
        persistence = new MemorySqlitePersistence();
        persistence.setup();
        worlds = new SimpleWorldContainer();
        manager = new PlotManager(persistence, worlds);
        worlds.addWorld(new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager));
        worlds.addWorld(new PlotWorld("world2", new PlotWorldConfiguration(new GenericPlotWorldScheme(2, 4, 9, 10, 7, 1), 0, 0, 5, Biome.PLAINS, false), manager));
    }

    @Test
    public void testAddAndGetPlot() throws Exception {
        List<PlotData> plots = persistence.getPlots();
        assertEquals("No plots should be in database before adding any", 0, plots.size());
        SimplePlayer owner = new SimplePlayer(UUID.randomUUID(), "randomName");
        persistence.savePlayer(owner); //PlotManager would do this
        PlotData data1 = new PlotData(1, 2, "world", owner);
        persistence.createPlot(data1);

        plots = persistence.getPlots();
        assertTrue("database should now contain the first added plot", plots.contains(data1));
    }

    @Test
    public void testRemovePlot() throws Exception {
        SimplePlayer player1 = new SimplePlayer(UUID.randomUUID(), "randomName");
        SimplePlayer player2 = new SimplePlayer(UUID.randomUUID(), "badBoy");
        SimplePlayer player3 = new SimplePlayer(UUID.randomUUID(), "helpingGuy");

        //PlotManager would do this
        persistence.savePlayer(player1);
        persistence.savePlayer(player2);
        persistence.savePlayer(player3);

        PlotData data1 = new PlotData(1, 2, "world", player1);
        persistence.createPlot(data1);
        persistence.addBlockedPlayer(data1.getId(), player2);
        persistence.addHelper(data1.getId(), player3);
        persistence.removePlot(data1.getId());
        assertTrue("Plot should be removed", !persistence.getPlots().contains(data1));

        persistence.createPlot(data1);
        PlotData newData = persistence.getPlots().get(0);
        assertTrue("Helpers should have been removed", newData.getHelpers().isEmpty());
        assertTrue("Blocked players should have been removed", newData.getBlockedPlayers().isEmpty());
    }

    @Test
    public void testAddHelper() throws Exception {
        PlotData data = new PlotData(1, 2, "world", new SimplePlayer(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);
        SimplePlayer helper = new SimplePlayer(UUID.randomUUID(), "helpingGuy");
        persistence.savePlayer(helper);//PlotManager would do that
        persistence.addHelper(data.getId(), helper);

        assertTrue("Helper should be added", persistence.getPlots().get(0).getHelpers().contains(helper));
    }

    @Test
    public void testRemoveHelper() throws Exception {
        PlotData data = new PlotData(1, 2, "world", new SimplePlayer(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);

        SimplePlayer helper = new SimplePlayer(UUID.randomUUID(), "helpingGuy");
        persistence.savePlayer(helper);//PlotManager would do that
        persistence.addHelper(data.getId(), helper);
        persistence.addHelper(data.getId(), new SimplePlayer(UUID.randomUUID(), "another"));
        persistence.addHelper(data.getId(), new SimplePlayer(UUID.randomUUID(), "third"));

        persistence.removeHelper(data.getId(), helper.getUniqueId());
        assertFalse("Helper should be removed", persistence.getPlots().get(0).getHelpers().contains(helper));
        assertFalse("No other helpers should be removed", persistence.getPlots().get(0).getHelpers().isEmpty());
    }

    @Test
    public void testAddBlockedPlayer() throws Exception {
        PlotData data = new PlotData(1, 2, "world", new SimplePlayer(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);
        SimplePlayer blocked = new SimplePlayer(UUID.randomUUID(), "who");
        persistence.savePlayer(blocked);//PlotManager would do that
        persistence.addBlockedPlayer(data.getId(), blocked);
        persistence.addBlockedPlayer(data.getId(), new SimplePlayer(UUID.randomUUID(), "badboy2"));
        persistence.addBlockedPlayer(data.getId(), new SimplePlayer(UUID.randomUUID(), "badboy3"));

        assertTrue("Blocked player should be added", persistence.getPlots().get(0).getBlockedPlayers().contains(blocked));
    }

    @Test
    public void testRemoveBlockedPlayer() throws Exception {
        PlotData data = new PlotData(1, 2, "world", manager.getPlayerCache().getCached(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);
        SimplePlayer blocked = new SimplePlayer(UUID.randomUUID(), "who");
        persistence.savePlayer(blocked);//PlotManager would do that
        persistence.addBlockedPlayer(data.getId(), blocked);
        persistence.addBlockedPlayer(data.getId(), new SimplePlayer(UUID.randomUUID(), "badboy2"));
        persistence.addBlockedPlayer(data.getId(), new SimplePlayer(UUID.randomUUID(), "badboy3"));
        persistence.removeBlockedPlayer(data.getId(), blocked.getUniqueId());

        assertFalse("Blocked player should be removed", persistence.getPlots().get(0).getBlockedPlayers().contains(blocked));
        assertFalse("No other blocked players should be removed", persistence.getPlots().get(0).getBlockedPlayers().isEmpty());
    }

    @Test
    public void testSetProtected() throws Exception {
        PlotData data = new PlotData(1, 2, "world", new SimplePlayer(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);

        persistence.setProtected(data.getId(), true);
        assertTrue(persistence.getPlots().get(0).isProtected());

        persistence.setProtected(data.getId(), false);
        assertFalse(persistence.getPlots().get(0).isProtected());
    }

    @Test
    public void testSetOwner() throws Exception {
        PlotData data = new PlotData(1, 2, "world", new SimplePlayer(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);

        SimplePlayer newOwner = manager.getPlayerCache().getCached(UUID.randomUUID(), "newName");

        persistence.setOwner(data.getId(), newOwner);
        assertEquals("Owner should be set to the new owner", newOwner, persistence.getPlots().get(0).getOwner());
    }

    @Test
    public void testSetName() throws Exception {
        PlotData data = new PlotData(1, 2, "world", new SimplePlayer(UUID.randomUUID(), "randomName"));
        persistence.createPlot(data);

        persistence.setName(data.getId(), "aNewName");
        assertEquals("aNewName", persistence.getPlots().get(0).getName());
    }

    @Test
    public void testSavePlayer() throws Exception {
        SimplePlayer player = new SimplePlayer(UUID.randomUUID(), "randomName");
        persistence.savePlayer(player);
        PlotData data = new PlotData(1, 2, "world", player);
        persistence.createPlot(data);
        assertEquals("randomName", persistence.getPlots().get(0).getOwner().getDisplayName());

        player = new SimplePlayer(player.getUniqueId(), "anotherName");
        persistence.savePlayer(player);
        assertEquals("anotherName", persistence.getPlots().get(0).getOwner().getDisplayName());
    }
}