package de.craften.plugins.plotplus.generator.blocks;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemTest {

    @Test
    public void testConstructionFromString() throws Exception {
        Item item = new Item("42:21");
        assertEquals("ID should be the number before the colon", 42, item.getId());
        assertEquals("Data should be the number after the colon", 21, item.getData());

        item = new Item("42");
        assertEquals("ID should be the number", 42, item.getId());
        assertEquals("Data should default to zero", 0, item.getData());
    }

    @Test
    public void testGetId() throws Exception {
        Item item = new Item(36, 0);
        assertEquals("ID should be as specified", 36, item.getId());
    }

    @Test
    public void testGetData() throws Exception {
        Item item = new Item(40, 3);
        assertEquals("Data should be as specified", 3, item.getData());
    }
}