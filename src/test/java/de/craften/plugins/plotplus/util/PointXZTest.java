package de.craften.plugins.plotplus.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class PointXZTest {
    @Test
    public void testCopyConstructor() {
        PointXZ p = new PointXZ(19, 94);
        assertEquals("Copied point should equal original point", p, new PointXZ(p));
    }

    @Test
    public void testGetX() throws Exception {
        assertEquals("X should be what we set it to", 42, new PointXZ(42, 21).getX());
    }

    @Test
    public void testGetZ() throws Exception {
        assertEquals("Z should be what we set it to", 21, new PointXZ(42, 21).getZ());
    }

    @Test
    public void testHashCode() throws Exception {
        PointXZ a = new PointXZ(64, 12);
        PointXZ b = new PointXZ(64, 12);

        assertTrue("Equal points should have the same hash", a.hashCode() == b.hashCode());
    }

    @Test
    public void testEquals() throws Exception {
        PointXZ a = new PointXZ(3, 9);
        PointXZ b = new PointXZ(3, 9);
        PointXZ c = new PointXZ(7, 98);

        assertTrue("Equal points should be equal", a.equals(b));
        assertFalse("Unequal points should not be equal", a.equals(c));
    }
}