package de.craften.plugins.plotplus.util.region;

import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Location;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class RectangularRegionTest {
    @Test
    public void testByStartAndSize() throws Exception {
        RectangularRegion region = RectangularRegion.byStartAndSize(3, 8, 5, 4);
        assertEquals(3, region.getXMin());
        assertEquals(7, region.getXMax());
        assertEquals(8, region.getZMin());
        assertEquals(11, region.getZMax());
    }

    @Test
    public void testContains() throws Exception {
        RectangularRegion region = new RectangularRegion(0, 0, 2, 2);
        assertTrue(region.contains(1, 1));
        assertTrue(region.contains(2, 2));
        assertFalse(region.contains(-1, 0));
    }

    @Test
    public void testContainsLocation() throws Exception {
        RectangularRegion region = new RectangularRegion(-2, 1, 2, 2);

        assertTrue(region.contains(new Location(null, 1.4, 32, 2.0)));
        assertFalse(region.contains(new Location(null, 0, 2.1, 3.14)));
    }

    @Test
    public void testFindEdgesRect() throws Exception {
        RectangularRegion rectRegion = new RectangularRegion(0, 0, 4, 8);
        List<PointXZ> edges = rectRegion.getEdges();

        assertEquals("Rectangular region should have four edges, +first edge twice", 4 + 1, edges.size());
        assertTrue(edges.contains(new PointXZ(0, 0)));
        assertTrue(edges.contains(new PointXZ(4, 0)));
        assertTrue(edges.contains(new PointXZ(4, 8)));
        assertTrue(edges.contains(new PointXZ(0, 8)));

        assertTrue("First edge should equal last edge", edges.get(0).equals(edges.get(edges.size() - 1)));
    }

    @Test
    public void testIterator() throws Exception {
        RectangularRegion region = RectangularRegion.byStartAndSize(0, 0, 3, 4); //12 fields
        Set<PointXZ> iteratedPoints = new HashSet<>();

        for (PointXZ p : region)
            iteratedPoints.add(p);

        assertEquals("Should iterate once over all fields", 3 * 4, iteratedPoints.size());
    }

    @Test
    public void testEquals() throws Exception {
        RectangularRegion a = RectangularRegion.byStartAndSize(5, 6, 12, 17);
        RectangularRegion b = RectangularRegion.byStartAndSize(5, 6, 12, 17);
        RectangularRegion c = RectangularRegion.byStartAndSize(8, 1, 4, 59);

        assertEquals("Equal regions should be equal", a, b);
        assertNotEquals("Not equal regions should not be equal", a, c);
    }

    @Test
    public void testHashCode() throws Exception {
        RectangularRegion a = RectangularRegion.byStartAndSize(5, 6, 12, 17);
        RectangularRegion b = RectangularRegion.byStartAndSize(5, 6, 12, 17);

        assertEquals("hashCode of equal regions should be equal", a.hashCode(), b.hashCode());
    }
}