package de.craften.plugins.plotplus.util.region;

import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Location;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class CombinedRegionTest {
    @Test
    public void testAdd() throws Exception {
        CombinedRegion region = new CombinedRegion();
        region.add(new RectangularRegion(0, 0, 2, 4));

        assertTrue("Region should contain point in region that we just added", region.contains(1, 2));
    }

    @Test
    public void testContains() throws Exception {
        CombinedRegion region = new CombinedRegion();
        region.add(RectangularRegion.byStartAndSize(0, 0, 2, 2));
        region.add(RectangularRegion.byStartAndSize(-5, -5, 2, 2));

        assertTrue(region.contains(1, 1));
        assertTrue(region.contains(-4, -5));
        assertFalse(region.contains(-1, -2));
    }

    @Test
    public void testContainsLocation() throws Exception {
        CombinedRegion region = new CombinedRegion();
        region.add(RectangularRegion.byStartAndSize(0, 0, 2, 2));
        region.add(RectangularRegion.byStartAndSize(-5, -5, 3, 3));

        assertTrue(region.contains(new Location(null, 0.23, 32, 0.6)));
        assertTrue(region.contains(new Location(null, -3.9, 0, -3)));
        assertFalse(region.contains(new Location(null, -1.4, 32, -2.3)));
    }

    @Test
    public void testIterator() throws Exception {
        CombinedRegion region = new CombinedRegion();
        region.add(RectangularRegion.byStartAndSize(0, 0, 1, 4)); //4 fields
        region.add(RectangularRegion.byStartAndSize(-5, -5, 8, 2)); //16 fields
        region.add(RectangularRegion.byStartAndSize(0, 0, 1, 8)); //8 fields (4 already in first region)

        Set<PointXZ> iteratedPoints = new HashSet<>();

        for (PointXZ p : region)
            iteratedPoints.add(p);

        assertTrue("Should iterate at least once over all fields", iteratedPoints.size() >= 24);
    }

    @Test
    public void testEquals() throws Exception {
        RectangularRegion region1 = RectangularRegion.byStartAndSize(5, 6, 12, 17);
        RectangularRegion region2 = RectangularRegion.byStartAndSize(2, 9, 12, 17);
        RectangularRegion region3 = RectangularRegion.byStartAndSize(8, 1, 4, 59);

        CombinedRegion a = new CombinedRegion();
        a.add(region1);
        a.add(region2);

        CombinedRegion b = new CombinedRegion();
        b.add(region2);
        b.add(region1);

        CombinedRegion c = new CombinedRegion();
        c.add(region2);
        c.add(region3);

        assertEquals("Equal regions should be equal", a, b);
        assertNotEquals("Not equal regions should not be equal", a, c);
    }

    @Test
    public void testHashCode() throws Exception {
        RectangularRegion region1 = RectangularRegion.byStartAndSize(5, 6, 12, 17);
        RectangularRegion region2 = RectangularRegion.byStartAndSize(8, 1, 4, 59);

        CombinedRegion a = new CombinedRegion();
        a.add(region1);
        a.add(region2);

        CombinedRegion b = new CombinedRegion();
        b.add(region2);
        b.add(region1);

        assertEquals("hashCode of equal regions should be equal", a.hashCode(), b.hashCode());
    }
}