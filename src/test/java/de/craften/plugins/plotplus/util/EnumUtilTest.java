package de.craften.plugins.plotplus.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EnumUtilTest {

    @Test
    public void testGetOrDefault() throws Exception {
        assertEquals("Return enum value if it is found",
                TestEnum.HI, EnumUtil.getOrDefault(TestEnum.class, "HI", TestEnum.HOW_ARE_YOU));

        assertEquals("Return fallback value if enum value is not found",
                TestEnum.I_AM_FINE, EnumUtil.getOrDefault(TestEnum.class, "UNKNOWN_VALUE", TestEnum.I_AM_FINE));
    }

    private static enum TestEnum {
        HI,
        HOW_ARE_YOU,
        I_AM_FINE
    }
}