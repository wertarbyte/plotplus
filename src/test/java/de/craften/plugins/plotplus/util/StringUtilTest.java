package de.craften.plugins.plotplus.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringUtilTest {
    @Test
    public void testJoin() throws Exception {
        assertEquals("a,b,c", StringUtil.join(new String[]{"a", "b", "c"}, ","));
        assertEquals("", StringUtil.join(new String[]{}, ","));
        assertEquals("works", StringUtil.join(new String[]{"wo", "rk", "s"}, ""));
    }
}