package de.craften.plugins.plotplus.util.region;

import de.craften.plugins.plotplus.util.PointXZ;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class EdgeFinderTest {
    @Test
    public void testFindEdgesRect() throws Exception {
        Region rectRegion = new RectangularRegion(0, 0, 4, 8);
        List<PointXZ> edges = EdgeFinder.findEdges(rectRegion);

        assertEquals(4 + 1, edges.size());
        assertTrue(edges.contains(new PointXZ(0, 0)));
        assertTrue(edges.contains(new PointXZ(4, 0)));
        assertTrue(edges.contains(new PointXZ(4, 8)));
        assertTrue(edges.contains(new PointXZ(0, 8)));

        assertTrue("First edge should equal last edge", edges.get(0).equals(edges.get(edges.size() - 1)));
    }

    @Test
    public void testFindEdges() throws Exception {
        CombinedRegion region = new CombinedRegion();
        region.add(new RectangularRegion(0, 0, 4, 2));
        region.add(new RectangularRegion(0, 2, 8, 7));
        List<PointXZ> edges = EdgeFinder.findEdges(region);

        assertEquals(6 + 1, edges.size());
        assertTrue(edges.contains(new PointXZ(0, 0)));
        assertTrue(edges.contains(new PointXZ(4, 0)));
        assertTrue(edges.contains(new PointXZ(4, 2)));
        assertTrue(edges.contains(new PointXZ(8, 2)));
        assertTrue(edges.contains(new PointXZ(8, 7)));
        assertTrue(edges.contains(new PointXZ(0, 7)));

        assertTrue("First edge should equal last edge", edges.get(0).equals(edges.get(edges.size() - 1)));
    }
}