package de.craften.plugins.plotplus.testhelpers;


import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldContainer;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;

public class SimpleWorldContainer implements PlotWorldContainer {
    private Map<String, PlotWorld> worlds = new HashMap<>();

    @Override
    public PlotWorld getWorld(World world) {
        return worlds.get(world.getName());
    }

    @Override
    public PlotWorld getWorld(String name) {
        return worlds.get(name);
    }

    public void addWorld(PlotWorld world) {
        worlds.put(world.getName(), world);
    }
}
