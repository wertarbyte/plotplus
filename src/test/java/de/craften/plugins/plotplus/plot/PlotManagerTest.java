package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.InpersistentPersistence;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldConfiguration;
import de.craften.plugins.plotplus.testhelpers.SimpleWorldContainer;
import de.craften.plugins.plotplus.worldtypes.generic.GenericPlotWorldScheme;
import org.bukkit.block.Biome;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class PlotManagerTest {
    private PlotManager manager;
    private PlotWorld world;
    private SimpleWorldContainer worlds;

    @Before
    public void setUp() throws Exception {
        worlds = new SimpleWorldContainer();
        manager = new PlotManager(new InpersistentPersistence(), worlds);
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);
    }

    @Test
    public void testGetPlots() throws Exception {
        assertEquals(0, manager.getPlots(world).size());
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomName");
        Plot plot1 = manager.createPlot(3, 5, owner, world);
        Plot plot2 = manager.createPlot(8, 2, owner, world);
        assertEquals(2, manager.getPlots(world).size());
        assertTrue(manager.getPlots(world).contains(plot1));
        assertTrue(manager.getPlots(world).contains(plot2));
    }

    @Test
    public void testCreatePlot() throws Exception {
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomName");
        Plot plot = manager.createPlot(3, 5, owner, world);

        assertEquals(3, plot.getIdX());
        assertEquals(5, plot.getIdZ());
        assertEquals(owner, plot.getOwner());
    }

    @Test
    public void testGetPlot() throws Exception {
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomName");
        manager.createPlot(3, 5, owner, world);
        Plot plot = manager.getPlot(3, 5, world);

        assertEquals(3, plot.getIdX());
        assertEquals(5, plot.getIdZ());
        assertEquals(owner, plot.getOwner());
    }

    @Test
    public void testIsPlotClaimed() throws Exception {
        manager.createPlot(3, 5, manager.getPlayerCache().getCached(UUID.randomUUID(), "randomName"), world);
        assertTrue("Claimed plot should be claimed", manager.isPlotClaimed(3, 5, world));
        assertFalse("Unclaimed plot shouldn't be claimed", manager.isPlotClaimed(1, -7, world));
    }

    @Test
    public void testRemovePlot() throws Exception {
        Plot p = manager.createPlot(3, 5, manager.getPlayerCache().getCached(UUID.randomUUID(), "randomName"), world);
        manager.removePlot(p);
        assertNull("Removed plot shouldn't exist", manager.getPlot(3, 5, world));
    }
}