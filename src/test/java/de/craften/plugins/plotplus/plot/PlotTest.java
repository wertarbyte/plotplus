package de.craften.plugins.plotplus.plot;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.generator.BlockProvider;
import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.persistence.InpersistentPersistence;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldConfiguration;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import de.craften.plugins.plotplus.testhelpers.SimpleWorldContainer;
import de.craften.plugins.plotplus.util.region.CombinedRegion;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import de.craften.plugins.plotplus.worldtypes.generic.GenericPlotWorldScheme;
import org.bukkit.Location;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class PlotTest {
    private SimpleWorldContainer worlds;
    PlotManager manager;
    private PlotWorld world;

    @Before
    public void setUp() throws Exception {
        worlds = new SimpleWorldContainer();
        manager = new PlotManager(new InpersistentPersistence(), worlds);
    }

    @Test
    public void testMayEnter() throws Exception {
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);

        Player owner = mockPlayer("leMaik");
        Player otherPlayer = mockPlayer("otherPlayer");
        Player blockedPlayer = mockPlayer("blockedPlayer");

        Plot plot = world.createPlotAt(mockLocation(0, 0, 0), owner);
        plot.addBlockedPlayer(manager.getPlayerCache().getCached(blockedPlayer));

        assertTrue("Owner may enter the plot", plot.mayEnter(owner));
        assertTrue("Other players may enter the plot", plot.mayEnter(otherPlayer));
        assertFalse("Blocked players may not enter the plot", plot.mayEnter(blockedPlayer));
    }

    @Test
    public void testMayBuild() throws Exception {
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);

        Player owner = mockPlayer("leMaik");
        Player otherPlayer = mockPlayer("otherPlayer");
        Player blockedPlayer = mockPlayer("blockedPlayer");
        Player helpingPlayer = mockPlayer("helpingPlayer");

        Plot plot = world.createPlotAt(mockLocation(0, 0, 0), owner);
        plot.addBlockedPlayer(manager.getPlayerCache().getCached(blockedPlayer));
        plot.addHelper(manager.getPlayerCache().getCached(helpingPlayer));

        assertTrue("Owner may build on the plot", plot.mayBuild(owner));
        assertFalse("Other players may not build on the plot", plot.mayBuild(otherPlayer));
        assertFalse("Blocked players may not build on the plot", plot.mayBuild(blockedPlayer));
        assertTrue("Helpers may build on the plot", plot.mayBuild(helpingPlayer));
    }

    @Test
    public void testMayManage() throws Exception {
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);

        Player owner = mockPlayer("leMaik");
        Player otherPlayer = mockPlayer("otherPlayer");
        Player blockedPlayer = mockPlayer("blockedPlayer");
        Player helpingPlayer = mockPlayer("helpingPlayer");

        Plot plot = world.createPlotAt(mockLocation(0, 0, 0), owner);
        plot.addBlockedPlayer(manager.getPlayerCache().getCached(blockedPlayer));
        plot.addHelper(manager.getPlayerCache().getCached(helpingPlayer));

        assertTrue("Owner may manage the plot", plot.mayManage(owner));
        assertFalse("Other players may not manage the plot", plot.mayManage(otherPlayer));
        assertFalse("Blocked players may not manage the plot", plot.mayManage(blockedPlayer));
        assertFalse("Helpers may not manage the plot", plot.mayManage(helpingPlayer));
    }

    @Test
    public void testGetBuildRegionConnectPlotsX() throws Exception {
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomPlayer");
        world = new PlotWorld("world", new PlotWorldConfiguration(new TestPlotScheme(16, 16, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, true), manager);
        worlds.addWorld(world);

        Plot plot = manager.createPlot(0, 0, owner, world);
        manager.createPlot(1, 0, owner, world);
        manager.createPlot(-1, 0, owner, world);
        assertEquals("Plots should connect in x-direction", new RectangularRegion(-2, 1, 19, 15 - 3), plot.getBuildRegion());
    }

    @Test
    public void testGetBuildRegionConnectPlotsZ() throws Exception {
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomPlayer");
        world = new PlotWorld("world", new PlotWorldConfiguration(new TestPlotScheme(16, 16, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, true), manager);
        worlds.addWorld(world);

        Plot plot = manager.createPlot(0, 0, owner, world);
        manager.createPlot(0, -1, owner, world);
        manager.createPlot(0, 1, owner, world);
        assertEquals("Plots should connect in z-direction", new RectangularRegion(4, -3, 13, 15 + 1), plot.getBuildRegion());
    }

    @Test
    public void testGetBuildRegionConnectPlotsXAndZ() throws Exception {
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomPlayer");
        world = new PlotWorld("world", new PlotWorldConfiguration(new TestPlotScheme(16, 16, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, true), manager);
        worlds.addWorld(world);

        Plot plot = manager.createPlot(0, 0, owner, world);
        manager.createPlot(0, -1, owner, world);
        manager.createPlot(0, 1, owner, world);
        manager.createPlot(1, 0, owner, world);
        manager.createPlot(-1, 0, owner, world);

        CombinedRegion expected = new CombinedRegion();
        expected.add(plot.getCoreBuildRegion()); //always inside build region
        expected.add(new RectangularRegion(-2, 1, 19, 15 - 3)); //x-direction
        expected.add(new RectangularRegion(4, -3, 13, 15 + 1)); //z-direction

        assertEquals("Plots should connect in x- and z-direction", expected, plot.getBuildRegion());
    }

    @Test
    public void testGetBuildRegionConnectPlotsFull() throws Exception {
        SimplePlayer owner = manager.getPlayerCache().getCached(UUID.randomUUID(), "randomPlayer");
        world = new PlotWorld("world", new PlotWorldConfiguration(new TestPlotScheme(16, 16, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, true), manager);
        worlds.addWorld(world);

        Plot plot = manager.createPlot(0, 0, owner, world);
        manager.createPlot(0, -1, owner, world);
        manager.createPlot(0, 1, owner, world);
        manager.createPlot(1, 0, owner, world);
        manager.createPlot(-1, 0, owner, world);
        manager.createPlot(-1, -1, owner, world);
        manager.createPlot(-1, 1, owner, world);
        manager.createPlot(1, -1, owner, world);
        manager.createPlot(1, 1, owner, world);

        assertEquals("Build region of a plot completely surrounded by connected plots should be a rectangle",
                new RectangularRegion(-2, -3, 19, 15 + 1), plot.getBuildRegion());
    }

    @Test
    public void testGetBuildRegionWithoutConnectedPlots() throws Exception {
        Player owner = mockPlayer("leMaik");
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);

        Plot plot = world.createPlotAt(mockLocation(0, 0, 0), owner);
        world.createPlotAt(mockLocation(16, 0, 0), owner);
        world.createPlotAt(mockLocation(-15, 0, 0), owner);

        assertEquals("Plot build region should be core build region if plots are not connected", plot.getCoreBuildRegion(), plot.getBuildRegion());
    }

    @Test
    public void testGetCoreBuildRegion() throws Exception {
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 2, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);

        /* plots in 'world' are 16x32 with following offsets:
           #-------------------------#
           |           1             |
           |   #---------------#     |
           | 4 |               |  2  |        ------->  X
           |   #---------------#     |        |
           |           3             |        |
           #-------------------------#        V  Z
         */

        Plot plot = world.createPlotAt(mockLocation(0, 0, 0), mockPlayer("leMaik"));
        assertEquals(new RectangularRegion(4, 1, 13, 28), plot.getCoreBuildRegion());
    }

    @Test
    public void testGetOwner() throws Exception {
        world = new PlotWorld("world", new PlotWorldConfiguration(new GenericPlotWorldScheme(1, 1, 1, 2, 3, 4), 0, 0, 5, Biome.PLAINS, false), manager);
        worlds.addWorld(world);

        Player owner = mockPlayer("leMaik");
        Plot plot = world.createPlotAt(mockLocation(0, 0, 0), owner);

        assertTrue("Owner should be the owner that was set", plot.getOwner().getUniqueId().equals(owner.getUniqueId()));
    }

    private Location mockLocation(double x, double y, double z) {
        Location location = mock(Location.class);
        when(location.getX()).thenReturn(x);
        when(location.getY()).thenReturn(y);
        when(location.getZ()).thenReturn(z);
        return location;
    }

    private Player mockPlayer(String nickname) {
        Player player = mock(Player.class);
        when(player.getName()).thenReturn(nickname);
        when(player.getUniqueId()).thenReturn(UUID.randomUUID());
        return player;
    }

    private class TestPlotScheme extends PlotWorldScheme {
        private final int northOffset;
        private final int eastOffset;
        private final int southOffset;
        private final int westOffset;

        public TestPlotScheme(int width, int length, int paddingNorth, int paddingEast, int paddingSouth, int paddingWest) {
            super(new TestPlotStructure(width, length));

            northOffset = paddingNorth;
            eastOffset = paddingEast;
            southOffset = paddingSouth;
            westOffset = paddingWest;
        }

        @Override
        public int getNorthOffset() {
            return northOffset;
        }

        @Override
        public int getEastOffset() {
            return eastOffset;
        }

        @Override
        public int getSouthOffset() {
            return southOffset;
        }

        @Override
        public int getWestOffset() {
            return westOffset;
        }

        @Override
        public void onLoaded(PlotWorld world) {

        }
    }

    private class TestPlotStructure extends PlotStructure implements BlockProvider {
        private int width, length;

        public TestPlotStructure(int width, int length) {
            this.width = width;
            this.length = length;
        }

        @Override
        public Block getBlockAt(int x, int y, int z) {
            return Block.AIR;
        }

        @Override
        public int getWidth() {
            return width;
        }

        @Override
        public int getLength() {
            return length;
        }
    }
}